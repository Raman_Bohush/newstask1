package com.epam.javalab.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Describes the properties of the table Author.
 * 
 * @author Raman_Bohush
 *
 */
@Entity
@Table(name = "AUTHOR")
public class Author implements Serializable {

	/**
	 * Serial version ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Property - author ID (primary key).
	 */
	@SequenceGenerator(name = "SEQ", sequenceName = "SEQ_AUTHOR_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	@Id
	@Column(name = "AUTHOR_ID", nullable = false, length = 20)
	private Long authorId;

	/**
	 * Property - author's name.
	 */
	@Column(name = "AUTHOR_NAME", nullable = false, length = 30)
	private String name;

	/**
	 * Property - expired date.
	 */
	@Column(name = "EXPIRED", nullable = true)
	private Date expired;

	@ManyToMany(mappedBy = "authorsList")
	private List<News> newsList;

	public Author() {
	}

	public Author(Long authorId, String name, Date expired) {
		this.authorId = authorId;
		this.name = name;
		this.expired = expired;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {

		this.name = name.trim();
	}

	public Date getExpired() {
		return expired;
	}

	public void setExpired(Date expired) {
		this.expired = expired;
	}

	@Override
	public int hashCode() {
		final int primeNumber = 31;
		int result = 1;
		result = result * primeNumber + ((authorId == null) ? 0 : authorId.hashCode());
		result = result * primeNumber + ((name == null) ? 0 : name.hashCode());
		result = result * primeNumber + ((expired == null) ? 0 : expired.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Author otherAuthor = (Author) obj;
		if (authorId == null) {
			if (otherAuthor.authorId != null) {
				return false;
			}
		} else {
			return authorId.equals(otherAuthor.authorId);
		}
		if (name == null) {
			if (otherAuthor.name != null) {
				return false;
			}
		} else {
			return name.equals(otherAuthor.name);
		}
		if (expired == null) {
			if (otherAuthor.expired != null) {
				return false;
			}
		} else {
			return name.equals(otherAuthor.expired);
		}
		return true;
	}

	@Override
	public String toString() {
		return "Author [authorId=" + authorId + ", name=" + name + ", expired=" + expired + "]";
	}
}

package com.epam.javalab.service;

import com.epam.javalab.domain.Comment;

/**
 * Comment Service interface. Set of additional operations for comment object and coordinates the application's response
 * in each operation.
 * 
 * @author Roma
 *
 */
public interface ICommentService extends IGenericService<Comment> {

	/**
	 * Invokes get news ID by comment ID operation from Data Access Object layer.
	 * 
	 * @param commentId
	 *            comment ID
	 * @return news ID
	 * @throws ServiceException
	 */
	// public Long getNewsIdByCommentId(Long commentId) throws ServiceException;
}

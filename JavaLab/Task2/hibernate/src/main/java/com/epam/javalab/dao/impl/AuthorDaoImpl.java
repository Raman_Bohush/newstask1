package com.epam.javalab.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.javalab.dao.IAuthorDao;
import com.epam.javalab.domain.Author;
import com.epam.javalab.exception.DaoException;

/**
 * Author Repository Implements all operations with author object.
 * 
 * @author Roma
 *
 */
@Repository
public class AuthorDaoImpl implements IAuthorDao {

	/**
	 * Receives a connection.
	 */
	@Autowired
	private SessionFactory sessionFactory;

	private AuthorDaoImpl() {
	}

	@Override
	public Long create(Author author) throws DaoException {

		Session session = null;
		Transaction transaction = null;
		Long authorId = null;

		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			authorId = (Long) session.save(author);
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DaoException("Hibernate error in the create author operation", e);
		} finally {
			session.close();
		}

		return authorId;

	}

	@Override
	public Author read(Long authorId) throws DaoException {

		Session session = null;
		Author author = null;

		try {
			session = sessionFactory.openSession();
			author = (Author) session.get(Author.class, authorId);
		} catch (HibernateException e) {
			throw new DaoException("Hibernate error in the read author operation", e);
		} finally {
			session.close();
		}

		return author;

	}

	@Override
	public void update(Author author) throws DaoException {

		Session session = null;
		Transaction transaction = null;

		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			session.update(author);
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DaoException("Hibernate error in the update author operation", e);
		} finally {
			session.close();
		}
	}

	@Override
	public void delete(Long authorId) throws DaoException {

		Session session = null;
		Transaction transaction = null;

		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();

			Author author = (Author) session.get(Author.class, authorId);
			session.delete(author);

			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DaoException("Hibernate error in the delete author operation", e);
		} finally {
			session.close();
		}
	}

	@Override
	public List<Author> readAll() throws DaoException {

		Session session = null;
		Criteria criteria = null;
		List<Author> authorsList = null;

		try {
			session = sessionFactory.openSession();
			criteria = session.createCriteria(Author.class);

			authorsList = criteria.list();
		} catch (HibernateException e) {
			throw new DaoException("Hibernate error in the read all authors operation", e);
		} finally {
			session.close();
		}

		return authorsList;
	}

	@Override
	public List<Author> getNotExpiredAuthors() throws DaoException {

		Session session = null;
		Criteria criteria = null;
		List<Author> notExpiredAuthorsList = null;

		try {
			session = sessionFactory.openSession();
			criteria = session.createCriteria(Author.class);
			criteria.add(Restrictions.isNull("expired"));
			notExpiredAuthorsList = criteria.list();
		} catch (HibernateException e) {
			throw new DaoException("Hibernate error in the read all not expired authors operation", e);
		} finally {
			session.close();
		}

		return notExpiredAuthorsList;
	}

}

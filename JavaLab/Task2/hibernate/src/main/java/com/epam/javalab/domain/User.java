package com.epam.javalab.domain;

import java.io.Serializable;

/**
 * Describes the properties of the table Users.
 * 
 * @author Raman_Bohush
 *
 */
public class User implements Serializable {

	/**
	 * Serial version ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Property - user ID (primary key).
	 */
	private Long userId;

	/**
	 * Property - user name.
	 */
	private String userName;

	/**
	 * Property - user login.
	 */
	private String login;

	/**
	 * Property - user password.
	 */
	private String password;

	public User() {
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}

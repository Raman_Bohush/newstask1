package com.epam.javalab.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.javalab.dao.ITagDao;
import com.epam.javalab.domain.Tag;
import com.epam.javalab.exception.DaoException;
import com.epam.javalab.exception.ServiceException;
import com.epam.javalab.service.ITagService;

@Service
public class TagServiceImpl implements ITagService {

	private static Logger logger = Logger.getLogger(TagServiceImpl.class);

	@Autowired
	private ITagDao tagDao;

	@Override
	public Long create(Tag tag) throws ServiceException {

		Long tagId = null;

		assertNotNull(tag, "Tag is empty.");

		try {
			tagId = tagDao.create(tag);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not create tag", e);
		}
		return tagId;

	}

	@Override
	public Tag read(Long tagId) throws ServiceException {

		Tag tag = null;

		assertNotNull(tagId, "Tag ID is empty.");

		try {
			tag = tagDao.read(tagId);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not read tag", e);
		}
		return tag;
	}

	@Override
	public void update(Tag tag) throws ServiceException {

		assertNotNull(tag, "Tag is empty.");

		try {
			tagDao.update(tag);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not update tag", e);
		}
	}

	@Override
	public void delete(Long tagId) throws ServiceException {

		assertNotNull(tagId, "Tag ID is empty.");

		try {
			tagDao.delete(tagId);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not delete tag", e);
		}

	}

	@Override
	public List<Tag> readAll() throws ServiceException {

		List<Tag> tagsList;
		try {
			tagsList = tagDao.readAll();
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not read all tags.", e);
		}
		return tagsList;
	}

	/**
	 * Asserts that object is not empty.
	 * 
	 * @param object
	 *            any object
	 * @param message
	 *            error message, that object is empty
	 * @throws ServiceException
	 */
	private void assertNotNull(Object object, String message) throws ServiceException {

		if (object == null) {
			throw new ServiceException(message);
		}
	}

}

package com.epam.javalab.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.javalab.dao.ITagDao;
import com.epam.javalab.domain.Tag;
import com.epam.javalab.exception.DaoException;

/**
 * Tag Repository Implements all operations with tag object.
 * 
 * @author Roma
 *
 */
@Repository
public class TagDaoImpl implements ITagDao {

	/**
	 * Receives a connection.
	 */
	@Autowired
	private SessionFactory sessionFactory;

	private TagDaoImpl() {
	}

	@Override
	public Long create(Tag tag) throws DaoException {

		Session session = null;
		Transaction transaction = null;
		Long tagId = null;

		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			tagId = (Long) session.save(tag);
			transaction.commit();

		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DaoException("Hibernate error in the create tag operation", e);
		} finally {
			session.close();
		}

		return tagId;
	}

	@Override
	public Tag read(Long tagId) throws DaoException {

		Session session = null;
		Tag tag = null;

		try {
			session = sessionFactory.openSession();
			tag = (Tag) session.get(Tag.class, tagId);
		} catch (HibernateException e) {
			throw new DaoException("Hibernate error in the read tag operation", e);
		} finally {
			session.close();
		}

		return tag;
	}

	@Override
	public void update(Tag tag) throws DaoException {

		Session session = null;
		Transaction transaction = null;

		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			session.update(tag);

			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DaoException("Hibernate error in the update tag operation", e);
		} finally {
			session.close();
		}
	}

	@Override
	public void delete(Long tagId) throws DaoException {

		Session session = null;
		Transaction transaction = null;

		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			Tag tag = (Tag) session.get(Tag.class, tagId);
			session.delete(tag);

			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DaoException("Hibernate error in the delete tag operation", e);
		} finally {
			session.close();
		}
	}

	@Override
	public List<Tag> readAll() throws DaoException {

		Session session = null;
		List<Tag> tagsList = null;
		Criteria criteria = null;

		try {
			session = sessionFactory.openSession();
			criteria = session.createCriteria(Tag.class);
			tagsList = criteria.list();
		} catch (HibernateException e) {
			throw new DaoException("Hibernate error in the read all tags operation", e);
		} finally {
			session.close();
		}

		return tagsList;
	}

}

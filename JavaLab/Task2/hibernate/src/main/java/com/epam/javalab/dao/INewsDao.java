package com.epam.javalab.dao;

import java.util.List;

import com.epam.javalab.domain.News;
import com.epam.javalab.domain.SearchCriteria;
import com.epam.javalab.exception.DaoException;

/**
 * News Data Access Object interface. Provides additional operations with news persistent object.
 * 
 * @author Raman_Bohush
 *
 */
public interface INewsDao extends IGenericDao<News> {

	/**
	 * Finds news persistent object by one of the criteria.
	 * 
	 * @param searchCriteria
	 *            criteria for searching
	 * @param numPage
	 *            number of the page
	 * @return list of found news
	 * @throws DaoException
	 */
	public List<News> searchNews(SearchCriteria searchCriteria, Integer numPage) throws DaoException;

	/**
	 * Gets count of all the news.
	 * 
	 * @return news count
	 * @throws DaoException
	 */
	public Long getCountNews(SearchCriteria searchCriteria) throws DaoException;

	/**
	 * Gets news ID of the next news.
	 * 
	 * @param searchCriteria
	 *            search criteria.
	 * @param newsId
	 *            news ID
	 * @return news ID of the next news.
	 * @throws DaoException
	 */
	public Long getNextNews(SearchCriteria searchCriteria, Long newsId) throws DaoException;

	/**
	 * Gets news ID of the previous news.
	 * 
	 * @param searchCriteria
	 *            search criteria.
	 * @param newsId
	 *            news ID
	 * @return news ID of the previous news
	 * @throws DaoException
	 */
	public Long getPreviousNews(SearchCriteria searchCriteria, Long newsId) throws DaoException;

}

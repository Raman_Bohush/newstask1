package com.epam.javalab.service;

import java.util.List;

import com.epam.javalab.domain.News;
import com.epam.javalab.domain.SearchCriteria;
import com.epam.javalab.exception.ServiceException;

/**
 * News Service interface. Set of additional operations for news object and coordinates the application's response in
 * each operation.
 * 
 * @author Roma
 *
 */
public interface INewsService extends IGenericService<News> {

	/**
	 * Invokes search news by one of the criteria operation from Data Access Object layer. Finds news persistent object
	 * by one of the criteria. Checks parameter to search.
	 * 
	 * @param searchCriteria
	 *            criteria for searching
	 * @param numPage
	 *            number of page
	 * @return list of found news
	 * @throws ServiceException
	 */
	public List<News> searchNews(SearchCriteria searchCriteriam, Integer numPage) throws ServiceException;

	/**
	 * Invokes get count news from Data Access Object layer.
	 * 
	 * @return news count
	 * @throws ServiceException
	 */
	public Long getCountNews(SearchCriteria searchCriteria) throws ServiceException;

	public Long getNextNews(SearchCriteria searchCriteria, Long newsId) throws ServiceException;

	public Long getPreviousNews(SearchCriteria searchCriteria, Long newsId) throws ServiceException;

	public void deleteMarkedNews(List<Long> newsIdList) throws ServiceException;
}

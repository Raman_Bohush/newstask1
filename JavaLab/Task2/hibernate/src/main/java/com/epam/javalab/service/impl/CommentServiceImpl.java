package com.epam.javalab.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.javalab.dao.ICommentDao;
import com.epam.javalab.domain.Comment;
import com.epam.javalab.exception.DaoException;
import com.epam.javalab.exception.ServiceException;
import com.epam.javalab.service.ICommentService;

@Service
public class CommentServiceImpl implements ICommentService {

	private static Logger logger = Logger.getLogger(CommentServiceImpl.class);

	@Autowired
	private ICommentDao commentDao;

	@Override
	public Long create(Comment comment) throws ServiceException {

		Long commentId = null;

		assertNotNull(comment, "Comment is empty.");

		try {
			commentId = commentDao.create(comment);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not create comment.", e);
		}
		return commentId;

	}

	@Override
	public Comment read(Long commentId) throws ServiceException {

		Comment comment = null;

		assertNotNull(commentId, "Comment ID is empty.");

		try {
			comment = commentDao.read(commentId);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not read comment", e);
		}
		return comment;
	}

	@Override
	public void update(Comment comment) throws ServiceException {

		assertNotNull(comment, "Comment is empty.");

		try {
			commentDao.update(comment);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not update comment", e);
		}

	}

	@Override
	public void delete(Long commentId) throws ServiceException {

		assertNotNull(commentId, "Comment ID is empty.");

		try {
			commentDao.delete(commentId);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not delete comment", e);
		}
	}

	@Override
	public List<Comment> readAll() throws ServiceException {

		List<Comment> commentsList;
		try {
			commentsList = commentDao.readAll();
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not read all comments.", e);
		}
		return commentsList;
	}

	/*
	 * @Override public Long getNewsIdByCommentId(Long commentId) throws ServiceException {
	 * 
	 * Long newsId = null;
	 * 
	 * assertNotNull(commentId, "Comment ID is empty.");
	 * 
	 * try { newsId = commentDao.getNewsIdByCommentId(commentId); } catch (DaoException e) {
	 * logger.error(e.getMessage()); throw new ServiceException("Can not get news ID by comment ID", e); } return
	 * newsId; }
	 */

	/**
	 * Asserts that object is not empty.
	 * 
	 * @param object
	 *            any object
	 * @param message
	 *            error message, that object is empty
	 * @throws ServiceException
	 */
	private void assertNotNull(Object object, String message) throws ServiceException {

		if (object == null) {
			throw new ServiceException(message);
		}
	}

}

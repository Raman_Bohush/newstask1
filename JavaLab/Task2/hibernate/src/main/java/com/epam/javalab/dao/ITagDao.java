package com.epam.javalab.dao;

import com.epam.javalab.domain.Tag;

/**
 * Tag Data Access Object interface. Provides additional operations with tag persistent object.
 * 
 * @author Raman_Bohush
 *
 */
public interface ITagDao extends IGenericDao<Tag> {
}

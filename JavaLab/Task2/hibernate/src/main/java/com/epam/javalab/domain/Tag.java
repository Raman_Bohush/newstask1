package com.epam.javalab.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Describes the properties of the table Tag.
 * 
 * @author Raman_Bohush
 *
 */
@Entity
@Table(name = "TAG")
public class Tag implements Serializable {

	/**
	 * Serial version ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Property - tag ID (primary key).
	 */
	@SequenceGenerator(name = "SEQ", sequenceName = "SEQ_TAG_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	@Id
	@Column(name = "TAG_ID", nullable = false, length = 20)
	private Long tagId;

	/**
	 * Property - tag name.
	 */
	@Column(name = "TAG_NAME", nullable = false, length = 30)
	private String tagName;

	@ManyToMany(mappedBy = "tagsList")
	private List<News> newsList;

	public Tag() {
	}

	public Tag(Long tagId, String tagName) {

		this.tagId = tagId;
		this.tagName = tagName;
	}

	public Long getTagId() {
		return tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {

		this.tagName = tagName.trim();
	}

	@Override
	public int hashCode() {
		final int primeNumber = 31;
		int result = 1;
		result = result * primeNumber + ((tagId == null) ? 0 : tagId.hashCode());
		result = result * primeNumber + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Tag otherTag = (Tag) obj;
		if (tagId == null) {
			if (otherTag.tagId != null) {
				return false;
			}
		} else {
			return tagId.equals(otherTag.tagId);
		}
		if (tagName == null) {
			if (otherTag.tagName != null) {
				return false;
			}
		} else {
			return tagName.equals(otherTag.tagName);
		}
		return true;
	}

	@Override
	public String toString() {
		return "Tag [tagId=" + tagId + ", tagName=" + tagName + "]";
	}

}

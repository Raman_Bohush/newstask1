package com.epam.javalab.dao;

import java.util.List;

import com.epam.javalab.domain.Author;
import com.epam.javalab.exception.DaoException;

/**
 * Author Data Access Object interface. Provides additional operations with author persistent object.
 * 
 * @author Raman_Bohush
 *
 */
public interface IAuthorDao extends IGenericDao<Author> {

	/**
	 * Gets the list of all not expired authors.
	 * 
	 * @return list of all not expired authors
	 * @throws DaoException
	 */
	public List<Author> getNotExpiredAuthors() throws DaoException;
}

package com.epam.javalab.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "COUNT_COMMENTS")
public class CountComment implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "NEWS_ID", nullable = false, length = 20)
	private Long newsId;

	@Column(name = "COUNT_COMMENTS", nullable = false, length = 20)
	private Long countComments;

	public CountComment() {
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public Long getCountComments() {
		return countComments;
	}

	public void setCountComments(Long countComments) {
		this.countComments = countComments;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((countComments == null) ? 0 : countComments.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CountComment other = (CountComment) obj;
		if (countComments == null) {
			if (other.countComments != null)
				return false;
		} else if (!countComments.equals(other.countComments))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CountComment [newsId=" + newsId + ", countComments=" + countComments + "]";
	}

}

package com.epam.javalab.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Describes the properties of the table News.
 * 
 * @author Raman_Bohush
 *
 */
@Entity
@Table(name = "NEWS")
public class News implements Serializable {

	/**
	 * Serial version ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Property - news ID (primary key).
	 */
	@SequenceGenerator(name = "SEQ", sequenceName = "SEQ_NEWS_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	@Id
	@Column(name = "NEWS_ID", nullable = false, length = 20)
	private Long newsId;

	/**
	 * Property - news short text.
	 */
	@Column(name = "SHORT_TEXT", nullable = false, length = 100)
	private String shortText;

	/**
	 * Property - news full text.
	 */
	@Column(name = "FULL_TEXT", nullable = false, length = 2000)
	private String fullText;

	/**
	 * Property - news title.
	 */
	@Column(name = "TITLE", nullable = false, length = 30)
	private String title;

	/**
	 * Property - creation date news.
	 */
	@Column(name = "CREATION_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	/**
	 * Property - modification date news.
	 */
	@Column(name = "MODIFICATION_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date modificationDate;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "NEWS_AUTHOR", joinColumns = { @JoinColumn(name = "NEWS_ID") }, inverseJoinColumns = { @JoinColumn(name = "AUTHOR_ID") })
	private List<Author> authorsList;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "NEWS_TAG", joinColumns = { @JoinColumn(name = "NEWS_ID") }, inverseJoinColumns = { @JoinColumn(name = "TAG_ID") })
	private List<Tag> tagsList;

	@OneToMany(mappedBy = "newsId", fetch = FetchType.LAZY, cascade = { CascadeType.REMOVE })
	private List<Comment> commentsList;

	@OneToOne(fetch = FetchType.LAZY)
	@PrimaryKeyJoinColumn
	private CountComment countComments;

	public News() {
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {

		this.shortText = shortText.trim();
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {

		this.fullText = fullText.trim();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {

		this.title = title.trim();
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public List<Author> getAuthorsList() {
		return authorsList;
	}

	public void setAuthorsList(List<Author> authorsList) {
		this.authorsList = authorsList;
	}

	public List<Tag> getTagsList() {
		return tagsList;
	}

	public void setTagsList(List<Tag> tagsList) {
		this.tagsList = tagsList;
	}

	public List<Comment> getCommentsList() {
		return commentsList;
	}

	public void setCommentsList(List<Comment> commentsList) {
		this.commentsList = commentsList;
	}

	public CountComment getCountComments() {
		return countComments;
	}

	public void setCountComments(CountComment countComments) {
		this.countComments = countComments;
	}

	@Override
	public int hashCode() {
		final int primeNumber = 31;
		int result = 1;
		result = result * primeNumber + ((newsId == null) ? 0 : newsId.hashCode());
		result = result * primeNumber + ((shortText == null) ? 0 : shortText.hashCode());
		result = result * primeNumber + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = result * primeNumber + ((fullText == null) ? 0 : fullText.hashCode());
		result = result * primeNumber + ((title == null) ? 0 : title.hashCode());
		result = result * primeNumber + ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = result * primeNumber + ((authorsList == null) ? 0 : authorsList.hashCode());
		result = result * primeNumber + ((tagsList == null) ? 0 : tagsList.hashCode());
		result = result * primeNumber + ((commentsList == null) ? 0 : commentsList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		News otherNews = (News) obj;
		if (newsId == null) {
			if (otherNews.newsId != null) {
				return false;
			}
		} else {
			return newsId.equals(otherNews.newsId);
		}
		if (shortText == null) {
			if (otherNews.shortText != null) {
				return false;
			}
		} else {
			return shortText.equals(otherNews.shortText);
		}
		if (creationDate == null) {
			if (otherNews.creationDate != null) {
				return false;
			}
		} else {
			return creationDate.equals(otherNews.creationDate);
		}
		if (fullText == null) {
			if (otherNews.fullText != null) {
				return false;
			}
		} else {
			return fullText.equals(otherNews.fullText);
		}
		if (title == null) {
			if (otherNews.title != null) {
				return false;
			}
		} else {
			return title.equals(otherNews.title);
		}
		if (modificationDate == null) {
			if (otherNews.modificationDate != null) {
				return false;
			}
		} else {
			return modificationDate.equals(otherNews.modificationDate);
		}
		if (authorsList == null) {
			if (otherNews.authorsList != null) {
				return false;
			}
		} else {
			return authorsList.equals(otherNews.authorsList);
		}

		if (tagsList == null) {
			if (otherNews.tagsList != null) {
				return false;
			}
		} else {
			return tagsList.equals(otherNews.tagsList);
		}

		if (commentsList == null) {
			if (otherNews.commentsList != null) {
				return false;
			}
		} else {
			return commentsList.equals(otherNews.commentsList);
		}
		return true;
	}

	@Override
	public String toString() {
		return "News [newsId=" + newsId + ", shortText=" + shortText + ", fullText=" + fullText + ", title=" + title
				+ ", creationDate=" + creationDate + ", modificationDate=" + modificationDate + ", authorsList="
				+ authorsList + ", tagsList=" + tagsList + ", commentsList=" + commentsList + "]";
	}

}

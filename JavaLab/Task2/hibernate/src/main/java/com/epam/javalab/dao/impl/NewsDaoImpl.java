package com.epam.javalab.dao.impl;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.javalab.dao.INewsDao;
import com.epam.javalab.domain.News;
import com.epam.javalab.domain.SearchCriteria;
import com.epam.javalab.exception.DaoException;
import com.epam.javalab.util.Pagination;
import com.epam.javalab.util.QueryBuilder;

/**
 * News Repository Implements all operations with news object.
 * 
 * @author Roma
 *
 */
@Repository
public class NewsDaoImpl implements INewsDao {

	/**
	 * Receives a connection.
	 */
	@Autowired
	private SessionFactory sessionFactory;

	private NewsDaoImpl() {
	}

	@Override
	public Long create(News news) throws DaoException {

		Session session = null;
		Transaction transaction = null;
		Long newsId = null;

		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			newsId = (Long) session.save(news);
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DaoException("Hibernate error in the create news operation", e);
		}
		return newsId;
	}

	@Override
	public News read(Long newsId) throws DaoException {

		Session session = null;
		News news = null;

		try {
			session = sessionFactory.openSession();
			news = (News) session.get(News.class, newsId);
			news.getAuthorsList().size();
			news.getTagsList().size();
			news.getCommentsList().size();
		} catch (HibernateException e) {
			throw new DaoException("Hibernate error in the read news operation", e);
		} finally {
			session.close();
		}
		return news;
	}

	@Override
	public void update(News news) throws DaoException {

		Session session = null;
		Transaction transaction = null;

		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			session.update(news);
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DaoException("Hibernate error in the update news operation", e);
		} finally {
			session.close();
		}

	}

	@Override
	public void delete(Long newsId) throws DaoException {

		Session session = null;
		Transaction transaction = null;

		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();

			News news = (News) session.get(News.class, newsId);
			session.delete(news);

			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DaoException("Hibernate error in the delete news operation", e);
		} finally {
			session.close();
		}

	}

	@Override
	public List<News> readAll() throws DaoException {

		Session session = null;
		Criteria criteria = null;
		List<News> newsList = null;

		try {
			session = sessionFactory.openSession();

			newsList = (List<News>) session.createCriteria(News.class).list();

		} catch (HibernateException e) {
			throw new DaoException("Hibernate error in the read all news operation", e);
		} finally {
			session.close();
		}

		return newsList;
	}

	@Override
	public List<News> searchNews(SearchCriteria searchCriteria, Integer numPage) throws DaoException {

		Session session = null;
		Criteria criteria = null;
		List<News> newsList = null;
		Integer startIdx = Pagination.getStartIndex(numPage);
		Integer endIdx = Pagination.getEndIndex(numPage);

		Criterion authorCriterion = null;
		Criterion tagCriterion = null;
		Disjunction disjunction = Restrictions.disjunction();

		Long authorsId = searchCriteria.getAuthorId();
		List<Long> tagsIdList = searchCriteria.getTagsIdList();

		Long[] tagArray = null;

		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(News.class);

		try {
			session = sessionFactory.openSession();
			criteria = session.createCriteria(News.class);

			if (authorsId != null && authorsId != 0) {

				detachedCriteria.createAlias("authorsList", "news_author");

				authorCriterion = Restrictions.eq("news_author.authorId", authorsId);
				disjunction.add(authorCriterion);

			}

			if (tagsIdList != null && !tagsIdList.isEmpty()) {

				tagArray = (Long[]) searchCriteria.getTagsIdList().toArray(new Long[tagsIdList.size()]);

				detachedCriteria.createAlias("tagsList", "news_tag", JoinType.LEFT_OUTER_JOIN);

				tagCriterion = Restrictions.in("news_tag.tagId", tagArray);
				disjunction.add(tagCriterion);
			}

			detachedCriteria.add(disjunction);

			detachedCriteria.setProjection(Projections.distinct(Projections.property("newsId")));
			detachedCriteria.setProjection(Projections.property("newsId"));

			criteria.createAlias("countComments", "count");

			criteria.add(Subqueries.propertyIn("newsId", detachedCriteria));

			criteria.addOrder(Order.desc("count.countComments"));
			criteria.addOrder(Order.desc("modificationDate"));

			criteria.setFirstResult(startIdx);
			criteria.setMaxResults(Pagination.COUNT_NEWS_ON_PAGE);

			newsList = (List<News>) criteria.list();

			for (News news : newsList) {
				Hibernate.initialize(news.getAuthorsList());
				Hibernate.initialize(news.getTagsList());
				Hibernate.initialize(news.getCommentsList());
				Hibernate.initialize(news.getCountComments());
			}
		} catch (HibernateException e) {
			throw new DaoException("Hibernate error in the search news operation", e);
		} finally {
			session.close();
		}

		return newsList;
	}

	@Override
	public Long getCountNews(SearchCriteria searchCriteria) throws DaoException {

		Session session = null;
		Criteria criteria = null;
		Long countNews = null;
		Criterion authorCriterion = null;
		Criterion tagCriterion = null;
		Disjunction disjunction = Restrictions.disjunction();

		Long authorsId = searchCriteria.getAuthorId();
		List<Long> tagsIdList = searchCriteria.getTagsIdList();

		Long[] tagArray = null;

		try {
			session = sessionFactory.openSession();
			criteria = session.createCriteria(News.class);

			if (authorsId != null && authorsId != 0) {

				criteria.createAlias("authorsList", "news_author");

				authorCriterion = Restrictions.eq("news_author.authorId", authorsId);
				disjunction.add(authorCriterion);

			}

			if (tagsIdList != null && !tagsIdList.isEmpty()) {

				tagArray = (Long[]) searchCriteria.getTagsIdList().toArray(new Long[tagsIdList.size()]);

				criteria.createAlias("tagsList", "news_tag", JoinType.LEFT_OUTER_JOIN);

				tagCriterion = Restrictions.in("news_tag.tagId", tagArray);
				disjunction.add(tagCriterion);
			}

			criteria.add(disjunction);

			criteria.setProjection(Projections.rowCount());

			countNews = (Long) criteria.uniqueResult();

		} catch (HibernateException e) {
			throw new DaoException("Hibernate error in the get count news operation", e);
		} finally {
			session.close();
		}

		return countNews;

	}

	@Override
	public Long getNextNews(SearchCriteria searchCriteria, Long newsId) throws DaoException {

		Session session = null;
		Long nextNewsId = null;

		Long authorId = searchCriteria.getAuthorId();
		List<Long> tagsIdList = searchCriteria.getTagsIdList();

		try {
			session = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(QueryBuilder.buildNextQuery(searchCriteria));

			int paramIdx = 0;
			if (authorId != null && authorId != 0) {
				sqlQuery.setLong(paramIdx++, authorId);
			}
			if (tagsIdList != null) {
				for (Long tagId : tagsIdList) {
					sqlQuery.setLong(paramIdx++, tagId);
				}
			}

			sqlQuery.setLong(paramIdx, newsId);

			Object result = sqlQuery.uniqueResult();

			if (result != null) {
				nextNewsId = ((BigDecimal) sqlQuery.uniqueResult()).longValue();
			}

		} catch (HibernateException e) {
			throw new DaoException("Hibernate error in the get next news operation", e);
		} finally {
			session.close();
		}

		return nextNewsId;
	}

	@Override
	public Long getPreviousNews(SearchCriteria searchCriteria, Long newsId) throws DaoException {

		Session session = null;
		Long prevNewsId = null;

		Long authorId = searchCriteria.getAuthorId();
		List<Long> tagsIdList = searchCriteria.getTagsIdList();

		try {
			session = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(QueryBuilder.buildPreviousQuery(searchCriteria));

			int paramIdx = 0;
			if (authorId != null && authorId != 0) {
				sqlQuery.setLong(paramIdx++, authorId);
			}
			if (tagsIdList != null) {
				for (Long tagId : tagsIdList) {
					sqlQuery.setLong(paramIdx++, tagId);
				}
			}

			sqlQuery.setLong(paramIdx, newsId);

			Object result = sqlQuery.uniqueResult();

			if (result != null) {
				prevNewsId = ((BigDecimal) sqlQuery.uniqueResult()).longValue();
			}

		} catch (HibernateException e) {
			throw new DaoException("Hibernate error in the get previous news operation", e);
		} finally {
			session.close();
		}

		return prevNewsId;
	}

	/**
	 * Builds news.
	 * 
	 * @param news
	 *            news
	 * @param rs
	 *            result set
	 * @return created news
	 * @throws SQLException
	 */
	private News buildNews(News news, ResultSet rs) throws SQLException {

		news = new News();
		news.setNewsId(rs.getLong(1));
		news.setTitle(rs.getString(2));
		news.setShortText(rs.getString(3));
		news.setFullText(rs.getString(4));
		news.setCreationDate(rs.getTimestamp(5));
		news.setModificationDate(rs.getDate(6));
		return news;
	}
}

package com.epam.javalab.dao;

import com.epam.javalab.domain.Comment;

/**
 * Comment Data Access Object interface. Provides additional operations with comment persistent object.
 * 
 * @author Raman_Bohush
 *
 */
public interface ICommentDao extends IGenericDao<Comment> {

	/**
	 * Gets news ID of the comment.
	 * 
	 * @param commentId
	 *            comment ID
	 * @return news ID
	 * @throws DaoException
	 */
	// public Long getNewsIdByCommentId(Long commentId) throws DaoException;

}

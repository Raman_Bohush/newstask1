package com.epam.javalab.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.javalab.dao.ICommentDao;
import com.epam.javalab.domain.Comment;
import com.epam.javalab.exception.DaoException;

/**
 * Comment Repository Implements all operations with comment object.
 * 
 * @author Roma
 *
 */
@Repository
public class CommentDaoImpl implements ICommentDao {

	/**
	 * Receives a connection.
	 */
	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * SQL request for get news ID by comment ID.
	 */
	private final static String GET_NEWS_ID_BY_COMMENT_ID_SQL = "SELECT news_id " + "FROM comments "
			+ "WHERE comment_id=?";

	private CommentDaoImpl() {
	}

	@Override
	public Long create(Comment comment) throws DaoException {

		Session session = null;
		Transaction transaction = null;
		Long commentId = null;

		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			commentId = (Long) session.save(comment);
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DaoException("Hibernate error in the create comment operation", e);
		} finally {
			session.close();
		}
		return commentId;
	}

	@Override
	public Comment read(Long commentId) throws DaoException {

		Session session = null;
		Comment comment = null;

		try {
			session = sessionFactory.openSession();
			comment = (Comment) session.get(Comment.class, commentId);
		} catch (HibernateException e) {
			throw new DaoException("Hibernate error in the read comment operation", e);
		} finally {
			session.close();
		}

		return comment;
	}

	@Override
	public void update(Comment comment) throws DaoException {

		Session session = null;
		Transaction transaction = null;

		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			session.update(comment);
			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DaoException("Hibernate error in the update comment operation", e);
		} finally {
			session.close();
		}
	}

	@Override
	public void delete(Long commentId) throws DaoException {

		Session session = null;
		Transaction transaction = null;

		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();

			Comment comment = (Comment) session.get(Comment.class, commentId);
			session.delete(comment);

			transaction.commit();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			throw new DaoException("Hibernate error in the delete comment operation", e);
		} finally {
			session.close();
		}
	}

	@Override
	public List<Comment> readAll() throws DaoException {

		Session session = null;
		Criteria criteria = null;
		List<Comment> commentsList = null;

		try {
			session = sessionFactory.openSession();
			criteria = session.createCriteria(Comment.class);

			commentsList = criteria.list();
		} catch (HibernateException e) {
			throw new DaoException("Hibernate error in the read all authors operation", e);
		} finally {
			session.close();
		}

		return commentsList;
	}

	/*
	 * @Override public Long getNewsIdByCommentId(Long commentId) throws DaoException {
	 * 
	 * Connection con = DataSourceUtils.getConnection(dataSource); ResultSet rs = null; Long newsId = null; try
	 * (PreparedStatement ps = con.prepareStatement(GET_NEWS_ID_BY_COMMENT_ID_SQL)) { ps.setLong(1, commentId); rs =
	 * ps.executeQuery(); if (rs.next()) { newsId = rs.getLong(1); } } catch (SQLException e) { throw new
	 * DaoException("SQL error in the get news ID by comment ID operation.", e); } finally {
	 * DatabaseUtil.closeResultSet(rs); DataSourceUtils.releaseConnection(con, dataSource); } return newsId; }
	 */

}

package com.epam.javalab.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Describes the properties of the table Comments.
 * 
 * @author Raman_Bohush
 *
 */
@Entity
@Table(name = "COMMENTS")
public class Comment implements Serializable {

	/**
	 * Serial version ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Property - comment ID (primary key).
	 */
	@SequenceGenerator(name = "SEQ", sequenceName = "SEQ_COMMENTS_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
	@Id
	@Column(name = "COMMENT_ID", nullable = false, length = 20)
	private Long commentId;

	/**
	 * Property - text comment.
	 */
	@Column(name = "COMMENT_TEXT", nullable = false, length = 100)
	private String commentText;

	/**
	 * Property - creation date comment.
	 */
	@Column(name = "CREATION_DATE", nullable = false)
	private Date creationDate;

	/**
	 * Property - news ID (foreign key).
	 */
	@Column(name = "NEWS_ID", nullable = false, length = 20)
	private Long newsId;

	public Comment() {
	}

	public Comment(Long commentId, String commentText, Date creationDate, Long newsId) {
		this.commentId = commentId;
		this.commentText = commentText;
		this.creationDate = creationDate;
		this.newsId = newsId;
	}

	public Long getCommentId() {
		return commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {

		this.commentText = commentText.trim();
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	@Override
	public int hashCode() {
		final int primeNumber = 31;
		int result = 1;
		result = result * primeNumber + ((commentId == null) ? 0 : commentId.hashCode());
		result = result * primeNumber + ((commentText == null) ? 0 : commentText.hashCode());
		result = result * primeNumber + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = result * primeNumber + ((newsId == null) ? 0 : newsId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Comment otherComment = (Comment) obj;
		if (commentId == null) {
			if (otherComment.commentId != null) {
				return false;
			}
		} else {
			return commentId.equals(otherComment.commentId);
		}
		if (commentText == null) {
			if (otherComment.commentText != null) {
				return false;
			}
		} else {
			return commentText.equals(otherComment.commentText);
		}
		if (creationDate == null) {
			if (otherComment.creationDate != null) {
				return false;
			}
		} else {
			return creationDate.equals(otherComment.creationDate);
		}
		if (newsId == null) {
			if (otherComment.newsId != null) {
				return false;
			}
		} else {
			return newsId.equals(otherComment.newsId);
		}
		return true;
	}

	@Override
	public String toString() {
		return "Comment [commentId=" + commentId + ", commentText=" + commentText + ", creationDate=" + creationDate
				+ ", newsId=" + newsId + "]";
	}

}

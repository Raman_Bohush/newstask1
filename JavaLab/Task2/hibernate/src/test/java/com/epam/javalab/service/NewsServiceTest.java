package com.epam.javalab.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.javalab.dao.impl.NewsDaoImpl;
import com.epam.javalab.domain.News;
import com.epam.javalab.exception.DaoException;
import com.epam.javalab.exception.ServiceException;
import com.epam.javalab.service.impl.NewsServiceImpl;

/**
 * Tests News Service using mocking framework - Mockito.
 * 
 * @author Roma
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {

	/**
	 * News Data Access Object is mock.
	 */
	@Mock
	private NewsDaoImpl newsDao;

	/**
	 * News Service.
	 */
	@InjectMocks
	private NewsServiceImpl newsService;

	/**
	 * Tests create operation.
	 * 
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void createNews() throws ServiceException, DaoException {

		News newNews = new News();
		newNews.setTitle("title");
		Long expectedNewsId = 2L;

		when(newsDao.create(newNews)).thenReturn(expectedNewsId);

		Long actualNewsId = newsService.create(newNews);

		Assert.assertEquals(expectedNewsId, actualNewsId);

		verify(newsDao, times(1)).create(newNews);
	}

	/**
	 * Tests read by id operation.
	 * 
	 * @throws DaoException
	 * @throws ServiceException
	 */
	@Test
	public void readNews() throws ServiceException, DaoException {

		News expectedNews = new News();
		expectedNews.setTitle("title");
		Long newsId = 3L;

		when(newsDao.read(newsId)).thenReturn(expectedNews);

		News actualNews = newsService.read(newsId);

		Assert.assertEquals(expectedNews, actualNews);

		verify(newsDao, times(1)).read(newsId);
	}

	/**
	 * Tests update operation.
	 * 
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void updateNews() throws ServiceException, DaoException {

		News newNews = new News();
		newNews.setTitle("title");

		newsService.update(newNews);

		verify(newsDao).update(newNews);
	}

	/**
	 * Tests delete operation.
	 * 
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void deleteNews() throws ServiceException, DaoException {

		Long newsId = 2L;

		newsService.delete(newsId);

		verify(newsDao).delete(newsId);
	}

	/**
	 * Tests read all operation.
	 * 
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void readAllNews() throws ServiceException, DaoException {

		List<News> expectedNewsList = new ArrayList<>();
		News news1 = new News();
		news1.setTitle("title1");
		News news2 = new News();
		news2.setTitle("title2");
		expectedNewsList.add(news1);
		expectedNewsList.add(news2);
		int expectedCountNews = expectedNewsList.size();

		when(newsDao.readAll()).thenReturn(expectedNewsList);

		List<News> actualNewsList = newsService.readAll();
		int actualCountNews = actualNewsList.size();

		Assert.assertEquals(expectedCountNews, actualCountNews);
		Assert.assertEquals(expectedNewsList, actualNewsList);

		verify(newsDao, times(1)).readAll();
	}

	/**
	 * Tests search news operation.
	 * 
	 * @throws ServiceException
	 * @throws DaoException
	 */
	/*
	 * @Test public void searchNews() throws ServiceException, DaoException {
	 * 
	 * List<NewsVO> expectedNewsVOList = new ArrayList<>(); NewsVO newsVO1 = new NewsVO(); newsVO1.setCountComment(3L);
	 * NewsVO newsVO2 = new NewsVO(); newsVO2.setCountComment(33L);
	 * 
	 * expectedNewsVOList.add(newsVO1); expectedNewsVOList.add(newsVO2);
	 * 
	 * SearchCriteria searchCriteria = new SearchCriteria();
	 * 
	 * Integer countNewsOnPage = 3; Integer numPage = 3; Long authorId = 1L; List<Long> tagsIdList = new ArrayList<>();
	 * tagsIdList.add(1L); tagsIdList.add(3L);
	 * 
	 * searchCriteria.setAuthorId(authorId); searchCriteria.setTagsIdList(tagsIdList);
	 * 
	 * when(newsDao.searchNews(searchCriteria, countNewsOnPage, numPage)).thenReturn(expectedNewsVOList);
	 * 
	 * List<NewsVO> actualNewsVOList = newsService.searchNews(searchCriteria, countNewsOnPage, numPage);
	 * 
	 * Assert.assertEquals(expectedNewsVOList, actualNewsVOList);
	 * 
	 * verify(newsDao, times(1)).searchNews(searchCriteria, countNewsOnPage, numPage); }
	 */

}

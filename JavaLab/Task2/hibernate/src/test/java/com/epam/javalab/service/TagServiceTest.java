package com.epam.javalab.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.javalab.dao.impl.TagDaoImpl;
import com.epam.javalab.domain.Tag;
import com.epam.javalab.exception.DaoException;
import com.epam.javalab.exception.ServiceException;
import com.epam.javalab.service.impl.TagServiceImpl;

/**
 * Tests Tag Service using mocking framework - Mockito.
 * 
 * @author Roma
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {

	/**
	 * Tag Data Access Object is mock.
	 */
	@Mock
	private TagDaoImpl tagDao;

	/**
	 * Tag Service.
	 */
	@InjectMocks
	private TagServiceImpl tagService;

	/**
	 * Tests create operation.
	 * 
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void createTag() throws ServiceException, DaoException {

		Tag tag = new Tag();
		tag.setTagName("economics");
		Long expectedTagId = 3L;

		when(tagDao.create(tag)).thenReturn(expectedTagId);

		Long actualTagId = tagService.create(tag);

		Assert.assertEquals(expectedTagId, actualTagId);

		verify(tagDao, times(1)).create(tag);
	}

	/**
	 * Tests read by id operation.
	 * 
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void readTag() throws ServiceException, DaoException {

		Tag expectedTag = new Tag();
		expectedTag.setTagName("tag");
		Long tagId = 1L;

		when(tagDao.read(tagId)).thenReturn(expectedTag);

		Tag actualTag = tagService.read(tagId);

		Assert.assertEquals(expectedTag, actualTag);

		verify(tagDao, times(1)).read(tagId);
	}

	/**
	 * Tests update operation.
	 * 
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void updateTag() throws ServiceException, DaoException {

		Tag tag = new Tag();
		tag.setTagName("taGGG");

		tagService.update(tag);

		verify(tagDao).update(tag);
	}

	/**
	 * Tests delete operation.
	 * 
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void deleteTag() throws ServiceException, DaoException {

		Long tagId = 1L;

		tagService.delete(tagId);

		verify(tagDao).delete(tagId);
	}

	/**
	 * Tests read all operation.
	 * 
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void readAllTags() throws ServiceException, DaoException {

		List<Tag> expectedTagsList = new ArrayList<>();
		Tag tag1 = new Tag();
		tag1.setTagName("tag1");
		Tag tag2 = new Tag();
		tag2.setTagName("tag2");
		expectedTagsList.add(tag1);
		expectedTagsList.add(tag2);

		int expectedCountTags = expectedTagsList.size();

		when(tagDao.readAll()).thenReturn(expectedTagsList);

		List<Tag> actualTagsList = tagService.readAll();
		int actualCountTags = actualTagsList.size();

		Assert.assertEquals(expectedCountTags, actualCountTags);
		Assert.assertEquals(expectedTagsList, actualTagsList);

		verify(tagDao, times(1)).readAll();
	}
}

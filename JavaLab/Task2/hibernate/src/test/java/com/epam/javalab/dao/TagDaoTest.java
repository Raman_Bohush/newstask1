package com.epam.javalab.dao;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.javalab.domain.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * Tests Tag Data Access Object using DBUnit framework.
 * 
 * @author Roma
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:springTest-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup("/dao/databaseDataset.xml")
public class TagDaoTest {

	/**
	 * Tag Data Access Object.
	 */
	@Autowired
	private ITagDao tagDao;

	/**
	 * Tests create operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void createTag() throws Exception {

		String tagName = "Bereza";

		Tag expectedTag = getTag(tagName);

		Long tagId = tagDao.create(expectedTag);

		Tag actualTag = tagDao.read(tagId);

		assertAuthorEqualsIgnoringID(expectedTag, actualTag);
	}

	/**
	 * Tests read by id operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void readTagById() throws Exception {

		Long tagId = 3L;
		String tagName = "Medicine";

		Tag expectedTag = getTag(tagId, tagName);

		Tag actualTag = tagDao.read(tagId);

		assertTagEquals(expectedTag, actualTag);
	}

	/**
	 * Tests update operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void updateTag() throws Exception {

		Long tagId = 3L;
		String tagName = "Auto";

		Tag expectedTag = getTag(tagId, tagName);

		tagDao.update(expectedTag);

		Tag actualTag = tagDao.read(tagId);

		assertTagEquals(expectedTag, actualTag);
	}

	/**
	 * Tests delete operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void deleteTag() throws Exception {

		Long tagId = 1L;
		tagDao.delete(tagId);

		Tag actualTag = tagDao.read(tagId);

		Assert.assertNull(actualTag);

	}

	/**
	 * Tests read all operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void getAllTags() throws Exception {

		List<Tag> tagsList = tagDao.readAll();
		int actualCountTags = tagsList.size();
		int expectedCountTags = 3;

		Assert.assertEquals(expectedCountTags, actualCountTags);
	}

	/**
	 * Builds tag and return him.
	 * 
	 * @param tagId
	 *            tag ID
	 * @param tagName
	 *            tag name
	 * @return created tag
	 */
	private Tag getTag(Long tagId, String tagName) {

		Tag tag = new Tag();
		tag.setTagId(tagId);
		tag.setTagName(tagName);
		return tag;
	}

	/**
	 * Builds tag without ID and return him.
	 * 
	 * @param tagId
	 *            tag ID
	 * @param tagName
	 *            tag name
	 * @return created tag
	 */
	private Tag getTag(String tagName) {

		Tag tag = new Tag();
		tag.setTagName(tagName);
		return tag;
	}

	/**
	 * Asserts that two tags are equal.
	 * 
	 * @param expectedTag
	 *            expected tag
	 * @param actualTag
	 *            actual tag
	 */
	private void assertTagEquals(Tag expectedTag, Tag actualTag) {

		Assert.assertEquals(expectedTag.getTagId(), actualTag.getTagId());
		Assert.assertEquals(expectedTag.getTagName(), actualTag.getTagName());
	}

	/**
	 * Asserts that two tags are equal ignoring tag ID.
	 * 
	 * @param expectedTag
	 *            expected tag
	 * @param actualTag
	 *            actual tag
	 */
	private void assertAuthorEqualsIgnoringID(Tag expectedTag, Tag actualTag) {

		Assert.assertEquals(expectedTag.getTagName(), actualTag.getTagName());
	}
}

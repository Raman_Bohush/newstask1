package com.epam.javalab.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.javalab.dao.impl.AuthorDaoImpl;
import com.epam.javalab.domain.Author;
import com.epam.javalab.exception.DaoException;
import com.epam.javalab.exception.ServiceException;
import com.epam.javalab.service.impl.AuthorServiceImpl;

/**
 * Tests Author Service using mocking framework - Mockito.
 * 
 * @author Roma
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {

	/**
	 * Author Data Access Object is mock.
	 */
	@Mock
	private AuthorDaoImpl authorDao;

	/**
	 * Author Service.
	 */
	@InjectMocks
	private AuthorServiceImpl authorService;

	/**
	 * Tests create operation.
	 * 
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void createAuthor() throws ServiceException, DaoException {

		Author author = new Author();
		author.setName("name");
		Long expectedAuthorId = 3L;

		when(authorDao.create(author)).thenReturn(expectedAuthorId);

		Long actualAuthorId = authorService.create(author);

		Assert.assertEquals(expectedAuthorId, actualAuthorId);

		verify(authorDao, times(1)).create(author);

	}

	/**
	 * Tests read by id operation.
	 * 
	 * @throws DaoException
	 * @throws ServiceException
	 */
	@Test
	public void readAuthor() throws DaoException, ServiceException {

		Author expectedAuthor = new Author();
		expectedAuthor.setName("name");
		Long authorId = 1L;

		when(authorDao.read(authorId)).thenReturn(expectedAuthor);

		Author actualAuthor = authorService.read(authorId);

		Assert.assertEquals(expectedAuthor, actualAuthor);

		verify(authorDao, times(1)).read(authorId);
	}

	/**
	 * Tests update operation.
	 * 
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void updateAuthor() throws ServiceException, DaoException {

		Author author = new Author();
		author.setName("name");

		authorService.update(author);

		verify(authorDao, times(1)).update(author);
	}

	/**
	 * Tests delete operation.
	 * 
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void deleteAuthor() throws ServiceException, DaoException {

		Long authorId = 2L;

		authorService.delete(authorId);

		verify(authorDao, times(1)).delete(authorId);
	}

	/**
	 * Tests read all operation.
	 * 
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void readAllAuthors() throws DaoException, ServiceException {

		List<Author> expectedAuthorsList = new ArrayList<>();
		Author author1 = new Author();
		author1.setName("roma");
		Author author2 = new Author();
		author2.setName("lena");
		expectedAuthorsList.add(author1);
		expectedAuthorsList.add(author2);
		int expectedCountAuthors = expectedAuthorsList.size();

		when(authorDao.readAll()).thenReturn(expectedAuthorsList);

		List<Author> actualAuthorsList = authorService.readAll();
		int actualCountAuthors = actualAuthorsList.size();

		Assert.assertEquals(expectedCountAuthors, actualCountAuthors);
		Assert.assertEquals(expectedAuthorsList, actualAuthorsList);

		verify(authorDao, times(1)).readAll();

	}
}

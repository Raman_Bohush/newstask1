package com.epam.javalab.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.javalab.dao.impl.CommentDaoImpl;
import com.epam.javalab.domain.Comment;
import com.epam.javalab.exception.DaoException;
import com.epam.javalab.exception.ServiceException;
import com.epam.javalab.service.impl.CommentServiceImpl;

/**
 * Tests Comment Service using mocking framework - Mockito.
 * 
 * @author Roma
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {

	/**
	 * Comment Data Access Object is mock.
	 */
	@Mock
	private CommentDaoImpl commentDao;

	/**
	 * Comment Service.
	 */
	@InjectMocks
	private CommentServiceImpl commentService;

	/**
	 * Tests create operation.
	 * 
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void createComment() throws ServiceException, DaoException {

		Comment comment = new Comment();
		comment.setCommentText("text");
		Long expectedCommentId = 2L;

		when(commentDao.create(comment)).thenReturn(expectedCommentId);

		Long actualCommentId = commentService.create(comment);

		Assert.assertEquals(expectedCommentId, actualCommentId);

		verify(commentDao, times(1)).create(comment);
	}

	/**
	 * Tests read by id operation.
	 * 
	 * @throws DaoException
	 * @throws ServiceException
	 */
	@Test
	public void readComment() throws ServiceException, DaoException {

		Comment expectedComment = new Comment();
		expectedComment.setCommentText("text");
		Long commentId = 1L;

		when(commentDao.read(commentId)).thenReturn(expectedComment);

		Comment actualComment = commentService.read(commentId);

		Assert.assertEquals(expectedComment, actualComment);

		verify(commentDao, times(1)).read(commentId);
	}

	/**
	 * Tests update operation.
	 * 
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void updateComment() throws ServiceException, DaoException {

		Comment comment = new Comment();
		comment.setCommentText("text");

		commentService.update(comment);

		verify(commentDao).update(comment);
	}

	/**
	 * Tests delete operation.
	 * 
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void deleteComment() throws ServiceException, DaoException {

		Long commentId = 2L;

		commentService.delete(commentId);

		verify(commentDao).delete(commentId);
	}

	/**
	 * Tests read all operation.
	 * 
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void readAllComments() throws ServiceException, DaoException {

		List<Comment> expectedCommentsList = new ArrayList<>();
		Comment comment1 = new Comment();
		comment1.setCommentText("text1");
		Comment comment2 = new Comment();
		comment2.setCommentText("text2");
		expectedCommentsList.add(comment1);
		expectedCommentsList.add(comment2);

		int expectedCountComments = expectedCommentsList.size();

		when(commentDao.readAll()).thenReturn(expectedCommentsList);

		List<Comment> actualCommentsList = commentService.readAll();
		int actualCountComments = actualCommentsList.size();

		Assert.assertEquals(expectedCountComments, actualCountComments);
		Assert.assertEquals(expectedCommentsList, actualCommentsList);

		verify(commentDao, times(1)).readAll();
	}
}

package com.epam.javalab.dao;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.javalab.dao.impl.CommentDaoImpl;
import com.epam.javalab.domain.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * Tests Comment Data Access Object using DBUnit framework.
 * 
 * @author Roma
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:dataSourceTest-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup("/dao/databaseDataset.xml")
public class CommentDaoTest {

	/**
	 * Comment Data Access Object.
	 */
	@Autowired
	private CommentDaoImpl commentDao;

	/**
	 * Tests create operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void createComment() throws Exception {

		Long newsId = 1L;
		String commentText = "big com_text1";
		Date creationDate = Timestamp.valueOf("2015-08-17 11:11:11");

		Comment expectedComment = getComment(newsId, commentText, creationDate);

		Long commentId = commentDao.create(expectedComment);

		Comment actualComment = commentDao.read(commentId);

		assertCommentEqualsIgnoringID(expectedComment, actualComment);
	}

	/**
	 * Tests read by id operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void readCommentById() throws Exception {

		Long commentId = 2L;
		Long newsId = 2L;
		String commentText = "com_text2";
		Date creationDate = Timestamp.valueOf("2015-04-17 15:23:45");

		Comment expectedComment = getComment(commentId, newsId, commentText, creationDate);

		Comment actualComment = commentDao.read(commentId);

		assertCommentEquals(expectedComment, actualComment);
	}

	/**
	 * Tests update operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void updateComment() throws Exception {

		Long commentId = 1L;
		Long newsId = 1L;
		String commentText = "bigg com_text1";
		Date creationDate = Timestamp.valueOf("2015-08-17 11:11:11");

		Comment expectedComment = getComment(commentId, newsId, commentText, creationDate);

		commentDao.update(expectedComment);

		Comment actualComment = commentDao.read(commentId);

		assertCommentEquals(expectedComment, actualComment);

	}

	/**
	 * Tests delete operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void deleteComment() throws Exception {

		Long commentId = 1L;
		commentDao.delete(commentId);

		Comment actualComment = commentDao.read(commentId);

		Assert.assertNull(actualComment);

	}

	/**
	 * Tests read all operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void getAllComments() throws Exception {

		List<Comment> commentsList = commentDao.readAll();
		int actualCountComments = commentsList.size();
		int expectedCountComments = 3;

		Assert.assertEquals(expectedCountComments, actualCountComments);
	}

	/**
	 * Builds comment and returns him.
	 * 
	 * @param commentId
	 *            comment ID
	 * @param newsId
	 *            news ID
	 * @param commentText
	 *            comment text
	 * @param creationDate
	 *            comment creation date
	 * @return created comment
	 */
	private Comment getComment(Long commentId, Long newsId, String commentText, Date creationDate) {

		Comment comment = new Comment();
		comment.setCommentId(commentId);
		comment.setNewsId(newsId);
		comment.setCommentText(commentText);
		comment.setCreationDate(creationDate);

		return comment;
	}

	/**
	 * Builds comment without comment Id and returns him.
	 * 
	 * @param newsId
	 *            news ID
	 * @param commentText
	 *            comment text
	 * @param creationDate
	 *            comment creation date
	 * @return created comment
	 */
	private Comment getComment(Long newsId, String commentText, Date creationDate) {

		Comment comment = new Comment();
		comment.setNewsId(newsId);
		comment.setCommentText(commentText);
		comment.setCreationDate(creationDate);

		return comment;
	}

	/**
	 * Asserts that two comments are equal.
	 * 
	 * @param expectedComment
	 *            expected comment
	 * @param actualComment
	 *            actual comment
	 */
	private void assertCommentEquals(Comment expectedComment, Comment actualComment) {

		Assert.assertEquals(expectedComment.getCommentId(), actualComment.getCommentId());
		Assert.assertEquals(expectedComment.getNewsId(), actualComment.getNewsId());
		Assert.assertEquals(expectedComment.getCommentText(), actualComment.getCommentText());
		Assert.assertEquals(expectedComment.getCreationDate(), actualComment.getCreationDate());
	}

	/**
	 * Asserts that two comments are equal ignoring comment ID.
	 * 
	 * @param expectedComment
	 *            expected comment
	 * @param actualComment
	 *            actual comment
	 */
	private void assertCommentEqualsIgnoringID(Comment expectedComment, Comment actualComment) {

		Assert.assertEquals(expectedComment.getNewsId(), actualComment.getNewsId());
		Assert.assertEquals(expectedComment.getCommentText(), actualComment.getCommentText());
		Assert.assertEquals(expectedComment.getCreationDate(), actualComment.getCreationDate());
	}
}

package com.epam.javalab.dao;

import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import com.epam.javalab.dao.impl.AuthorDaoImpl;
import com.epam.javalab.domain.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * Tests Author Data Access Object using DBUnit framework.
 * 
 * @author Roma
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:dataSourceTest-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup("/dao/databaseDataset.xml")
public class AuthorDaoTest {

	/**
	 * Author Data Access Object.
	 */
	@Autowired
	private AuthorDaoImpl authorDao;

	/**
	 * Tests create operation.
	 * 
	 * @throws Exception
	 */

	@Test
	public void createAuthor() throws Exception {

		String authorName = "Sveta";
		Date expired = null;

		Author expectedAuthor = getAuthor(authorName, expired);

		Long authorId = authorDao.create(expectedAuthor);

		Author actualAuthor = authorDao.read(authorId);

		assertAuthorEqualsIgnoringID(expectedAuthor, actualAuthor);

	}

	/**
	 * Tests read by id operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void readAuthorById() throws Exception {

		Long authorId = 2L;
		String authorName = "Masha";
		Date expired = null;

		Author expectedAuthor = getAuthor(authorId, authorName, expired);

		Author actualAuthor = authorDao.read(authorId);

		assertAuthorEquals(expectedAuthor, actualAuthor);
	}

	/**
	 * Tests update operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void updateAuthor() throws Exception {

		Long authorId = 2L;
		String authorName = "Mashaaaaaaa";
		Date expired = null;

		Author expectedAuthor = getAuthor(authorId, authorName, expired);

		authorDao.update(expectedAuthor);

		Author actualAuthor = authorDao.read(authorId);

		assertAuthorEquals(expectedAuthor, actualAuthor);

	}

	/**
	 * Tests delete operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void deleteAuthor() throws Exception {

		Long authorId = 1L;
		authorDao.delete(authorId);

		Author actualAuthor = authorDao.read(authorId);

		Assert.assertNull(actualAuthor);
	}

	/**
	 * Tests read all operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void getAllAuthors() throws Exception {

		List<Author> authorsList = authorDao.readAll();
		int actualCountAuthors = authorsList.size();
		int expectedCountAuthors = 3;

		Assert.assertEquals(expectedCountAuthors, actualCountAuthors);
	}

	/**
	 * Builds author and returns him.
	 * 
	 * @param authorId
	 *            author ID
	 * @param authorName
	 *            author name
	 * @param expired
	 *            expired date
	 * @return created author
	 */
	private Author getAuthor(Long authorId, String authorName, Date expired) {

		Author author = new Author();
		author.setAuthorId(authorId);
		author.setName(authorName);
		author.setExpired(expired);
		return author;
	}

	/**
	 * Builds author without ID and returns him.
	 * 
	 * @param authorName
	 *            author name
	 * @param expired
	 *            expired date
	 * @return created author
	 */
	private Author getAuthor(String authorName, Date expired) {

		Author author = new Author();
		author.setName(authorName);
		author.setExpired(expired);
		return author;
	}

	/**
	 * Asserts that two authors are equal.
	 * 
	 * @param expectedAuthor
	 *            expected author
	 * @param actualAuthor
	 *            actual author
	 */
	private void assertAuthorEquals(Author expectedAuthor, Author actualAuthor) {

		Assert.assertEquals(expectedAuthor.getAuthorId(), actualAuthor.getAuthorId());
		Assert.assertEquals(expectedAuthor.getName(), actualAuthor.getName());
		Assert.assertEquals(expectedAuthor.getExpired(), actualAuthor.getExpired());
	}

	/**
	 * Asserts that two authors are equal ignoring author ID.
	 * 
	 * @param expectedAuthor
	 *            expected author
	 * @param actualAuthor
	 *            actual author
	 */
	private void assertAuthorEqualsIgnoringID(Author expectedAuthor, Author actualAuthor) {

		Assert.assertEquals(expectedAuthor.getName(), actualAuthor.getName());
		Assert.assertEquals(expectedAuthor.getExpired(), actualAuthor.getExpired());
	}

}

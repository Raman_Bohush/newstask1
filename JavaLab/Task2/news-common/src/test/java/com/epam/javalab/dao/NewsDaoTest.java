package com.epam.javalab.dao;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.javalab.dao.impl.NewsDaoImpl;
import com.epam.javalab.domain.News;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

/**
 * Tests News Data Access Object using DBUnit framework.
 * 
 * @author Roma
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:dataSourceTest-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DatabaseSetup("/dao/databaseDataset.xml")
public class NewsDaoTest {

	private static final String NEWS_AUTHOR_TABLE = "news_author";
	private static final String NEWS_TAG_TABLE = "news_tag";
	/**
	 * News Data Access Object.
	 */
	@Autowired
	private NewsDaoImpl newsDao;

	/**
	 * Tests create operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void createNews() throws Exception {

		String newsTitle = "Windows";
		String newsShortText = "short4";
		String newsFullText = "full4";
		Date newsCreationDate = Timestamp.valueOf("2015-08-17 11:11:11");
		Date newsModificationDate = new SimpleDateFormat("yyyy-MM-dd").parse("2015-09-17");

		News expectedNews = getNews(newsTitle, newsShortText, newsFullText, newsCreationDate, newsModificationDate);

		Long newsId = newsDao.create(expectedNews);

		News actualNews = newsDao.read(newsId);

		assertNewsEqualsIgnoringID(expectedNews, actualNews);
	}

	/**
	 * Tests read by id operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void readNewsById() throws Exception {

		Long newsId = 2L;
		String newsTitle = "Programming";
		String newsShortText = "short2";
		String newsFullText = "full2";
		Date newsCreationDate = Timestamp.valueOf("2015-04-17 15:23:45");
		Date newsModificationDate = new SimpleDateFormat("yyyy-MM-dd").parse("2015-09-17");

		News expectedNews = getNews(newsId, newsTitle, newsShortText, newsFullText, newsCreationDate,
				newsModificationDate);

		News actualNews = newsDao.read(newsId);

		assertNewsEquals(expectedNews, actualNews);
	}

	/**
	 * Tests update operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void updateNews() throws Exception {

		Long newsId = 2L;
		String newsTitle = "Programming 20";
		String newsShortText = "short2";
		String newsFullText = "full2";
		Date newsCreationDate = Timestamp.valueOf("2015-04-17 15:23:45");
		Date newsModificationDate = new SimpleDateFormat("yyyy-MM-dd").parse("2015-09-17");

		News expectedNews = getNews(newsId, newsTitle, newsShortText, newsFullText, newsCreationDate,
				newsModificationDate);

		newsDao.update(expectedNews);

		News actualNews = newsDao.read(newsId);

		assertNewsEquals(expectedNews, actualNews);
	}

	/**
	 * Tests delete operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void deleteNews() throws Exception {

		Long newsId = 2L;
		newsDao.delete(newsId);

		News actualNews = newsDao.read(newsId);

		Assert.assertNull(actualNews);
	}

	/**
	 * Tests read all operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void getAllNews() throws Exception {

		List<News> newsList = newsDao.readAll();
		int actualCountNews = newsList.size();
		int expectedCountNews = 5;

		Assert.assertEquals(expectedCountNews, actualCountNews);
	}

	/**
	 * Tests search news operation.
	 * 
	 * @throws Exception
	 */
	/*
	 * @Test public void searchNews() throws Exception {
	 * 
	 * SearchCriteria searchCriteria = new SearchCriteria();
	 * 
	 * Long authorId = 2L; List<Long> tagsIdList = new ArrayList<>(); Integer countNewsOnPage = 3; Integer numPage = 3;
	 * 
	 * tagsIdList.add(3L); tagsIdList.add(2L);
	 * 
	 * searchCriteria.setAuthorId(authorId); searchCriteria.setTagsIdList(tagsIdList);
	 * 
	 * List<NewsVO> newsList = newsDao.searchNews(searchCriteria, countNewsOnPage, numPage); int actualCountNews =
	 * newsList.size(); int expectedCountNews = 1;
	 * 
	 * Assert.assertEquals(expectedCountNews, actualCountNews);
	 * 
	 * }
	 */

	/**
	 * Tests sorting news by comments operation.
	 * 
	 * @throws Exception
	 */
	/*
	 * @Test public void getSortedNews() throws Exception {
	 * 
	 * List<NewsVO> newsList = newsDao.getSortedNews(3, 1); int actualCountNews = newsList.size(); int expectedCountNews
	 * = 3;
	 * 
	 * Assert.assertEquals(expectedCountNews, actualCountNews);
	 * 
	 * String expectedTitle0 = "Programming"; String expectedTitle1 = "Football"; String expectedTitle2 = "Belarus";
	 * 
	 * String actualTitle0 = newsList.get(0).getNews().getTitle(); String actualTitle1 =
	 * newsList.get(1).getNews().getTitle(); String actualTitle2 = newsList.get(2).getNews().getTitle();
	 * 
	 * Assert.assertEquals(expectedTitle0, actualTitle0); Assert.assertEquals(expectedTitle1, actualTitle1);
	 * Assert.assertEquals(expectedTitle2, actualTitle2);
	 * 
	 * }
	 */

	/**
	 * Tests add author for news operation.
	 * 
	 * @throws Exception
	 */
	@ExpectedDatabase(value = "/dao/expectedAddForNewsDataset.xml", table = NEWS_AUTHOR_TABLE, assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void addAuthorForNews() throws Exception {

		Long newsId = 4L;
		Long authorId = 4L;

		newsDao.addAuthorForNews(newsId, authorId);

	}

	/**
	 * Tests add tag for news operation.
	 * 
	 * @throws Exception
	 */
	@ExpectedDatabase(value = "/dao/expectedAddForNewsDataset.xml", table = NEWS_TAG_TABLE, assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void addTagForNews() throws Exception {

		Long newsId = 4L;
		Long tagId = 4L;
		newsDao.addTagForNews(newsId, tagId);

	}

	/**
	 * Builds news and returns her.
	 * 
	 * @param newsId
	 *            news ID
	 * @param newsTitle
	 *            news title
	 * @param newsShortText
	 *            news short text
	 * @param newsFullText
	 *            news full text
	 * @param newsCreationDate
	 *            news creation date
	 * @param newsModificationDate
	 *            news modification date
	 * @return created news
	 */
	private News getNews(Long newsId, String newsTitle, String newsShortText, String newsFullText,
			Date newsCreationDate, Date newsModificationDate) {

		News news = new News();
		news.setNewsId(newsId);
		news.setTitle(newsTitle);
		news.setShortText(newsShortText);
		news.setFullText(newsFullText);
		news.setCreationDate(newsCreationDate);
		news.setModificationDate(newsModificationDate);
		return news;
	}

	/**
	 * Builds news without ID and returns her.
	 * 
	 * @param newsTitle
	 *            news title
	 * @param newsShortText
	 *            news short text
	 * @param newsFullText
	 *            news full text
	 * @param newsCreationDate
	 *            news creation date
	 * @param newsModificationDate
	 *            news modification date
	 * @return created news
	 */
	private News getNews(String newsTitle, String newsShortText, String newsFullText, Date newsCreationDate,
			Date newsModificationDate) {

		News news = new News();
		news.setTitle(newsTitle);
		news.setShortText(newsShortText);
		news.setFullText(newsFullText);
		news.setCreationDate(newsCreationDate);
		news.setModificationDate(newsModificationDate);
		return news;
	}

	/**
	 * Asserts that two news are equal.
	 * 
	 * @param expectedNews
	 *            expected news
	 * @param actualNews
	 *            actual news
	 */
	private void assertNewsEquals(News expectedNews, News actualNews) {

		Assert.assertEquals(expectedNews.getNewsId(), actualNews.getNewsId());
		Assert.assertEquals(expectedNews.getTitle(), actualNews.getTitle());
		Assert.assertEquals(expectedNews.getShortText(), actualNews.getShortText());
		Assert.assertEquals(expectedNews.getFullText(), actualNews.getFullText());
		Assert.assertEquals(expectedNews.getCreationDate(), actualNews.getCreationDate());
		Assert.assertEquals(expectedNews.getModificationDate(), actualNews.getModificationDate());
	}

	/**
	 * Asserts that two news are equal ignoring news ID.
	 * 
	 * @param expectedNews
	 *            expected news
	 * @param actualNews
	 *            actual news
	 */
	private void assertNewsEqualsIgnoringID(News expectedNews, News actualNews) {

		Assert.assertEquals(expectedNews.getTitle(), actualNews.getTitle());
		Assert.assertEquals(expectedNews.getShortText(), actualNews.getShortText());
		Assert.assertEquals(expectedNews.getFullText(), actualNews.getFullText());
		Assert.assertEquals(expectedNews.getCreationDate(), actualNews.getCreationDate());
		Assert.assertEquals(expectedNews.getModificationDate(), actualNews.getModificationDate());
	}

}

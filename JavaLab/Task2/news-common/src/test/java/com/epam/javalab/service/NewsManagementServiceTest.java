package com.epam.javalab.service;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.transaction.annotation.Transactional;

import com.epam.javalab.domain.Author;
import com.epam.javalab.domain.News;
import com.epam.javalab.domain.NewsVO;
import com.epam.javalab.domain.Tag;
import com.epam.javalab.exception.ServiceException;
import com.epam.javalab.service.impl.AuthorServiceImpl;
import com.epam.javalab.service.impl.CommentServiceImpl;
import com.epam.javalab.service.impl.NewsManagementServiceImpl;
import com.epam.javalab.service.impl.NewsServiceImpl;
import com.epam.javalab.service.impl.TagServiceImpl;

/**
 * Tests News Management Service using mocking framework - Mockito.
 * 
 * @author Raman_Bohush
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsManagementServiceTest {

	@Mock
	private AuthorServiceImpl authorService;

	@Mock
	private CommentServiceImpl commentService;

	@Mock
	private NewsServiceImpl newsService;

	@Mock
	private TagServiceImpl tagService;

	@InjectMocks
	private NewsManagementServiceImpl newsManagementService;

	/**
	 * Tests transactional save news operation.
	 * 
	 * @throws ServiceException
	 */
	@Transactional
	@Test
	public void saveNews() throws ServiceException {

		News news = new News();
		news.setNewsId(5L);
		Long expectedNewsId = news.getNewsId();
		Author author = new Author();
		author.setAuthorId(1L);
		Long expectedAuthorId = author.getAuthorId();
		List<Tag> tagsList = new ArrayList<>();
		Tag tag1 = new Tag();
		Tag tag2 = new Tag();
		tagsList.add(tag1);
		tagsList.add(tag2);
		int expectedCountTags = tagsList.size();
		NewsVO newsVO = new NewsVO();
		newsVO.setNews(news);
		newsVO.setAuthor(author);
		newsVO.setTagsList(tagsList);

		when(newsService.create(news)).thenReturn(news.getNewsId());

		newsManagementService.saveNews(newsVO);

		verify(newsService, times(1)).create(news);
		verify(newsService, times(1)).addAuthorForNews(expectedNewsId, expectedAuthorId);
		verify(newsService, times(expectedCountTags)).addTagForNews(anyLong(), anyLong());
	}
}

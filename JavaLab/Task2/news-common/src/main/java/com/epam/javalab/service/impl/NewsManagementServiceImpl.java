package com.epam.javalab.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.javalab.domain.Author;
import com.epam.javalab.domain.Comment;
import com.epam.javalab.domain.News;
import com.epam.javalab.domain.NewsVO;
import com.epam.javalab.domain.SearchCriteria;
import com.epam.javalab.domain.Tag;
import com.epam.javalab.exception.ServiceException;
import com.epam.javalab.service.IAuthorService;
import com.epam.javalab.service.ICommentService;
import com.epam.javalab.service.INewsManagementService;
import com.epam.javalab.service.INewsService;
import com.epam.javalab.service.ITagService;

@Service
public class NewsManagementServiceImpl implements INewsManagementService {

	@Autowired
	private IAuthorService authorService;

	@Autowired
	private INewsService newsService;

	@Autowired
	private ICommentService commentService;

	@Autowired
	private ITagService tagService;

	@Transactional(rollbackFor = Exception.class)
	@Override
	public Long saveNews(NewsVO newsVO) throws ServiceException {

		assertNotNull(newsVO, "NewsVO is empty.");

		News news = newsVO.getNews();
		Long newsId = newsService.create(news);
		Long authorId = newsVO.getAuthor().getAuthorId();
		List<Tag> tagsList = newsVO.getTagsList();
		Long tagId = null;

		newsService.addAuthorForNews(newsId, authorId);

		if (tagsList != null) {
			for (Tag tag : tagsList) {
				tagId = tag.getTagId();
				newsService.addTagForNews(newsId, tagId);
			}
		}

		return newsId;
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void deleteNews(List<Long> newsIdList) throws ServiceException {

		assertNotNull(newsIdList, "News news ID list is empty.");

		for (Long newsId : newsIdList) {
			commentService.deleteByNewsId(newsId);
			newsService.deleteAuthorNews(newsId);
			newsService.deleteTagNews(newsId);
			newsService.delete(newsId);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void updateNews(NewsVO newsVO) throws ServiceException {

		assertNotNull(newsVO, "NewsVO is empty.");

		News news = newsVO.getNews();
		Long newsId = news.getNewsId();
		Long authorId = newsVO.getAuthor().getAuthorId();
		List<Tag> tagsList = newsVO.getTagsList();
		Long tagId = null;

		newsService.update(news);
		newsService.updateAuthorNews(newsId, authorId);
		newsService.deleteTagNews(newsId);

		if (tagsList != null) {
			for (Tag tag : tagsList) {
				tagId = tag.getTagId();
				newsService.addTagForNews(newsId, tagId);
			}
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public NewsVO getSingleNewsVO(Long newsId) throws ServiceException {

		assertNotNull(newsId, "News ID is empty.");

		News news = null;
		Author author = null;
		List<Tag> tagsList = null;
		Long countComment = null;
		List<Comment> commentsList = null;
		NewsVO newsVO = null;

		news = newsService.read(newsId);

		assertNotNull(news, "News is not found.");

		newsVO = new NewsVO();
		author = newsService.getAuthorNews(newsId);
		tagsList = newsService.getTagsNews(newsId);
		commentsList = newsService.getCommentsNews(newsId);
		countComment = (long) commentsList.size();
		newsVO.setNews(news);
		newsVO.setAuthor(author);
		newsVO.setCommentsList(commentsList);
		newsVO.setCountComment(countComment);
		newsVO.setTagsList(tagsList);

		return newsVO;
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public List<NewsVO> searchNews(SearchCriteria searchCriteria, Integer numPage) throws ServiceException {

		assertNotNull(searchCriteria, "Search criteria is empty.");
		assertNotNull(numPage, "Number of page is empty.");

		List<NewsVO> newsVOList = new ArrayList<>();
		List<News> newsList = null;
		Long newsId = null;
		NewsVO newsVO = null;

		newsList = newsService.searchNews(searchCriteria, numPage);

		if (newsList.isEmpty()) {
			throw new ServiceException("News list is empty.");
		}

		for (News news : newsList) {
			newsId = news.getNewsId();
			newsVO = getSingleNewsVO(newsId);
			newsVOList.add(newsVO);
		}
		return newsVOList;

	}

	/**
	 * Asserts that object is not empty.
	 * 
	 * @param object
	 *            any object
	 * @param message
	 *            error message, that object is empty
	 * @throws ServiceException
	 */
	private void assertNotNull(Object object, String message) throws ServiceException {

		if (object == null) {
			throw new ServiceException(message);
		}
	}

}

package com.epam.javalab.domain;

import java.util.List;

/**
 * Describes the properties of the criteria for searching.
 * 
 * @author Raman_Bohush
 *
 */
public class SearchCriteria {

	/**
	 * Author's ID
	 */
	private Long authorId;

	/**
	 * List of the tags.
	 */
	private List<Long> tagsIdList;

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public List<Long> getTagsIdList() {
		return tagsIdList;
	}

	public void setTagsIdList(List<Long> tagsIdList) {
		this.tagsIdList = tagsIdList;
	}

}

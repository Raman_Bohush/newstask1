package com.epam.javalab.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.javalab.dao.IAuthorDao;
import com.epam.javalab.domain.Author;
import com.epam.javalab.exception.DaoException;
import com.epam.javalab.exception.ServiceException;
import com.epam.javalab.service.IAuthorService;

@Service
public class AuthorServiceImpl implements IAuthorService {

	private static Logger logger = Logger.getLogger(AuthorServiceImpl.class);

	@Autowired
	private IAuthorDao authorDao;

	@Override
	public Long create(Author author) throws ServiceException {

		Long authorId = null;

		assertNotNull(author, "Author is empty.");

		try {
			authorId = authorDao.create(author);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not create author.", e);
		}
		return authorId;

	}

	@Override
	public Author read(Long authorId) throws ServiceException {

		Author author = null;

		assertNotNull(authorId, "Author ID is empty.");

		try {
			author = authorDao.read(authorId);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not read author.", e);
		}
		return author;
	}

	@Override
	public void update(Author author) throws ServiceException {

		assertNotNull(author, "Author is empty.");

		try {
			authorDao.update(author);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not update author.", e);
		}

	}

	@Override
	public void delete(Long authorId) throws ServiceException {

		assertNotNull(authorId, "Author ID is empty.");

		try {
			authorDao.delete(authorId);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not delete author.", e);
		}

	}

	@Override
	public List<Author> readAll() throws ServiceException {

		List<Author> authorsList;
		try {
			authorsList = authorDao.readAll();
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not read all authors.", e);
		}
		return authorsList;
	}

	@Override
	public List<Author> getNotExpiredAuthors() throws ServiceException {

		List<Author> notExpectedAuthorsList;
		try {
			notExpectedAuthorsList = authorDao.getNotExpiredAuthors();
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not read not expired authors.", e);
		}
		return notExpectedAuthorsList;
	}

	/**
	 * Asserts that object is not empty.
	 * 
	 * @param object
	 *            any object
	 * @param message
	 *            error message, that object is empty
	 * @throws ServiceException
	 */
	private void assertNotNull(Object object, String message) throws ServiceException {

		if (object == null) {
			throw new ServiceException(message);
		}
	}
}

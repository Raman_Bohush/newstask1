package com.epam.javalab.domain;

import java.io.Serializable;

/**
 * Describes the properties of the table Tag.
 * 
 * @author Raman_Bohush
 *
 */
public class Tag implements Serializable {

	/**
	 * Serial version ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Property - tag ID (primary key).
	 */
	private Long tagId;

	/**
	 * Property - tag name.
	 */
	private String tagName;

	public Tag() {
	}

	public Tag(Long tagId, String tagName) {

		this.tagId = tagId;
		this.tagName = tagName;
	}

	public Long getTagId() {
		return tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {

		this.tagName = tagName.trim();
	}

	@Override
	public int hashCode() {
		final int primeNumber = 47;
		int result = 1;
		result = result * primeNumber + ((tagId == null) ? 0 : tagId.hashCode());
		result = result * primeNumber + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Tag otherTag = (Tag) obj;
		if (tagId == null) {
			if (otherTag.tagId != null) {
				return false;
			}
		} else {
			return tagId.equals(otherTag.tagId);
		}
		if (tagName == null) {
			if (otherTag.tagName != null) {
				return false;
			}
		} else {
			return tagName.equals(otherTag.tagName);
		}
		return true;
	}

	@Override
	public String toString() {
		return "Tag [tagId=" + tagId + ", tagName=" + tagName + "]";
	}

}

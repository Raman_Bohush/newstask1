package com.epam.javalab.service;

import com.epam.javalab.domain.Comment;
import com.epam.javalab.exception.ServiceException;

/**
 * Comment Service interface. Set of additional operations for comment object and coordinates the application's response
 * in each operation.
 * 
 * @author Roma
 *
 */
public interface ICommentService extends IGenericService<Comment> {

	/**
	 * Invokes delete by news ID operation from Data Access Object layer. Delete comments by news ID from database.
	 * Checks parameter to delete.
	 * 
	 * @param id
	 *            the id
	 * @throws ServiceException
	 */
	public void deleteByNewsId(Long newsId) throws ServiceException;

	/**
	 * Invokes get news ID by comment ID operation from Data Access Object layer.
	 * 
	 * @param commentId
	 *            comment ID
	 * @return news ID
	 * @throws ServiceException
	 */
	public Long getNewsIdByCommentId(Long commentId) throws ServiceException;
}

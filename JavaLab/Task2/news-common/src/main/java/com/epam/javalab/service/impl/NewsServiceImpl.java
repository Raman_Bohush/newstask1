package com.epam.javalab.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.javalab.dao.INewsDao;
import com.epam.javalab.domain.Author;
import com.epam.javalab.domain.Comment;
import com.epam.javalab.domain.News;
import com.epam.javalab.domain.SearchCriteria;
import com.epam.javalab.domain.Tag;
import com.epam.javalab.exception.DaoException;
import com.epam.javalab.exception.ServiceException;
import com.epam.javalab.service.INewsService;

@Service
public class NewsServiceImpl implements INewsService {

	private static Logger logger = Logger.getLogger(NewsServiceImpl.class);

	@Autowired
	private INewsDao newsDao;

	@Override
	public Long create(News news) throws ServiceException {

		Long newsId = null;

		assertNotNull(news, "News is empty.");

		try {
			newsId = newsDao.create(news);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not create news.", e);
		}
		return newsId;

	}

	@Override
	public News read(Long newsId) throws ServiceException {

		News news = null;

		assertNotNull(newsId, "News ID is empty.");

		try {
			news = newsDao.read(newsId);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not read news.", e);
		}
		return news;
	}

	@Override
	public void update(News news) throws ServiceException {

		assertNotNull(news, "News is empty.");

		try {
			newsDao.update(news);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not update news.", e);
		}

	}

	@Override
	public void delete(Long newsId) throws ServiceException {

		assertNotNull(newsId, "News ID is empty.");

		try {
			newsDao.delete(newsId);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not delete news.", e);
		}

	}

	@Override
	public List<News> readAll() throws ServiceException {

		List<News> newsList;
		try {
			newsList = newsDao.readAll();

		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not read all news.", e);
		}
		return newsList;
	}

	@Override
	public List<News> searchNews(SearchCriteria searchCriteria, Integer numPage) throws ServiceException {

		List<News> newsList = null;

		assertNotNull(searchCriteria, "Search criteria is empty.");

		try {
			newsList = newsDao.searchNews(searchCriteria, numPage);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not search news by criteria.", e);
		}
		return newsList;
	}

	@Override
	public void addAuthorForNews(Long newsId, Long authorId) throws ServiceException {

		assertNotNull(newsId, "News ID is empty.");

		assertNotNull(authorId, "Author ID is empty.");

		try {
			newsDao.addAuthorForNews(newsId, authorId);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not add author for news.", e);
		}

	}

	@Override
	public void addTagForNews(Long newsId, Long tagId) throws ServiceException {

		assertNotNull(newsId, "News ID is empty.");

		assertNotNull(tagId, "Tag ID is empty.");

		try {
			newsDao.addTagForNews(newsId, tagId);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not add tag for news.", e);
		}

	}

	@Override
	public void updateAuthorNews(Long newsId, Long authorId) throws ServiceException {

		assertNotNull(newsId, "News ID is empty.");

		assertNotNull(authorId, "Author ID is empty.");

		try {
			newsDao.updateAuthorNews(newsId, authorId);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not update author news.", e);
		}
	}

	@Override
	public void updateTagsNews(Long newsId, Long tagId) throws ServiceException {

		assertNotNull(newsId, "News ID is empty.");

		assertNotNull(tagId, "Tag ID is empty.");

		try {
			newsDao.updateTagNews(newsId, tagId);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not update tags news.", e);
		}
	}

	@Override
	public void deleteAuthorNews(Long newsId) throws ServiceException {

		assertNotNull(newsId, "News ID is empty.");

		try {
			newsDao.deleteAuthorNews(newsId);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not delete relation between author and news.", e);
		}

	}

	@Override
	public void deleteTagNews(Long newsId) throws ServiceException {

		assertNotNull(newsId, "News ID is empty.");

		try {
			newsDao.deleteTagNews(newsId);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not delete relation between tag and news.", e);
		}

	}

	@Override
	public Long getCountNews(SearchCriteria searchCriteria) throws ServiceException {

		Long count = null;
		try {
			count = newsDao.getCountNews(searchCriteria);
		} catch (DaoException e) {
			throw new ServiceException("Can not get count pages.", e);
		}

		return count;
	}

	@Override
	public Long getNextNews(SearchCriteria searchCriteria, Long newsId) throws ServiceException {

		Long nextNewsId = null;

		assertNotNull(searchCriteria, "Search criteria is empty.");
		assertNotNull(newsId, "News ID is empty.");

		try {
			nextNewsId = newsDao.getNextNews(searchCriteria, newsId);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not get next news");
		}

		return nextNewsId;

	}

	@Override
	public Long getPreviousNews(SearchCriteria searchCriteria, Long newsId) throws ServiceException {

		Long previousNewsId = null;

		assertNotNull(searchCriteria, "Search criteria is empty.");
		assertNotNull(newsId, "News ID is rrr.");

		try {
			previousNewsId = newsDao.getPreviousNews(searchCriteria, newsId);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not get previous news");
		}

		return previousNewsId;

	}

	@Override
	public List<Comment> getCommentsNews(Long newsId) throws ServiceException {

		List<Comment> commentsList = null;
		assertNotNull(newsId, "News ID is empty.");

		try {
			commentsList = newsDao.getCommentsNews(newsId);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not get list of comments");
		}
		return commentsList;
	}

	@Override
	public List<Tag> getTagsNews(Long newsId) throws ServiceException {

		List<Tag> tagsList = null;
		assertNotNull(newsId, "News ID is empty.");

		try {
			tagsList = newsDao.getTagsNews(newsId);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not get list of tags");
		}
		return tagsList;
	}

	@Override
	public Author getAuthorNews(Long newsId) throws ServiceException {

		Author author = null;
		assertNotNull(newsId, "News ID is empty.");

		try {
			author = newsDao.getAuthorNews(newsId);
		} catch (DaoException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Can not get author news");
		}
		return author;
	}

	/**
	 * Asserts that object is not empty.
	 * 
	 * @param object
	 *            any object
	 * @param message
	 *            error message, that object is empty
	 * @throws ServiceException
	 */
	private void assertNotNull(Object object, String message) throws ServiceException {

		if (object == null) {
			throw new ServiceException(message);
		}
	}

}

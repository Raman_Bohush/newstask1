package com.epam.javalab.service;

import java.util.List;

import com.epam.javalab.domain.Author;
import com.epam.javalab.exception.ServiceException;

/**
 * Author Service interface. Set of additional operations for author object and coordinates the application's response
 * in each operation.
 * 
 * @author Roma
 *
 */
public interface IAuthorService extends IGenericService<Author> {

	public List<Author> getNotExpiredAuthors() throws ServiceException;
}

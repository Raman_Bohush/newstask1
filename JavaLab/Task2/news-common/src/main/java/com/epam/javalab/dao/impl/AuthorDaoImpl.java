package com.epam.javalab.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.javalab.dao.IAuthorDao;
import com.epam.javalab.domain.Author;
import com.epam.javalab.exception.DaoException;
import com.epam.javalab.util.DatabaseUtil;

/**
 * Author Repository Implements all operations with author object.
 * 
 * @author Roma
 *
 */
@Repository
public class AuthorDaoImpl implements IAuthorDao {

	/**
	 * Receives a connection.
	 */
	@Autowired
	private DataSource dataSource;

	/**
	 * SQL request for create record.
	 */
	private final static String CREATE_SQL = "INSERT INTO author (author_id, author_name, expired) "
			+ "VALUES(SEQ_AUTHOR_ID.nextVal, ?, ?)";

	/**
	 * SQL request for read record by id.
	 */
	private final static String READ_BY_ID_SQL = "SELECT author_id, author_name, expired " + "FROM author "
			+ "WHERE author_id=?";

	/**
	 * SQL request for update record.
	 */
	private final static String UPDATE_SQL = "UPDATE author " + "SET author_name=?, expired=? " + "WHERE author_id=?";

	/**
	 * SQL request for delete record.
	 */
	private final static String DELETE_SQL = "DELETE FROM author " + "WHERE author_id=?";

	/**
	 * SQL request for read all records.
	 */
	private final static String READ_ALL_SQL = "SELECT author_id, author_name, expired " + "FROM author";

	/**
	 * SQL request for read all not expired authors.
	 */
	private final static String READ_NOT_EXPIRED_SQL = "SELECT author_id, author_name, expired "
			+ "FROM author WHERE expired IS NULL";

	private final static String AUTHOR_ID = "author_id";

	private AuthorDaoImpl() {
	}

	@Override
	public Long create(Author author) throws DaoException {

		ResultSet rsKeys = null;
		Long authorId = null;
		String[] generatedKeys = { AUTHOR_ID };
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(CREATE_SQL, generatedKeys)) {
			ps.setString(1, author.getName());
			ps.setTimestamp(2, (author.getExpired() == null) ? null : new java.sql.Timestamp(author.getExpired()
					.getTime()));
			ps.executeUpdate();

			rsKeys = ps.getGeneratedKeys();
			if (rsKeys.next()) {
				authorId = rsKeys.getLong(1);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the create operation.", e);
		} finally {
			DatabaseUtil.closeResultSet(rsKeys);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return authorId;
	}

	@Override
	public Author read(Long authorId) throws DaoException {

		ResultSet rs = null;
		Author author = null;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(READ_BY_ID_SQL)) {
			ps.setLong(1, authorId);
			rs = ps.executeQuery();
			if (rs.next()) {
				author = buildAuthor(author, rs);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the read operation.", e);
		} finally {
			DatabaseUtil.closeResultSet(rs);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return author;
	}

	@Override
	public void update(Author author) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		// System.out.println(author);
		try (PreparedStatement ps = con.prepareStatement(UPDATE_SQL)) {
			ps.setString(1, author.getName());
			ps.setTimestamp(2, (author.getExpired() == null) ? null : new java.sql.Timestamp(author.getExpired()
					.getTime()));
			ps.setLong(3, author.getAuthorId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("SQL error in the update operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public void delete(Long authorId) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(DELETE_SQL)) {
			ps.setLong(1, authorId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("SQL error in the delete operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public List<Author> readAll() throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		List<Author> authorsList;
		Author author = null;

		try (Statement statement = con.createStatement(); ResultSet rs = statement.executeQuery(READ_ALL_SQL)) {
			authorsList = new ArrayList<>();
			while (rs.next()) {
				author = buildAuthor(author, rs);
				authorsList.add(author);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the read all operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return authorsList;
	}

	@Override
	public List<Author> getNotExpiredAuthors() throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		List<Author> notExpiredAuthorsList;
		Author author = null;

		try (Statement statement = con.createStatement(); ResultSet rs = statement.executeQuery(READ_NOT_EXPIRED_SQL)) {
			notExpiredAuthorsList = new ArrayList<>();
			while (rs.next()) {
				author = buildAuthor(author, rs);
				notExpiredAuthorsList.add(author);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the read not expexted operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}

		return notExpiredAuthorsList;
	}

	/**
	 * Builds author.
	 * 
	 * @param author
	 *            author
	 * @param rs
	 *            result set
	 * @return created author
	 * @throws SQLException
	 */
	private Author buildAuthor(Author author, ResultSet rs) throws SQLException {

		author = new Author();
		author.setAuthorId(rs.getLong(1));
		author.setName(rs.getString(2));
		author.setExpired(rs.getTimestamp(3));
		return author;
	}

}

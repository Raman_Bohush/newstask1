package com.epam.javalab.domain;

import java.io.Serializable;

/**
 * Describes the properties of the table Roles.
 * 
 * @author Raman_Bohush
 *
 */
public class Role implements Serializable {

	/**
	 * Serial version ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Property - user ID (foreign key).
	 */
	private Long userId;

	/**
	 * Property - rike name.
	 */
	private String roleName;

	public Role() {
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}

package com.epam.javalab.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.javalab.dao.ICommentDao;
import com.epam.javalab.domain.Comment;
import com.epam.javalab.exception.DaoException;
import com.epam.javalab.util.DatabaseUtil;

/**
 * Comment Repository Implements all operations with comment object.
 * 
 * @author Roma
 *
 */
@Repository
public class CommentDaoImpl implements ICommentDao {

	/**
	 * Receives a connection.
	 */
	@Autowired
	private DataSource dataSource;

	/**
	 * SQL request for create record.
	 */
	private final static String CREATE_SQL = "INSERT INTO comments (comment_id, news_id, comment_text, creation_date) "
			+ "VALUES(SEQ_COMMENTS_ID.nextVal, ?, ?, ?)";

	/**
	 * SQL request for read record by id.
	 */
	private final static String READ_BY_ID_SQL = "SELECT comment_id, news_id, comment_text, creation_date "
			+ "FROM comments " + "WHERE comment_id=?";

	/**
	 * SQL request for update record.
	 */
	private final static String UPDATE_SQL = "UPDATE comments " + "SET news_id=?, comment_text=?, creation_date=? "
			+ "WHERE comment_id=?";

	/**
	 * SQL request for delete record.
	 */
	private final static String DELETE_BY_ID_SQL = "DELETE FROM comments " + "WHERE comment_id=?";

	private final static String DELETE_BY_NEWS_ID_SQL = "DELETE FROM comments " + "WHERE news_id=?";

	/**
	 * SQL request for read all records.
	 */
	private final static String READ_ALL_SQL = "SELECT comment_id, news_id, comment_text, creation_date "
			+ "FROM comments";

	/**
	 * SQL request for get news ID by comment ID.
	 */
	private final static String GET_NEWS_ID_BY_COMMENT_ID_SQL = "SELECT news_id " + "FROM comments "
			+ "WHERE comment_id=?";

	private final static String COMMENT_ID = "comment_id";

	private CommentDaoImpl() {
	}

	@Override
	public Long create(Comment comment) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		ResultSet rsKey = null;
		Long commentId = null;
		String[] generatedKeys = { COMMENT_ID };
		try (PreparedStatement ps = con.prepareStatement(CREATE_SQL, generatedKeys)) {
			ps.setLong(1, comment.getNewsId());
			ps.setString(2, comment.getCommentText());
			ps.setTimestamp(3, new java.sql.Timestamp(comment.getCreationDate().getTime()));
			ps.executeUpdate();

			rsKey = ps.getGeneratedKeys();
			if (rsKey.next()) {
				commentId = rsKey.getLong(1);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the create operation.", e);
		} finally {
			DatabaseUtil.closeResultSet(rsKey);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return commentId;
	}

	@Override
	public Comment read(Long commentId) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		ResultSet rs = null;
		Comment comment = null;
		try (PreparedStatement ps = con.prepareStatement(READ_BY_ID_SQL)) {
			ps.setLong(1, commentId);
			rs = ps.executeQuery();
			if (rs.next()) {
				comment = buildComment(comment, rs);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the read operation.", e);
		} finally {
			DatabaseUtil.closeResultSet(rs);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return comment;
	}

	@Override
	public void update(Comment comment) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(UPDATE_SQL)) {
			ps.setLong(1, comment.getNewsId());
			ps.setString(2, comment.getCommentText());
			ps.setTimestamp(3, new java.sql.Timestamp(comment.getCreationDate().getTime()));
			ps.setLong(4, comment.getCommentId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("SQL error in the update operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public void delete(Long commentId) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(DELETE_BY_ID_SQL)) {
			ps.setLong(1, commentId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("SQL error in the delete operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public void deleteByNewsId(Long newsId) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(DELETE_BY_NEWS_ID_SQL)) {
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("SQL error in the delete by news ID operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public List<Comment> readAll() throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		List<Comment> commentsList = new ArrayList<Comment>();
		Comment comment = null;

		try (Statement statement = con.createStatement(); ResultSet rs = statement.executeQuery(READ_ALL_SQL)) {
			while (rs.next()) {
				comment = buildComment(comment, rs);
				commentsList.add(comment);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the read  all operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return commentsList;
	}

	@Override
	public Long getNewsIdByCommentId(Long commentId) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		ResultSet rs = null;
		Long newsId = null;
		try (PreparedStatement ps = con.prepareStatement(GET_NEWS_ID_BY_COMMENT_ID_SQL)) {
			ps.setLong(1, commentId);
			rs = ps.executeQuery();
			if (rs.next()) {
				newsId = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the get news ID by comment ID operation.", e);
		} finally {
			DatabaseUtil.closeResultSet(rs);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return newsId;
	}

	/**
	 * Builds comment.
	 * 
	 * @param comment
	 *            comment
	 * @param rs
	 *            result set
	 * @return created comment
	 * @throws SQLException
	 */
	private Comment buildComment(Comment comment, ResultSet rs) throws SQLException {

		comment = new Comment();
		comment.setCommentId(rs.getLong(1));
		comment.setNewsId(rs.getLong(2));
		comment.setCommentText(rs.getString(3));
		comment.setCreationDate(rs.getTimestamp(4));
		return comment;
	}

}

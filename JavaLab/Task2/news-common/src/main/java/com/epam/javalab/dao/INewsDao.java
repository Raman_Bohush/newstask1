package com.epam.javalab.dao;

import java.util.List;

import com.epam.javalab.domain.Author;
import com.epam.javalab.domain.Comment;
import com.epam.javalab.domain.News;
import com.epam.javalab.domain.SearchCriteria;
import com.epam.javalab.domain.Tag;
import com.epam.javalab.exception.DaoException;

/**
 * News Data Access Object interface. Provides additional operations with news persistent object.
 * 
 * @author Raman_Bohush
 *
 */
public interface INewsDao extends IGenericDao<News> {

	/**
	 * Finds news persistent object by one of the criteria.
	 * 
	 * @param searchCriteria
	 *            criteria for searching
	 * @param numPage
	 *            number of the page
	 * @return list of found news
	 * @throws DaoException
	 */
	public List<News> searchNews(SearchCriteria searchCriteria, Integer numPage) throws DaoException;

	/**
	 * Binds author with the news. Add author for news.
	 * 
	 * @param authorId
	 *            the author id
	 * @param newsId
	 *            the news id
	 * @throws DaoException
	 */
	public void addAuthorForNews(Long newsId, Long authorId) throws DaoException;

	/**
	 * Binds tag with the news. Add tag for news.
	 * 
	 * @param newsId
	 *            the news id
	 * @param tagId
	 *            the tag id
	 * @throws DaoException
	 */
	public void addTagForNews(Long newsId, Long tagId) throws DaoException;

	/**
	 * Destroys relation between author and news.
	 * 
	 * @param newsId
	 *            news ID
	 * @throws DaoException
	 */
	public void deleteAuthorNews(Long newsId) throws DaoException;

	/**
	 * Destroys relation between tag and news.
	 * 
	 * @param newsId
	 * @throws DaoException
	 */
	public void deleteTagNews(Long newsId) throws DaoException;

	/**
	 * Gets count of all the news.
	 * 
	 * @return news count
	 * @throws DaoException
	 */
	public Long getCountNews(SearchCriteria searchCriteria) throws DaoException;

	/**
	 * Update author of the news.
	 * 
	 * @param newsId
	 *            news ID
	 * @param authorId
	 *            author ID
	 * @throws DaoException
	 */
	public void updateAuthorNews(Long newsId, Long authorId) throws DaoException;

	/**
	 * Update tag of the news.
	 * 
	 * @param newsId
	 *            news
	 * @param tagId
	 *            tag ID
	 * @throws DaoException
	 */
	public void updateTagNews(Long newsId, Long tagId) throws DaoException;

	/**
	 * Gets news ID of the next news.
	 * 
	 * @param searchCriteria
	 *            search criteria.
	 * @param newsId
	 *            news ID
	 * @return news ID of the next news.
	 * @throws DaoException
	 */
	public Long getNextNews(SearchCriteria searchCriteria, Long newsId) throws DaoException;

	/**
	 * Gets news ID of the previous news.
	 * 
	 * @param searchCriteria
	 *            search criteria.
	 * @param newsId
	 *            news ID
	 * @return news ID of the previous news
	 * @throws DaoException
	 */
	public Long getPreviousNews(SearchCriteria searchCriteria, Long newsId) throws DaoException;

	/**
	 * Gets comments of the specific news.
	 * 
	 * @param newsId
	 *            news ID.
	 * @return comments list.
	 * @throws DaoException
	 */
	public List<Comment> getCommentsNews(Long newsId) throws DaoException;

	/**
	 * Gets list of tags of the specific news.
	 * 
	 * @param newsId
	 *            news ID.
	 * @return list of tags of the specific news.
	 * @throws DaoException
	 */
	public List<Tag> getTagsNews(Long newsId) throws DaoException;

	/**
	 * Gets author news.
	 * 
	 * @param newsId
	 *            news ID.
	 * @return author news.
	 * @throws DaoException
	 */
	public Author getAuthorNews(Long newsId) throws DaoException;

}

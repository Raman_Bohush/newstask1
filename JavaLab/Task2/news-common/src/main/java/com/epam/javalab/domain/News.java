package com.epam.javalab.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * Describes the properties of the table News.
 * 
 * @author Raman_Bohush
 *
 */
public class News implements Serializable {

	/**
	 * Serial version ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Property - news ID (primary key).
	 */
	private Long newsId;

	/**
	 * Property - news short text.
	 */
	private String shortText;

	/**
	 * Property - news full text.
	 */
	private String fullText;

	/**
	 * Property - news title.
	 */
	private String title;

	/**
	 * Property - creation date news.
	 */
	private Date creationDate;

	/**
	 * Property - modification date news.
	 */
	private Date modificationDate;

	public News() {
	}

	public News(Long newsId, String shortText, String fullText, String title, Date creationDate, Date modificationDate) {
		this.newsId = newsId;
		this.shortText = shortText;
		this.fullText = fullText;
		this.title = title;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {

		this.shortText = shortText.trim();
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {

		this.fullText = fullText.trim();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {

		this.title = title.trim();
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	@Override
	public int hashCode() {
		final int primeNumber = 47;
		int result = 1;
		result = result * primeNumber + ((newsId == null) ? 0 : newsId.hashCode());
		result = result * primeNumber + ((shortText == null) ? 0 : shortText.hashCode());
		result = result * primeNumber + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = result * primeNumber + ((fullText == null) ? 0 : fullText.hashCode());
		result = result * primeNumber + ((title == null) ? 0 : title.hashCode());
		result = result * primeNumber + ((modificationDate == null) ? 0 : modificationDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		News otherNews = (News) obj;
		if (newsId == null) {
			if (otherNews.newsId != null) {
				return false;
			}
		} else {
			return newsId.equals(otherNews.newsId);
		}
		if (shortText == null) {
			if (otherNews.shortText != null) {
				return false;
			}
		} else {
			return shortText.equals(otherNews.shortText);
		}
		if (creationDate == null) {
			if (otherNews.creationDate != null) {
				return false;
			}
		} else {
			return creationDate.equals(otherNews.creationDate);
		}
		if (fullText == null) {
			if (otherNews.fullText != null) {
				return false;
			}
		} else {
			return fullText.equals(otherNews.fullText);
		}
		if (title == null) {
			if (otherNews.title != null) {
				return false;
			}
		} else {
			return title.equals(otherNews.title);
		}
		if (modificationDate == null) {
			if (otherNews.modificationDate != null) {
				return false;
			}
		} else {
			return modificationDate.equals(otherNews.modificationDate);
		}
		return true;
	}

	@Override
	public String toString() {
		return "News [newsId=" + newsId + ", shortText=" + shortText + ", fullText=" + fullText + ", title=" + title
				+ ", creationDate=" + creationDate + ", modificationDate=" + modificationDate + "]";
	}

}

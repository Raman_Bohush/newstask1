package com.epam.javalab.domain;

import java.io.Serializable;
import java.util.List;

/**
 * Describes the properties of the News as VO.
 * @author Raman_Bohush
 *
 */
public class NewsVO implements Serializable{

	/**
	 * Serial version ID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * News.
	 */
	private News news;
	
	/**
	 * Author news.
	 */
	private Author author;
	
	/**
	 * Tags list for news.
	 */
	private List<Tag> tagsList;
	
	/**
	 * The number of comments to news.
	 */
	private Long countComment;
	
	/**
	 * Comments list for news.
	 */
	private List<Comment> commentsList;
	
	public NewsVO(){};
	
	public NewsVO(News news, Author author, List<Tag> tagsList,
			Long countComment, List<Comment> commentsList) {
		this.news = news;
		this.author = author;
		this.tagsList = tagsList;
		this.countComment = countComment;
		this.commentsList = commentsList;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Tag> getTagsList() {
		return tagsList;
	}

	public void setTagsList(List<Tag> tagsList) {
		this.tagsList = tagsList;
	}

	public Long getCountComment() {
		return countComment;
	}

	public void setCountComment(Long countComment) {
		this.countComment = countComment;
	}

	public List<Comment> getCommentsList() {
		return commentsList;
	}

	public void setCommentsList(List<Comment> commentsList) {
		this.commentsList = commentsList;
	}

	@Override
	public int hashCode() {
		final int primeNumber = 47;
		int result = 1;
		result = result * primeNumber + ((news == null) ? 0 : news.hashCode());
		result = result * primeNumber + ((author == null) ? 0 : author.hashCode());
		result = result * primeNumber + ((tagsList == null) ? 0 : tagsList.hashCode());
		result = result * primeNumber + ((countComment == null) ? 0 : countComment.hashCode());
		result = result * primeNumber + ((commentsList == null) ? 0 : commentsList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj){
			return true;
		}
		if(obj == null){
			return false;
		}
		if(this.getClass() != obj.getClass()){
			return false;
		}
		NewsVO otherNewsVO = (NewsVO) obj;
		if(news == null){
			if(otherNewsVO.news != null){
				return false;
			}
		}else {
			return news.equals(otherNewsVO.news);
		}
		if(author == null){
			if(otherNewsVO.author != null){
				return false;
			}
		}else {
			return author.equals(otherNewsVO.author);
		}
		if(tagsList == null){
			if(otherNewsVO.tagsList != null){
				return false;
			}
		}else {
			return tagsList.equals(otherNewsVO.tagsList);
		}
		if(countComment == null){
			if(otherNewsVO.countComment != null){
				return false;
			}
		}else {
			return countComment.equals(otherNewsVO.countComment);
		}
		if(commentsList == null){
			if(otherNewsVO.commentsList != null){
				return false;
			}
		}else {
			return commentsList.equals(otherNewsVO.commentsList);
		}
		return true;
	}

	@Override
	public String toString() {
		return "NewsVO [news=" + news + ", author=" + author + ", tagsList="
				+ tagsList + ", countComment=" + countComment
				+ ", commentsList=" + commentsList + "]";
	}
	
	
}

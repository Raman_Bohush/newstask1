package com.epam.javalab.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.javalab.dao.ITagDao;
import com.epam.javalab.domain.Tag;
import com.epam.javalab.exception.DaoException;
import com.epam.javalab.util.DatabaseUtil;

/**
 * Tag Repository Implements all operations with tag object.
 * 
 * @author Roma
 *
 */
@Repository
public class TagDaoImpl implements ITagDao {

	/**
	 * Receives a connection.
	 */
	@Autowired
	private DataSource dataSource;

	/**
	 * SQL request for create record.
	 */
	private final static String CREATE_SQL = "INSERT INTO tag (tag_id, tag_name) " + "VALUES(SEQ_TAG_ID.nextVal, ?)";

	/**
	 * SQL request for read record by id.
	 */
	private final static String READ_BY_ID_SQL = "SELECT tag_id, tag_name " + "FROM tag " + "WHERE tag_id=?";

	/**
	 * SQL request for update record.
	 */
	private final static String UPDATE_SQL = "UPDATE tag " + "SET tag_name=? " + "WHERE tag_id=?";

	/**
	 * SQL request for delete record.
	 */
	private final static String DELETE_SQL = "DELETE FROM tag " + "WHERE tag_id=?";

	/**
	 * SQL request for read all records.
	 */
	private final static String READ_ALL_SQL = "SELECT tag_id, tag_name " + "FROM tag";

	private final static String GET_TAGS_LIST_SQL = "SELECT tag_id, tag_name FROM tag";

	private final static String TAG_ID = "tag_id";

	private TagDaoImpl() {
	}

	@Override
	public Long create(Tag tag) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		ResultSet rsKey = null;
		Long tagId = null;
		String[] generatedKeys = { TAG_ID };
		try (PreparedStatement ps = con.prepareStatement(CREATE_SQL, generatedKeys)) {
			ps.setString(1, tag.getTagName());
			ps.executeUpdate();

			rsKey = ps.getGeneratedKeys();
			if (rsKey.next()) {
				tagId = rsKey.getLong(1);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the create operation.", e);
		} finally {
			DatabaseUtil.closeResultSet(rsKey);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return tagId;
	}

	@Override
	public Tag read(Long tagId) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		ResultSet rs = null;
		Tag tag = null;
		try (PreparedStatement ps = con.prepareStatement(READ_BY_ID_SQL)) {
			ps.setLong(1, tagId);
			rs = ps.executeQuery();
			if (rs.next()) {
				tag = buildTag(tag, rs);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the read operation.", e);
		} finally {
			DatabaseUtil.closeResultSet(rs);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return tag;
	}

	@Override
	public void update(Tag tag) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(UPDATE_SQL)) {
			ps.setString(1, tag.getTagName());
			ps.setLong(2, tag.getTagId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("SQL error in the update operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public void delete(Long tagId) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(DELETE_SQL)) {
			ps.setLong(1, tagId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("SQL error in the delete operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public List<Tag> readAll() throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		List<Tag> tagsList = new ArrayList<Tag>();
		Tag tag = null;

		try (Statement statement = con.createStatement(); ResultSet rs = statement.executeQuery(READ_ALL_SQL)) {
			while (rs.next()) {
				tag = buildTag(tag, rs);
				tagsList.add(tag);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the read all operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return tagsList;
	}

	/**
	 * Builds tag.
	 * 
	 * @param tag
	 *            tag
	 * @param rs
	 *            result set
	 * @return created tag
	 * @throws SQLException
	 */
	private Tag buildTag(Tag tag, ResultSet rs) throws SQLException {

		tag = new Tag();
		tag.setTagId(rs.getLong(1));
		tag.setTagName(rs.getString(2));
		return tag;
	}

}

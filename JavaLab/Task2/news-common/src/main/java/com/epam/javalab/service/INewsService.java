package com.epam.javalab.service;

import java.util.List;

import com.epam.javalab.domain.Author;
import com.epam.javalab.domain.Comment;
import com.epam.javalab.domain.News;
import com.epam.javalab.domain.SearchCriteria;
import com.epam.javalab.domain.Tag;
import com.epam.javalab.exception.ServiceException;

/**
 * News Service interface. Set of additional operations for news object and coordinates the application's response in
 * each operation.
 * 
 * @author Roma
 *
 */
public interface INewsService extends IGenericService<News> {

	/**
	 * Invokes search news by one of the criteria operation from Data Access Object layer. Finds news persistent object
	 * by one of the criteria. Checks parameter to search.
	 * 
	 * @param searchCriteria
	 *            criteria for searching
	 * @param numPage
	 *            number of page
	 * @return list of found news
	 * @throws ServiceException
	 */
	public List<News> searchNews(SearchCriteria searchCriteriam, Integer numPage) throws ServiceException;

	/**
	 * Invokes add author for news operation from Data Access Object layer. Binds author with the news. Add author for
	 * news.
	 * 
	 * @param newsId
	 *            the news
	 * @param authorId
	 *            the author id
	 * @throws ServiceException
	 */
	public void addAuthorForNews(Long newsId, Long authorId) throws ServiceException;

	/**
	 * Invokes add tag for news operation from Data Access Object layer. Binds tag with the news. Add tag for news.
	 * 
	 * @param newsId
	 *            the news id
	 * @param tagId
	 *            the tag id
	 * @throws ServiceException
	 */
	public void addTagForNews(Long newsId, Long tagId) throws ServiceException;

	/**
	 * Invokes delete author news from Data Access Object layer. Delete delete relation between author and news from
	 * database. Checks parameter to delete.
	 * 
	 * @param newsId
	 *            news ID.
	 * @throws ServiceException
	 */
	public void deleteAuthorNews(Long newsId) throws ServiceException;

	/**
	 * Invokes delete tag news from Data Access Object layer. Delete delete relation between tag and news from database.
	 * Checks parameter to delete.
	 * 
	 * @param newsId
	 *            news ID.
	 * @throws ServiceException
	 */
	public void deleteTagNews(Long newsId) throws ServiceException;

	/**
	 * Invokes get count news from Data Access Object layer.
	 * 
	 * @return news count
	 * @throws ServiceException
	 */
	public Long getCountNews(SearchCriteria searchCriteria) throws ServiceException;

	public void updateAuthorNews(Long newsId, Long authorId) throws ServiceException;

	public void updateTagsNews(Long newsId, Long authorId) throws ServiceException;

	public Long getNextNews(SearchCriteria searchCriteria, Long newsId) throws ServiceException;

	public Long getPreviousNews(SearchCriteria searchCriteria, Long newsId) throws ServiceException;

	public List<Comment> getCommentsNews(Long newsId) throws ServiceException;

	public List<Tag> getTagsNews(Long newsId) throws ServiceException;

	public Author getAuthorNews(Long newsId) throws ServiceException;
}

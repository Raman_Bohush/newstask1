package com.epam.javalab.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.epam.javalab.exception.DaoException;

/**
 * Closes resources.
 * 
 * @author Raman_Bohush
 *
 */
public class DatabaseUtil {

	/**
	 * Closes the ResultSet.
	 * 
	 * @param rs
	 *            result set.
	 * @throws DaoException
	 */
	public static void closeResultSet(ResultSet rs) throws DaoException {

		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				throw new DaoException("Error closing.", e);
			}
		}
	}
}

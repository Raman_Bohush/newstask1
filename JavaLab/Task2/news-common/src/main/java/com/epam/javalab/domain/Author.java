package com.epam.javalab.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * Describes the properties of the table Author.
 * 
 * @author Raman_Bohush
 *
 */
public class Author implements Serializable {

	/**
	 * Serial version ID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Property - author ID (primary key).
	 */
	private Long authorId;

	/**
	 * Property - author's name.
	 */
	private String name;

	/**
	 * Property - expired date.
	 */
	private Date expired;

	public Author() {
	}

	public Author(Long authorId, String name, Date expired) {
		this.authorId = authorId;
		this.name = name;
		this.expired = expired;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {

		this.name = name.trim();
	}

	public Date getExpired() {
		return expired;
	}

	public void setExpired(Date expired) {
		this.expired = expired;
	}

	@Override
	public int hashCode() {
		final int primeNumber = 47;
		int result = 1;
		result = result * primeNumber + ((authorId == null) ? 0 : authorId.hashCode());
		result = result * primeNumber + ((name == null) ? 0 : name.hashCode());
		result = result * primeNumber + ((expired == null) ? 0 : expired.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Author otherAuthor = (Author) obj;
		if (authorId == null) {
			if (otherAuthor.authorId != null) {
				return false;
			}
		} else {
			return authorId.equals(otherAuthor.authorId);
		}
		if (name == null) {
			if (otherAuthor.name != null) {
				return false;
			}
		} else {
			return name.equals(otherAuthor.name);
		}
		if (expired == null) {
			if (otherAuthor.expired != null) {
				return false;
			}
		} else {
			return name.equals(otherAuthor.expired);
		}
		return true;
	}

	@Override
	public String toString() {
		return "Author [authorId=" + authorId + ", name=" + name + ", expired=" + expired + "]";
	}
}

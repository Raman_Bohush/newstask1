package com.epam.javalab.exception;


/**
 * Exception in the Service layer.
 * @author Raman_Bohush
 *
 */
public class ServiceException extends NewsManagmentException{
	
	/**
	 * Serial version ID.
	 */
	private static final long serialVersionUID = 1L;

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ServiceException(String message) {
		super(message);
	}

	public ServiceException(Throwable cause) {
		super(cause);
	}
}

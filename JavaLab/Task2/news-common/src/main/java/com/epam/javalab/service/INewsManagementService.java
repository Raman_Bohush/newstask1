package com.epam.javalab.service;

import java.util.List;

import com.epam.javalab.domain.NewsVO;
import com.epam.javalab.domain.SearchCriteria;
import com.epam.javalab.exception.ServiceException;


/**
 * News management Service interface.
 * Set of transactions, complex operations 
 * and coordinates the application's response in each operation.
 * @author Roma
 *
 */
public interface INewsManagementService{
	
	/***
	 * Saves news with author and tags as one step.
	 * @throws ServiceException
	 */
	public Long saveNews(NewsVO newsVO) throws ServiceException;
	
	/**
	 * Delete news with comments.
	 * @param newsVO
	 * @throws ServiceException
	 */
	public void deleteNews(List<Long> newsListId) throws ServiceException;
	
	/**
	 * Update news with author and tags as one step.
	 * @param newsVO
	 * @throws ServiceException
	 */
	public void updateNews(NewsVO newsVO) throws ServiceException;

	public NewsVO getSingleNewsVO(Long newsId) throws ServiceException;

	public List<NewsVO> searchNews(SearchCriteria searchCriteria, Integer numPage) throws ServiceException;
}

package com.epam.javalab.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.javalab.dao.INewsDao;
import com.epam.javalab.domain.Author;
import com.epam.javalab.domain.Comment;
import com.epam.javalab.domain.News;
import com.epam.javalab.domain.SearchCriteria;
import com.epam.javalab.domain.Tag;
import com.epam.javalab.exception.DaoException;
import com.epam.javalab.util.DatabaseUtil;
import com.epam.javalab.util.Pagination;
import com.epam.javalab.util.QueryBuilder;

/**
 * News Repository Implements all operations with news object.
 * 
 * @author Roma
 *
 */
@Repository
public class NewsDaoImpl implements INewsDao {

	/**
	 * Receives a connection.
	 */
	@Autowired
	private DataSource dataSource;

	private final static Integer COUNT_NEWS_ON_PAGE = 3;

	/**
	 * SQL request for create record.
	 */
	private final static String CREATE_SQL = "INSERT INTO news (news_id, title, short_text, full_text, creation_date, modification_date) "
			+ "VALUES(SEQ_NEWS_ID.nextVal, ?, ?, ?, ?, ?)";

	/**
	 * SQL request for read record by id.
	 */
	private final static String READ_BY_ID_SQL = "SELECT news_id, title, short_text, full_text, creation_date, modification_date "
			+ "FROM news " + "WHERE news_id=?";

	/**
	 * SQL request for update record.
	 */
	private final static String UPDATE_SQL = "UPDATE news "
			+ "SET title=?, short_text=?, full_text=?, creation_date=?, modification_date=? " + "WHERE news_id=?";

	/**
	 * SQL request for delete record.
	 */
	private final static String DELETE_SQL = "DELETE FROM news " + "WHERE news_id=?";

	/**
	 * SQL request for read all records.
	 */
	private final static String READ_ALL_SQL = "SELECT news_id, title, short_text, full_text, creation_date, modification_date "
			+ "FROM news";

	/**
	 * SQL request for create record in the News_Author table.
	 */
	private final static String ADD_AUTHOR_FOR_NEWS_SQL = "INSERT INTO news_author(news_id, author_id) "
			+ "VALUES(?, ?)";

	/**
	 * SQL request for create record in the News_Tag table.
	 */
	private final static String ADD_TAG_FOR_NEWS_SQL = "INSERT INTO news_tag(news_id, tag_id) " + "VALUES(?, ?)";

	/**
	 * SQL request for get author news.
	 */
	private final static String GET_AUTHOR_NEWS_SQL = "SELECT author.author_id, author.author_name, author.expired "
			+ "FROM author JOIN news_author ON author.author_id=news_author.author_id " + "WHERE news_author.news_id=?";

	/**
	 * SQL request for get tags news.
	 */
	private final static String GET_TAGS_NEWS_SQL = "SELECT tag.tag_id, tag.tag_name "
			+ "FROM tag JOIN news_tag ON tag.tag_id=news_tag.tag_id " + "WHERE news_tag.news_id=?";

	/**
	 * SQL request for get comments news.
	 */
	private final static String GET_COMMENTS_NEWS_SQL = "SELECT comments.comment_id, comments.news_id, comments.comment_text, comments.creation_date "
			+ "FROM news JOIN comments ON news.news_id=comments.news_id "
			+ "WHERE comments.news_id=? ORDER BY comments.creation_date";

	/**
	 * SQL request for delete author news.
	 */
	private final static String DELETE_AUTHOR_NEWS_SQL = "DELETE FROM news_author " + "WHERE news_id=?";

	/**
	 * SQL request for delete tag news.
	 */
	private final static String DELETE_TAG_NEWS_SQL = "DELETE FROM news_tag " + "WHERE news_id=?";

	/**
	 * SQL request for update author news.
	 */
	private final static String UPDATE_AUTHOR_NEWS_SQL = "UPDATE news_author " + "SET author_id=? " + "WHERE news_id=?";

	/**
	 * SQL request for update tags news.
	 */
	private final static String UPDATE_TAGS_NEWS_SQL = "UPDATE news_tag " + "SET tag_id=? " + "WHERE news_id=?";

	private final static String NEWS_ID = "news_id";

	private NewsDaoImpl() {
	}

	@Override
	public Long create(News news) throws DaoException {

		ResultSet rsKey = null;
		Long newsId = null;
		String[] generatedKey = { NEWS_ID };
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(CREATE_SQL, generatedKey)) {
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setTimestamp(4, new java.sql.Timestamp(news.getCreationDate().getTime()));
			ps.setDate(5, new java.sql.Date(news.getModificationDate().getTime()));
			ps.executeUpdate();

			rsKey = ps.getGeneratedKeys();
			if (rsKey.next()) {
				newsId = rsKey.getLong(1);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the create operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
			DatabaseUtil.closeResultSet(rsKey);
		}
		return newsId;
	}

	@Override
	public News read(Long newsId) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		ResultSet rs = null;
		News news = null;
		try (PreparedStatement ps = con.prepareStatement(READ_BY_ID_SQL)) {
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			if (rs.next()) {
				news = buildNews(news, rs);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the read operation.", e);
		} finally {
			DatabaseUtil.closeResultSet(rs);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return news;
	}

	@Override
	public void update(News news) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(UPDATE_SQL)) {
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setTimestamp(4, new java.sql.Timestamp(news.getCreationDate().getTime()));
			ps.setDate(5, new java.sql.Date(news.getModificationDate().getTime()));
			ps.setLong(6, news.getNewsId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("SQL error in the update operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}

	}

	@Override
	public void delete(Long newsId) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(DELETE_SQL)) {
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("SQL error in the delete operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}

	}

	@Override
	public List<News> readAll() throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		List<News> newsList = new ArrayList<News>();
		News news = null;

		try (Statement statement = con.createStatement(); ResultSet rs = statement.executeQuery(READ_ALL_SQL)) {
			while (rs.next()) {
				news = buildNews(news, rs);
				newsList.add(news);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the read  all operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return newsList;
	}

	@Override
	public List<News> searchNews(SearchCriteria searchCriteria, Integer numPage) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		List<News> newsList = new ArrayList<>();
		News news = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<Long> tagsIdList = searchCriteria.getTagsIdList();
		Long authorId = searchCriteria.getAuthorId();
		Integer startIdx = Pagination.getStartIndex(numPage);
		Integer endIdx = Pagination.getEndIndex(numPage);
		String query = QueryBuilder.buildSearchQuery(searchCriteria);

		try {
			ps = con.prepareStatement(query);
			int paramIdx = 1;
			if (authorId != null && authorId != 0) {
				ps.setLong(paramIdx++, authorId);
			}
			if (tagsIdList != null) {
				for (Long tagId : tagsIdList) {
					ps.setLong(paramIdx++, tagId);
				}
			}
			ps.setInt(paramIdx++, startIdx);
			ps.setInt(paramIdx++, endIdx);
			rs = ps.executeQuery();

			while (rs.next()) {
				news = buildNews(news, rs);
				newsList.add(news);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the search news operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return newsList;
	}

	@Override
	public void addAuthorForNews(Long newsId, Long authorId) throws DaoException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(ADD_AUTHOR_FOR_NEWS_SQL)) {
			ps.setLong(1, newsId);
			ps.setLong(2, authorId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("SQL error in the add author for news operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}

	}

	@Override
	public void addTagForNews(Long newsId, Long tagId) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(ADD_TAG_FOR_NEWS_SQL)) {
			ps.setLong(1, newsId);
			ps.setLong(2, tagId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("SQL error in the add tags for news operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public void updateAuthorNews(Long newsId, Long authorId) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(UPDATE_AUTHOR_NEWS_SQL)) {
			ps.setLong(1, authorId);
			ps.setLong(2, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("SQL error in the update author news operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public void updateTagNews(Long newsId, Long tagId) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(UPDATE_TAGS_NEWS_SQL)) {
			ps.setLong(1, tagId);
			ps.setLong(2, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("SQL error in the update tags news operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public void deleteAuthorNews(Long newsId) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(DELETE_AUTHOR_NEWS_SQL)) {
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("SQL error in the delete author news operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public void deleteTagNews(Long newsId) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(DELETE_TAG_NEWS_SQL)) {
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DaoException("SQL error in the delete tag news operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public Long getCountNews(SearchCriteria searchCriteria) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		Long countNews = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<Long> tagsIdList = searchCriteria.getTagsIdList();
		Long authorId = searchCriteria.getAuthorId();

		String query = QueryBuilder.buildCountQuery(searchCriteria);
		try {
			ps = con.prepareStatement(query);
			int paramIdx = 1;
			if (authorId != null && authorId != 0) {
				ps.setLong(paramIdx++, authorId);
			}
			if (tagsIdList != null) {
				for (Long tagId : tagsIdList) {
					ps.setLong(paramIdx++, tagId);
				}
			}
			rs = ps.executeQuery();

			if (rs.next()) {
				countNews = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the search news operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}

		return countNews;
	}

	@Override
	public Long getNextNews(SearchCriteria searchCriteria, Long newsId) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Long> tagsIdList = searchCriteria.getTagsIdList();
		Long authorId = searchCriteria.getAuthorId();
		Long nextNewsId = null;
		String query = QueryBuilder.buildNextQuery(searchCriteria);

		try {
			ps = con.prepareStatement(query);
			int paramIdx = 1;
			if (authorId != null && authorId != 0) {
				ps.setLong(paramIdx++, authorId);
			}
			if (tagsIdList != null) {
				for (Long tagId : tagsIdList) {
					ps.setLong(paramIdx++, tagId);
				}
			}
			ps.setLong(paramIdx, newsId);
			rs = ps.executeQuery();

			if (rs.next()) {
				nextNewsId = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the search news operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}

		return nextNewsId;
	}

	@Override
	public Long getPreviousNews(SearchCriteria searchCriteria, Long newsId) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Long> tagsIdList = searchCriteria.getTagsIdList();
		Long authorId = searchCriteria.getAuthorId();
		Long nextNewsId = null;
		String query = QueryBuilder.buildPreviousQuery(searchCriteria);

		try {
			ps = con.prepareStatement(query);
			int paramIdx = 1;
			if (authorId != null && authorId != 0) {
				ps.setLong(paramIdx++, authorId);
			}
			if (tagsIdList != null) {
				for (Long tagId : tagsIdList) {
					ps.setLong(paramIdx++, tagId);
				}
			}

			ps.setLong(paramIdx++, newsId);
			rs = ps.executeQuery();

			if (rs.next()) {
				nextNewsId = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the search news operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}

		return nextNewsId;
	}

	/**
	 * Gets author news.
	 * 
	 * @param newsId
	 *            news ID
	 * @param con
	 *            connection
	 * @return author news
	 * @throws DaoException
	 */
	@Override
	public Author getAuthorNews(Long newsId) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		ResultSet rs = null;
		Author author = null;

		try (PreparedStatement ps = con.prepareStatement(GET_AUTHOR_NEWS_SQL)) {
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			if (rs.next()) {
				author = new Author();
				author.setAuthorId(rs.getLong(1));
				author.setName(rs.getString(2));
				author.setExpired(rs.getDate(3));
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the get author news operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}

		return author;
	}

	/**
	 * Gets tags news.
	 * 
	 * @param newsId
	 *            news ID
	 * @param con
	 *            connection
	 * @return tags list for news
	 * @throws DaoException
	 */
	@Override
	public List<Tag> getTagsNews(Long newsId) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		ResultSet rs = null;
		Tag tag = null;
		List<Tag> tagsList = new ArrayList<>();

		try (PreparedStatement ps = con.prepareStatement(GET_TAGS_NEWS_SQL)) {
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			while (rs.next()) {
				tag = new Tag();
				tag.setTagId(rs.getLong(1));
				tag.setTagName(rs.getString(2));
				tagsList.add(tag);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the get tags news operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}

		return tagsList;
	}

	/**
	 * Gets comments news.
	 * 
	 * @param newsId
	 *            news ID
	 * @param con
	 *            connection
	 * @return comments list for news
	 * @throws DaoException
	 */
	@Override
	public List<Comment> getCommentsNews(Long newsId) throws DaoException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		ResultSet rs = null;
		Comment comment = null;
		List<Comment> commentsList = new ArrayList<>();

		try (PreparedStatement ps = con.prepareStatement(GET_COMMENTS_NEWS_SQL)) {
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			while (rs.next()) {
				comment = new Comment();
				comment.setCommentId(rs.getLong(1));
				comment.setNewsId(rs.getLong(2));
				comment.setCommentText(rs.getString(3));
				comment.setCreationDate(rs.getDate(4));
				commentsList.add(comment);
			}
		} catch (SQLException e) {
			throw new DaoException("SQL error in the get comments news operation.", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}

		return commentsList;
	}

	/**
	 * Builds news.
	 * 
	 * @param news
	 *            news
	 * @param rs
	 *            result set
	 * @return created news
	 * @throws SQLException
	 */
	private News buildNews(News news, ResultSet rs) throws SQLException {

		news = new News();
		news.setNewsId(rs.getLong(1));
		news.setTitle(rs.getString(2));
		news.setShortText(rs.getString(3));
		news.setFullText(rs.getString(4));
		news.setCreationDate(rs.getTimestamp(5));
		news.setModificationDate(rs.getDate(6));
		return news;
	}
}

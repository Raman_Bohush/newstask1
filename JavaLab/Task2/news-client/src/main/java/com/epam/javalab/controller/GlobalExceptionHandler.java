package com.epam.javalab.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import com.epam.javalab.exception.DaoException;
import com.epam.javalab.exception.ServiceException;

@ControllerAdvice
public class GlobalExceptionHandler {

	public static final String DEFAULT_ERROR_VIEW = "exceptionPage";

	@ExceptionHandler({ DaoException.class, ServiceException.class })
	public ModelAndView handleSQLException(HttpServletRequest request, Exception ex) {

		ModelAndView modelAndView = new ModelAndView(DEFAULT_ERROR_VIEW);

		modelAndView.addObject("exception", ex);
		modelAndView.addObject("url", request.getRequestURL());

		return modelAndView;
	}
}

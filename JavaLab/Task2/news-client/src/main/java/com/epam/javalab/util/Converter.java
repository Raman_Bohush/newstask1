package com.epam.javalab.util;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.javalab.domain.Tag;
import com.epam.javalab.exception.ServiceException;
import com.epam.javalab.service.IAuthorService;
import com.epam.javalab.service.ITagService;

/**
 * Helps to build property editors.
 * 
 * @author Raman_Bohush
 *
 */
@Component
public class Converter extends PropertyEditorSupport {

	/**
	 * Tag service
	 */
	@Autowired
	private ITagService tagService;

	/**
	 * Author service
	 */
	@Autowired
	private IAuthorService authorService;

	/**
	 * Convert String to Tag instance.
	 */
	@Override
	public void setAsText(String text) throws IllegalArgumentException {

		Tag tag = null;
		try {
			tag = tagService.read(Long.valueOf(text));
		} catch (ServiceException e) {
			// ?
		}
		this.setValue(tag);
	}
}

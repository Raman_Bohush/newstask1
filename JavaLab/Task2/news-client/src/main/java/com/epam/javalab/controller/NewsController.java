package com.epam.javalab.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.javalab.domain.NewsVO;
import com.epam.javalab.domain.SearchCriteria;
import com.epam.javalab.exception.ServiceException;
import com.epam.javalab.service.IAuthorService;
import com.epam.javalab.service.ICommentService;
import com.epam.javalab.service.INewsManagementService;
import com.epam.javalab.service.INewsService;
import com.epam.javalab.service.ITagService;
import com.epam.javalab.util.Pagination;

/**
 * Handles requests related to the display news.
 * 
 * @author Raman_Bohush
 *
 */
@Controller
@SessionAttributes({ "criteria" })
public class NewsController {

	/**
	 * News service
	 */
	@Autowired
	private INewsService newsService;

	/**
	 * Author service
	 */
	@Autowired
	private IAuthorService authorService;

	/**
	 * Tag service
	 */
	@Autowired
	private ITagService tagService;

	/**
	 * News Management service
	 */
	@Autowired
	private INewsManagementService newsManagementService;

	/**
	 * Comment service
	 */
	@Autowired
	private ICommentService commentService;

	/**
	 * Create SearchCriteria instance before each handling methods.
	 * 
	 * @return search criteria
	 */
	@ModelAttribute("criteria")
	public SearchCriteria getCriteria() {
		return new SearchCriteria();
	}

	/**
	 * Handles requests for reset search criteria and. Fills data model. And redirect on the news URL.
	 * 
	 * @param numPage
	 *            number of the page
	 * @param model
	 *            data model
	 * @return path of the redirection
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/newslist")
	public String resetCriteria(@RequestParam(value = "numPage", defaultValue = "1", required = false) Integer numPage,
			Model model) throws ServiceException {

		model.addAttribute("criteria", new SearchCriteria());

		return "redirect:/";
	}

	/**
	 * Fills model and send response to client.
	 * 
	 * @param searchCriteria
	 *            search criteria
	 * @param numPage
	 *            number of the page
	 * @param model
	 *            data model
	 * @return view home
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String adminPage(@ModelAttribute("criteria") SearchCriteria searchCriteria,
			@RequestParam(value = "numPage", defaultValue = "1", required = false) Integer numPage, Model model)
			throws ServiceException {

		List<NewsVO> newsVOList = newsManagementService.searchNews(searchCriteria, numPage);
		model.addAttribute("authorsList", authorService.readAll());
		model.addAttribute("tagsList", tagService.readAll());
		model.addAttribute("newsList", newsVOList);
		Long count = newsService.getCountNews(searchCriteria);
		model.addAttribute("pagesCount", Pagination.getCountPages(count));
		return "home";
	}

}

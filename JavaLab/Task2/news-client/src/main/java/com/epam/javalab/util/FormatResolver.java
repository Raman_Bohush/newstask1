package com.epam.javalab.util;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * Provides localized data pattern
 * 
 * @author Raman_Bohush
 *
 */
public class FormatResolver {

	public final static String DATE_PATTERN = "locale.date_pattern";

	/**
	 * Message source
	 */
	@Autowired
	private MessageSource messageSource;

	/**
	 * Returns localized data pattern
	 * 
	 * @return data pattern
	 */
	public String getDatePattern() {

		Locale locale = LocaleContextHolder.getLocale();

		String datePattern = messageSource.getMessage(DATE_PATTERN, null, locale);

		return datePattern;
	}

}

function showLinks(id, name) {
	document.getElementById('update' + id).hidden = false;
	document.getElementById('edit' + id).hidden = true;
	document.getElementById('cancel' + id).hidden = false;
	document.getElementById(name + id).hidden = false;
	document.getElementById(id).disabled = false;

}

function hideLinks(id, name) {
	document.getElementById('update' + id).hidden = true;
	document.getElementById('edit' + id).hidden = false;
	document.getElementById('cancel' + id).hidden = true;
	document.getElementById(name + id).hidden = true;
	document.getElementById(id).disabled = true;
	document.getElementById(id).style.border="";
	document.getElementById(id).value = document.getElementById('oldValue' + id).value;
}

function submitForm(name, id) {
	var item = document.getElementById(id).value;
	if(!item){
		document.getElementById(id).style.border="1px solid red";	
	}else {
		document.getElementById(name + id).submit();
	}
}

function save(id){
	
	var item = document.getElementById('add').value;
	if(!item){
		document.getElementById('add').style.border="1px solid red";	
	}else {
		document.getElementById(id).submit();
	}
}

<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src=<c:url value="/resources/js/validator.js" />></script>
</head>
<body>
	<div class="content">

		<div class="back">
			<a href="/news-client/"><s:message code="locale.back"/></a>
		</div>
		<div>
			<div class="news-title">
				<b><c:out value="${newsVO.news.title}" /></b>
			</div>
			<div class="news-author">
				<c:out value="(${newsVO.author.name})" />
			</div>
			<div class="news-date">
				<c:set var="locale" value="${pageContext.response.locale}" />
				<c:choose>
					<c:when test="${locale == 'en'}">
						<fmt:setBundle basename="messages_en" var="bundle" />
						<fmt:message bundle="${bundle}" key="locale.date_pattern"
							var="datePattern" />
					</c:when>
					<c:otherwise>
						<fmt:setBundle basename="messages_ru" var="bundle" />
						<fmt:message bundle="${bundle}" key="locale.date_pattern"
							var="datePattern" />
					</c:otherwise>
				</c:choose>
				<fmt:formatDate value="${newsVO.news.modificationDate}"
					pattern="${datePattern}" />
			</div>
			<div class="news-full-text">
				<c:out value="${newsVO.news.fullText}" />
			</div>
		</div>
		<div class="comments_block">
			<c:forEach items="${newsVO.commentsList}" var="comment">
				<span><fmt:formatDate value="${comment.creationDate}"
						pattern="${datePattern}" /></span>
				<div class="comment">
					<div class="comment-text">
						<c:out value="${comment.commentText}"></c:out>
					</div>
				</div>
			</c:forEach>
			<div class="addComm">
				<form
					action="/news-client/viewNews/${newsVO.news.newsId}/addComment"
					method="POST" onsubmit="return validate(this, 'commentForm')">
					<textarea class="commentTextarea" name="commentText"
						id="commentText" maxlength="100"></textarea>
					<input class="big_button" type="submit"
						value=<s:message code="locale.post_comment" />> <input
						type="hidden" name="newsId" value="${newsVO.news.newsId}">
				</form>
			</div>

		</div>

		<c:if test="${fn:length(newsVO.commentsList) lt 1}">
			<div class="message-conteiner">
				<s:message code="locale.msg.no_comments" />
			</div>
		</c:if>

		<div class="prev-next">
			<c:if test="${not empty previousNewsId}">
				<a class="previous" href="/news-client/viewNews/${previousNewsId}"><s:message
						code="locale.previous" /></a>
			</c:if>

			<c:if test="${not empty nextNewsId}">
				<a class="next" href="/news-client/viewNews/${nextNewsId}"><s:message
						code="locale.next" /></a>
			</c:if>
		</div>
	</div>
</body>
</html>
<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href=<c:url value="/resources/css/sumoselect.css" />>
<script src=<c:url value="/resources/js/jquery-1.11.3.min.js" />></script>
<script src=<c:url value="/resources/js/jquery.sumoselect.min.js" />></script>
<script src=<c:url value="/resources/js/dropdown.js" />></script>
<script src=<c:url value="/resources/js/validator.js" />></script>

</head>
<body>
	<div class="content">

		<sf:form action="/news-client" modelAttribute="criteria" method="GET"
			class="filterForm">
			<div class="user_filter_block">
				<div class="selectAuthor">
					<sf:select path="authorId" class="dropdown">
						<sf:option value="0" selected="true">
							<s:message code="locale.select_author" />
						</sf:option>
						<c:forEach var="author" items="${authorsList}">
							<sf:option value="${author.authorId}">
								<c:out value="${author.name}" />
							</sf:option>
						</c:forEach>
					</sf:select>
				</div>
				<div class="multiselectTags">
					<sf:select path="tagsIdList" class="dropdown">
						<sf:option value="0" disabled="true" selected="true">
							<s:message code="locale.select_tags" />
						</sf:option>
						<c:forEach var="tag" items="${tagsList}">
							<sf:option value="${tag.tagId}">
								<c:out value="${tag.tagName}" />
							</sf:option>
						</c:forEach>
					</sf:select>
				</div>
			</div>
			<input class="btn" type="submit"
				value="<s:message code="locale.filter" />">
		</sf:form>
		<form action="/news-client/newslist" class="resetForm">
			<input class="btn" type="submit"
				value="<s:message code="locale.reset" />">
		</form>
		<div class="newslist-container">
			<c:forEach var="newsVO" items="${newsList}">
				<div class="news-info">
					<div class="news-title">
						<c:out value="${newsVO.news.title}" />
					</div>
					<div class="news-author">
						<c:out value="(${newsVO.author.name})" />
					</div>
					<div class="news-date">
						<c:set var="locale" value="${pageContext.response.locale}" />
						<c:choose>
							<c:when test="${locale == 'en'}">
								<fmt:setBundle basename="messages_en" var="bundle" />
								<fmt:message bundle="${bundle}" key="locale.date_pattern"
									var="datePattern" />
							</c:when>
							<c:otherwise>
								<fmt:setBundle basename="messages_ru" var="bundle" />
								<fmt:message bundle="${bundle}" key="locale.date_pattern"
									var="datePattern" />
							</c:otherwise>
						</c:choose>
						<fmt:formatDate value="${newsVO.news.modificationDate}"
							pattern="${datePattern}" />
					</div>
					<div class="news-short-text">
						<c:out value="${newsVO.news.shortText}" />
					</div>

					<div class="view-news">
						<a href="/news-client/viewNews/${newsVO.news.newsId}"><s:message
								code="locale.view" /></a>
					</div>

					<div class="news-comments">
						<s:message code="locale.comments" />
						<c:out value="(${newsVO.countComment})" />
					</div>

					<div class="news-tags">
						<c:forEach var="tag" items="${newsVO.tagsList}">
							<span><c:out value="${tag.tagName}" /></span>
						</c:forEach>
					</div>
				</div>
			</c:forEach>
		</div>
		<div class="pagination">
			<c:forEach begin="1" end="${pagesCount}" var="numPage">
				<a href="/news-client?numPage=${numPage}"><c:out
						value="${numPage}"></c:out></a>
			</c:forEach>
		</div>
	</div>
</body>
</html>
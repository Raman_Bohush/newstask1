package com.epam.javalab.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/**
 * Exception handler.
 * 
 * @author Raman_Bohush
 *
 */
@ControllerAdvice
public class GlobalExceptionHandler {

	public static final String DEFAULT_ERROR_VIEW = "exceptionPage";
	public static final String ERROR_404_VIEW = "404error";

	/**
	 * Handles of the exceptions and fills model.
	 * 
	 * @param request
	 *            request
	 * @param ex
	 *            exception
	 * @return model and view.
	 */
	@ExceptionHandler({ Exception.class })
	public ModelAndView handleSQLException(HttpServletRequest request, Exception ex) {

		ModelAndView modelAndView = new ModelAndView(DEFAULT_ERROR_VIEW);

		modelAndView.addObject("exception", ex);
		modelAndView.addObject("url", request.getRequestURL());

		return modelAndView;
	}

}

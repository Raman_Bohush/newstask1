package com.epam.javalab.rest;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.javalab.domain.Author;
import com.epam.javalab.domain.News;
import com.epam.javalab.domain.Tag;
import com.epam.javalab.exception.ServiceException;
import com.epam.javalab.service.IAuthorService;
import com.epam.javalab.service.INewsService;
import com.epam.javalab.service.ITagService;

/**
 * Import and converting tweets to news. Adds news in database.
 * 
 * @author Raman_Bohush
 *
 */
@Component("twitterTask")
public class TwitterProcessor {

	private static final String targetURL = "http://localhost:8086/news-admin/import";

	@Autowired
	private INewsService newsService;

	@Autowired
	private IAuthorService authorService;

	@Autowired
	private ITagService tagService;

	/**
	 * Receiving the JSON array of tweets from web service.
	 * 
	 * @throws IOException
	 * @throws ServiceException
	 */
	public void importNews() throws IOException, ServiceException {

		URL url;

		url = new URL(targetURL);

		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Accept", "*");

		if (connection.getResponseCode() != 200) {

			throw new RuntimeException("HTTP request error. Code: " + connection.getResponseCode());
		}

		JSONArray jsonArray = new JSONArray(IOUtils.toString(connection.getInputStream()));

		buildNews(jsonArray);

	}

	/**
	 * Builds news objects from JSON array of tweets and adds them in database.
	 * 
	 * @param jsonArray
	 *            JSON array.
	 * @throws ServiceException
	 */
	private void buildNews(JSONArray jsonArray) throws ServiceException {

		for (int i = 0; i < jsonArray.length(); i++) {

			List<Author> authorsList = new ArrayList<>();
			List<Tag> tagsList = new ArrayList<>();
			News news = new News();
			news.setTitle(jsonArray.getJSONObject(i).get("text").toString().substring(0, 20));
			news.setShortText(jsonArray.getJSONObject(i).get("text").toString().substring(0, 50));
			news.setFullText(jsonArray.getJSONObject(i).get("text").toString().substring(0));

			JSONObject authorNameJsonObject = (JSONObject) jsonArray.getJSONObject(i).get("user");

			JSONArray hashtags = ((JSONObject) jsonArray.getJSONObject(i).get("entities")).getJSONArray("hashtags");

			for (int j = 0; j < hashtags.length(); j++) {

				Tag tag = new Tag();
				tag.setTagName(hashtags.getJSONObject(j).get("text").toString());
				tagService.create(tag);
				tagsList.add(tag);
			}

			Author author = new Author();
			author.setName(authorNameJsonObject.get("name").toString());
			author.setExpired(null);
			authorService.create(author);

			authorsList.add(author);

			news.setTagsList(tagsList);
			news.setAuthorsList(authorsList);

			Date currentDate = new Date();
			news.setCreationDate(currentDate);
			news.setModificationDate(currentDate);

			newsService.create(news);
		}
	}

}

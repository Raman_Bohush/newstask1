package com.epam.javalab.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.javalab.domain.Comment;
import com.epam.javalab.exception.ServiceException;
import com.epam.javalab.service.ICommentService;
import com.epam.javalab.service.INewsService;

/**
 * Handles requests related to the view single news.
 * 
 * @author Raman_Bohush
 *
 */
@RequestMapping("/viewNews")
@Controller
@SessionAttributes({ "criteria" })
public class CommentController {

	/**
	 * News service
	 */
	@Autowired
	private INewsService newsService;

	/**
	 * Comment service
	 */
	@Autowired
	private ICommentService commentService;

	/**
	 * Handles requests for addition new comment. And redirect on the viewNews URL.
	 * 
	 * @param commentText
	 *            comment text
	 * @param newsId
	 *            news ID
	 * @return path of the redirection
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/{newsId}/addComment", method = RequestMethod.POST)
	public String addComment(@RequestParam("commentText") String commentText, @RequestParam("newsId") Long newsId)
			throws ServiceException {

		Comment comment = new Comment();
		comment.setNewsId(newsId);
		comment.setCommentText(commentText);
		comment.setCreationDate(new Date());

		commentService.create(comment);

		return "redirect:/viewNews/{newsId}";
	}

	/**
	 * Handles requests for delete comment. And redirect on the viewNews URL.
	 * 
	 * @param commentId
	 *            comment ID
	 * @return path of the redirection
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/{newsId}/deleteComment/{commentId}", method = RequestMethod.DELETE)
	public String deleteComment(@PathVariable("commentId") Long commentId) throws ServiceException {

		commentService.delete(commentId);

		return "redirect:/viewNews/{newsId}";
	}
}

package com.epam.javalab.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.javalab.domain.Tag;
import com.epam.javalab.exception.ServiceException;
import com.epam.javalab.service.ITagService;

/**
 * Handles requests related to the modification tags.
 * 
 * @author Raman_Bohush
 *
 */
@Controller
@RequestMapping(value = "/editTags")
public class TagController {

	/**
	 * Tag service
	 */
	@Autowired
	private ITagService tagService;

	/**
	 * Fills model and send response to client.
	 * 
	 * @param model
	 *            data model
	 * @return view editTags
	 * @throws ServiceException
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String showTags(Model model) throws ServiceException {

		model.addAttribute("tagsList", tagService.readAll());
		return "editTags";
	}

	/**
	 * Handles requests for update tag name. And redirect on the editTags URL.
	 * 
	 * @param tagId
	 *            tag ID
	 * @param tagName
	 *            tag name
	 * @return path of the redirection
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/{tagId}", method = RequestMethod.PUT)
	public String updateTag(@PathVariable("tagId") Long tagId, @RequestParam("tagName") String tagName)
			throws ServiceException {

		Tag tag = tagService.read(tagId);
		tag.setTagName(tagName);
		tagService.update(tag);

		return "redirect:/editTags";
	}

	/**
	 * Handles requests for delete tag. And redirect on the editTags URL.
	 * 
	 * @param tagId
	 *            tag ID
	 * @return path of the redirection
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/delete/{tagId}", method = RequestMethod.GET)
	public String deleteTag(@PathVariable("tagId") Long tagId) throws ServiceException {

		tagService.delete(tagId);
		return "redirect:/editTags";
	}

	/**
	 * Handles requests for addition tag. And redirect on the editTags URL.
	 * 
	 * @param tagName
	 * @return
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/addTag", method = RequestMethod.POST)
	public String addTag(@RequestParam("nameNewTag") String tagName) throws ServiceException {

		Tag tag = new Tag();
		tag.setTagName(tagName);
		tagService.create(tag);

		return "redirect:/editTags";
	}
}

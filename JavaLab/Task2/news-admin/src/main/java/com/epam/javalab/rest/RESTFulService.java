package com.epam.javalab.rest;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Receiving the tweets from Twitter.
 * 
 * @author Raman_Bohush
 *
 */
@RestController
public class RESTFulService {

	private final static String AccessToken = "519738247-8VVwtqrg7xp8hrHUA0ch7SDD38AXE820dyoSsRzE";
	private final static String AccessSecret = "0wT5SC4oPy55lHWKD6IQ4C8tHZbRUWOSjFSilNdUgYc8h";
	private final static String ConsumerKey = "KSoPA3J8NQ4Q5jiGMLrZV2OKp";
	private final static String ConsumerSecret = "uGPUEEepvp5Ahk0jiNvTfCO2TbFTDe52FxnF4xdpq0riWWJFuJ";

	private final static String twitterRestRequest = "https://api.twitter.com/1.1/statuses/home_timeline.json?count=3";

	/**
	 * Receiving the tweets from Twitter.
	 * 
	 * @return JSON array of tweets as string.
	 * @throws Exception
	 */
	@RequestMapping(value = "/import", method = RequestMethod.GET, produces = "text/plain")
	public String importNews() throws Exception {

		OAuthConsumer consumer = new CommonsHttpOAuthConsumer(ConsumerKey, ConsumerSecret);

		consumer.setTokenWithSecret(AccessToken, AccessSecret);
		HttpGet request = new HttpGet(twitterRestRequest);
		consumer.sign(request);

		HttpClient client = new DefaultHttpClient();
		HttpResponse response = client.execute(request);

		return IOUtils.toString(response.getEntity().getContent());

	}
}

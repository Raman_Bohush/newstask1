package com.epam.javalab.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.javalab.domain.Author;
import com.epam.javalab.exception.ServiceException;
import com.epam.javalab.service.IAuthorService;

/**
 * Handles requests related to the modification authors.
 * 
 * @author Raman_Bohush
 *
 */
@Controller
@RequestMapping(value = "/editAuthors")
public class AuthorController {

	/**
	 * Authors service
	 */
	@Autowired
	private IAuthorService authorService;

	/**
	 * Fills model and send response to client.
	 * 
	 * @param model
	 *            data model
	 * @return view editAuthors
	 * @throws ServiceException
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String showAuthors(Model model) throws ServiceException {

		model.addAttribute("authorsList", authorService.readAll());
		return "editAuthors";
	}

	/**
	 * Handles requests for update author name. And redirect on the editAuthors URL.
	 * 
	 * @param authorId
	 *            author ID
	 * @param authorName
	 *            author name
	 * @return path of the redirection
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/{authorId}", method = RequestMethod.PUT)
	public String updateAuthor(@PathVariable("authorId") Long authorId, @RequestParam("authorName") String authorName)
			throws ServiceException {

		Author author = new Author();
		author.setAuthorId(authorId);
		author.setName(authorName);
		author.setExpired(null);

		authorService.update(author);

		return "redirect:/editAuthors";
	}

	/**
	 * Handles requests for update author expire status. And redirect on the editAuthors URL.
	 * 
	 * @param authorId
	 *            author ID
	 * @return path of the redirection
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/expire/{authorId}", method = RequestMethod.GET)
	public String expireAuthor(@PathVariable("authorId") Long authorId) throws ServiceException {

		Author author = authorService.read(authorId);
		author.setExpired(new Date());

		authorService.update(author);

		return "redirect:/editAuthors";
	}

	/**
	 * Handles requests for addition author. And redirect on the editAuthors URL.
	 * 
	 * @param authorName
	 *            author name
	 * @return path of the redirection
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/addAuthor", method = RequestMethod.POST)
	public String addAuthor(@RequestParam("nameNewAuthor") String authorName) throws ServiceException {

		Author author = new Author();
		author.setName(authorName);
		author.setExpired(null);

		authorService.create(author);

		return "redirect:/editAuthors";
	}
}

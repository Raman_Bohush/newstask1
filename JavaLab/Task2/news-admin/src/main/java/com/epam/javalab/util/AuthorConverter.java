package com.epam.javalab.util;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.javalab.domain.Author;
import com.epam.javalab.exception.ServiceException;
import com.epam.javalab.service.IAuthorService;

@Component
public class AuthorConverter extends PropertyEditorSupport {

	/**
	 * Author service
	 */
	@Autowired
	private IAuthorService authorService;

	/**
	 * Convert String to Tag instance.
	 */
	@Override
	public void setAsText(String text) throws IllegalArgumentException {

		Author author = null;
		try {
			author = authorService.read(Long.valueOf(text));
		} catch (ServiceException e) {
			new RuntimeException("Can not parse Author object", e);
		}
		this.setValue(author);
	}
}

package com.epam.javalab.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.javalab.service.IAuthorService;
import com.epam.javalab.service.INewsService;
import com.epam.javalab.service.ITagService;

/**
 * Handles requests related to the login operation.
 * 
 * @author Raman_Bohush
 *
 */
@Controller
public class LoginController {

	/**
	 * News service
	 */
	@Autowired
	private INewsService newsService;

	/**
	 * Author service
	 */
	@Autowired
	private IAuthorService authorService;

	/**
	 * Tag service
	 */
	@Autowired
	private ITagService tagService;

	/**
	 * Forwards on the login page
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/")
	public String start(Model model) {
		return "login";
	}

	/**
	 * Handles requests for authorization. Fills data model. And redirect on the editNews URL.
	 * 
	 * @param error
	 *            error
	 * @param logout
	 *            logout
	 * @return view login
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String showLoginForm(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout, Model model) {
		String message = "";
		if (error != null) {
			message = "Invalid username or password!";
		}

		model.addAttribute("message", message);

		return "login";
	}

	/**
	 * Handles requests for 403 error. And redirect on the editNews URL.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/403page")
	public String denied403() {
		return "redirect:login?denied";
	}

}

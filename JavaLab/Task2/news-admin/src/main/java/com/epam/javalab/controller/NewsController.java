package com.epam.javalab.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.epam.javalab.domain.Author;
import com.epam.javalab.domain.News;
import com.epam.javalab.domain.SearchCriteria;
import com.epam.javalab.domain.Tag;
import com.epam.javalab.exception.ServiceException;
import com.epam.javalab.service.IAuthorService;
import com.epam.javalab.service.INewsService;
import com.epam.javalab.service.ITagService;
import com.epam.javalab.util.AuthorConverter;
import com.epam.javalab.util.FormatResolver;
import com.epam.javalab.util.Pagination;
import com.epam.javalab.util.TagConverter;

/**
 * Handles requests related to the display news.
 * 
 * @author Raman_Bohush
 *
 */
@Controller
@SessionAttributes({ "criteria" })
public class NewsController {

	/**
	 * News service
	 */
	@Autowired
	private INewsService newsService;

	/**
	 * Author service
	 */
	@Autowired
	private IAuthorService authorService;

	/**
	 * Tag service
	 */
	@Autowired
	private ITagService tagService;

	/**
	 * Converts the date format
	 */
	@Autowired
	private FormatResolver formatResolver;

	/**
	 * Helps to build property editors.
	 */
	@Autowired
	private TagConverter tagConverter;

	/**
	 * Helps to build property editors.
	 */
	@Autowired
	private AuthorConverter authorConverter;

	/**
	 * Registers custom editor for next conversion: String to Date and String to Tag instance.
	 * 
	 * @param binder
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatResolver.getDatePattern());
		binder.registerCustomEditor(Date.class, new CustomDateEditor(simpleDateFormat, true));
		binder.registerCustomEditor(Tag.class, tagConverter);
		binder.registerCustomEditor(Author.class, authorConverter);

	}

	/**
	 * Create SearchCriteria instance before each handling methods.
	 * 
	 * @return search criteria
	 */
	@ModelAttribute("criteria")
	public SearchCriteria getCriteria() {
		return new SearchCriteria();
	}

	/**
	 * Handles requests for reset search criteria and. Fills data model. And redirect on the news URL.
	 * 
	 * @param numPage
	 *            number of the page
	 * @param model
	 *            data model
	 * @param sessionStatus
	 *            session status
	 * @return path of the redirection
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/newslist")
	public String resetCriteria(@RequestParam(value = "numPage", defaultValue = "1", required = false) Integer numPage,
			Model model, SessionStatus sessionStatus) throws ServiceException {

		sessionStatus.setComplete();
		model.addAttribute("criteria", new SearchCriteria());

		return "redirect:/news";
	}

	/**
	 * Fills model and send response to client.
	 * 
	 * @param searchCriteria
	 *            search criteria
	 * @param numPage
	 *            number of the page
	 * @param model
	 *            data model
	 * @return view home
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/news", method = RequestMethod.GET)
	public String showNewsList(@ModelAttribute("criteria") SearchCriteria searchCriteria,
			@RequestParam(value = "numPage", defaultValue = "1", required = false) Integer numPage, Model model)
			throws ServiceException {

		model.addAttribute("authorsList", authorService.readAll());
		model.addAttribute("tagsList", tagService.readAll());
		model.addAttribute("newsList", newsService.searchNews(searchCriteria, numPage));
		Long count = newsService.getCountNews(searchCriteria);
		model.addAttribute("pagesCount", Pagination.getCountPages(count));
		return "home";
	}

	/**
	 * Handles requests for delete marked news. And redirect on the news URL.
	 * 
	 * @param newsIdList
	 *            news Id list
	 * @return path of the redirection
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/deleteNews", method = RequestMethod.DELETE)
	public String deleteMarkedNews(@RequestParam(value = "markedNews", required = false) List<Long> newsIdList)
			throws ServiceException {

		newsService.deleteMarkedNews(newsIdList);

		return "redirect:/news";
	}

	/**
	 * Fills model and send response to client.
	 * 
	 * @param newsVO
	 *            news VO
	 * @param model
	 *            data model
	 * @return view addNews
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/addNews", method = RequestMethod.GET)
	public String showSaveNewsForm(@ModelAttribute("news") News news, Model model) throws ServiceException {

		model.addAttribute("news", new News());
		model.addAttribute("authorsList", authorService.getNotExpiredAuthors());
		model.addAttribute("tagsList", tagService.readAll());
		model.addAttribute("currentDate", new Date());

		return "addNews";
	}

	/**
	 * Handles requests for preservation new news. And redirect on the editNews URL.
	 * 
	 * @param newsVO
	 *            news VO
	 * @return path of the redirection
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/addNews/saveNews", method = RequestMethod.POST)
	public String saveNews(@ModelAttribute("news") News news) throws ServiceException {

		Date modificationDate = news.getCreationDate();
		news.setModificationDate(modificationDate);

		Long newsId = newsService.create(news);

		return "redirect:/viewNews/" + newsId;
	}

	/**
	 * Fills model and send response to client.
	 * 
	 * @param newsId
	 *            news ID
	 * @param model
	 *            data model
	 * @return view editNews
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/editNews/{newsId}", method = RequestMethod.GET)
	public String showEditNewsForm(@PathVariable("newsId") Long newsId, Model model) throws ServiceException {

		Author newsAuthor = newsService.read(newsId).getAuthorsList().get(0);

		model.addAttribute("news", new News());
		if (newsAuthor.getExpired() == null) {
			model.addAttribute("authorsList", authorService.getNotExpiredAuthors());
		} else {
			model.addAttribute("authorsList", authorService.readAll());
		}
		model.addAttribute("tagsList", tagService.readAll());
		model.addAttribute("currentDate", new Date());
		model.addAttribute("news", newsService.read(newsId));

		return "editNews";
	}

	/**
	 * Handles requests for update new news. And redirect on the editNews URL.
	 * 
	 * @param newsVO
	 *            news VO
	 * @return path of the redirection
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/editNews/{newsId}/saveNews", method = RequestMethod.POST)
	public String updateNews(@ModelAttribute("news") News news) throws ServiceException {

		Date modificationDate = new Date();
		news.setModificationDate(modificationDate);

		newsService.update(news);

		return "redirect:/viewNews/{newsId}";
	}

	/**
	 * Fills model and send response to client.
	 * 
	 * @param searchCriteria
	 *            search criteria
	 * @param newsId
	 *            news ID
	 * @param model
	 *            data model
	 * @return view viewNews
	 * @throws ServiceException
	 */
	@RequestMapping(value = "/viewNews/{newsId}", method = RequestMethod.GET)
	public String viewNews(@ModelAttribute("criteria") SearchCriteria searchCriteria,
			@PathVariable("newsId") Long newsId, Model model) throws ServiceException {
		model.addAttribute("news", newsService.read(newsId));
		model.addAttribute("previousNewsId", newsService.getPreviousNews(searchCriteria, newsId));
		model.addAttribute("nextNewsId", newsService.getNextNews(searchCriteria, newsId));

		return "viewNews";
	}

}

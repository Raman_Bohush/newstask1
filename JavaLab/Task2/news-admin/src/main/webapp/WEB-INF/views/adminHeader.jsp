<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/style.css" />'>
</head>
<body>
	<header id="header">
		<h1>
			<s:message code="locale.header" />
		</h1>
		<div class="header-info">
			<div class="hello">
				<s:message code="locale.hello" />
			</div>
			<div class="logout">
				<c:url value="/j_spring_security_logout" var="logoutUrl" />
				<a href="${logoutUrl}"><s:message code="locale.logout" /></a>
			</div>
			<br>
			<div class="locale_btn">
				<a href="?language=ru"><s:message code="locale.ru" /></a> <a
					href="?language=en"><s:message code="locale.en" /></a>
			</div>

		</div>
	</header>
</body>
</html>
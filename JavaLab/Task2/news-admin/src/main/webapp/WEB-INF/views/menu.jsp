<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<div id="left-menu">
		<ul class="menu">
			<li><a href="/news-admin/newslist"><s:message code="locale.newslist" /></a></li>
			<li><a href="/news-admin/addNews"><s:message code="locale.addnews" /></a></li>
			<li><a href="/news-admin/editAuthors"><s:message code="locale.add_update_authors" /></a></li>
			<li><a href="/news-admin/editTags"><s:message code="locale.add_update_tags" /></a></li>
		</ul>
	</div>
</body>
</html>
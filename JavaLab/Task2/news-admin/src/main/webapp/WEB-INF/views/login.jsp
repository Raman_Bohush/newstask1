<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Sign In</title>
	</head>
	<body>
		<div class="signin-container">
			<form action="<c:url value='j_spring_security_check'/>" method="POST">
				<h2><s:message code="locale.login"/></h2>
				<input type="text" id="login" name="j_username"> <input
					type="password" id="password" name="j_password"> <input
					type="submit" value="<s:message code="locale.login"/>"> <span>${message}</span>
			</form>
		</div>
	</body>
</html>
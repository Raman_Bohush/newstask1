<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src=<c:url value="/resources/js/validator.js" />></script>
</head>
<body>
	<div class="content">

		<div>
			<div class="news-title">
				<b><c:out value="${news.title}" /></b>
			</div>
			<div class="news-author">
				<c:forEach var="author" items="${news.authorsList}">
					<c:out value="(${author.name})" />
				</c:forEach>
			</div>
			<div class="news-date">
				<c:set var="locale" value="${pageContext.response.locale}" />
				<c:choose>
					<c:when test="${locale == 'en'}">
						<fmt:setBundle basename="messages_en" var="bundle" />
						<fmt:message bundle="${bundle}" key="locale.date_pattern"
							var="datePattern" />
					</c:when>
					<c:otherwise>
						<fmt:setBundle basename="messages_ru" var="bundle" />
						<fmt:message bundle="${bundle}" key="locale.date_pattern"
							var="datePattern" />
					</c:otherwise>
				</c:choose>
				<fmt:formatDate value="${news.modificationDate}"
					pattern="${datePattern}" />
			</div>
			<div class="news-full-text">
				<c:out value="${news.fullText}" />
			</div>
		</div>
		<div class="comments_block">
			<c:forEach items="${news.commentsList}" var="comment">
				<span><fmt:formatDate value="${comment.creationDate}"
						pattern="${datePattern}" /></span>
				<div class="comment">
					<div class="comment-text">
						<c:out value="${comment.commentText}"></c:out>
					</div>
					<sf:form
						action="/news-admin/viewNews/${comment.newsId}/deleteComment/${comment.commentId}"
						method="DELETE">
						<input id="${comment.commentId}" class="closeButton" type="submit"
							value="x">
					</sf:form>
				</div>
			</c:forEach>
			<div class="addComm">
				<form action="/news-admin/viewNews/${news.newsId}/addComment"
					method="POST" onsubmit="return validate(this, 'commentForm')">
					<textarea class="commentTextarea" name="commentText"
						id="commentText" maxlength="100"></textarea>
					<input class="big_button" type="submit"
						value=<s:message code="locale.post_comment" />> <input
						type="hidden" name="newsId" value="${news.newsId}">
				</form>
			</div>

		</div>

		<c:if test="${fn:length(news.commentsList) lt 1}">
			<div class="message-conteiner">
				<s:message code="locale.msg.no_comments" />
			</div>
		</c:if>

		<div class="prev-next">
			<c:if test="${not empty previousNewsId}">
				<a class="previous" href="/news-admin/viewNews/${previousNewsId}"><s:message
						code="locale.previous" /></a>
			</c:if>

			<c:if test="${not empty nextNewsId}">
				<a class="next" href="/news-admin/viewNews/${nextNewsId}"><s:message
						code="locale.next" /></a>
			</c:if>
		</div>
	</div>
</body>
</html>
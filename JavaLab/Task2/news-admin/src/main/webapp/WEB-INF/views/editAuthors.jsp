<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src=<c:url value="/resources/js/links.js" />></script>
</head>
<body>
	<div class="content">
		<c:forEach items="${authorsList}" var="author">
			<div class="edit">
				<label for="${author.authorId}"><s:message
						code="locale.author" />:</label><input disabled="disabled"
					id="${author.authorId}" class="textInput" type="text"
					name="authorName" value="${author.name}"
					form="editForm${author.authorId}" maxlength="30"
					name="authorName${author.authorId}"> <input
					disabled="disabled" id="oldValue${author.authorId}"
					class="textInput" type="text" name="authorName"
					value="${author.name}" form="editForm${author.authorId}"
					maxlength="30" name="authorName${author.authorId}" hidden="true">
				<sf:form action="/news-admin/editAuthors/${author.authorId}"
					id="editForm${author.authorId}" method="PUT"
					onsubmit="return validate(this, 'authorForm')">
					<span id="edit${author.authorId}" class="links"
						onclick="showLinks('${author.authorId}', 'expire')"><s:message
							code="locale.edit" /></span> <span id="update${author.authorId}"
						class="links" hidden="true"
						onclick="submitForm('editForm', '${author.authorId}')"><s:message
							code="locale.update" /></span> <span id="expire${author.authorId}"
						class="links" hidden="true"
						onclick="submitForm('editForm', '${author.authorId}')"><a
						href="/news-admin/editAuthors/expire/${author.authorId}"><s:message
								code="locale.expire" /></a></span> <span id="cancel${author.authorId}"
						class="links" hidden="true"
						onclick="hideLinks('${author.authorId}', 'expire')"><s:message
							code="locale.cancel" /></span>
				</sf:form>
			</div>
		</c:forEach>
		<div class="edit">
			<label for="addAuthor"><s:message code="locale.add_author" />:</label><input
				id="add" class="textInput" type="text" value=""
				name="nameNewAuthor" form="addForm" maxlength="30">
			<form action="/news-admin/editAuthors/addAuthor" id="addForm"
				method="POST">
				<span id="add" class="links" onclick="save('addForm')"><s:message
						code="locale.save" /></span>
			</form>
		</div>
	</div>
</body>
</html>
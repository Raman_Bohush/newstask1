<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src=<c:url value="/resources/js/links.js" />></script>
</head>
<body>
	<div class="content">
		<c:forEach items="${tagsList}" var="tag">
			<div class="edit">
				<label for="${tag.tagId}"><s:message code="locale.tag" />:</label><input
					disabled="disabled" id="${tag.tagId}" class="textInput" type="text"
					name="tagName" value="${tag.tagName}" form="editForm${tag.tagId}"
					maxlength="30" name="tagName${tag.tagId}"> <input
					disabled="disabled" id="oldValue${tag.tagId}" class="textInput" type="text"
					name="tagName" value="${tag.tagName}" form="editForm${tag.tagId}"
					maxlength="30" name="tagName${tag.tagId}" hidden="true">
				<sf:form action="/news-admin/editTags/${tag.tagId}"
					id="editForm${tag.tagId}" method="PUT"
					onsubmit="return validateTag(this)">
					<span id="edit${tag.tagId}" class="links"
						onclick="showLinks('${tag.tagId}', 'delete')"><s:message
							code="locale.edit" /></span> <span id="update${tag.tagId}"
						class="links" hidden="true"
						onclick="submitForm('editForm', '${tag.tagId}')"><s:message
							code="locale.update" /></span> <span id="delete${tag.tagId}"
						hidden="true" class="links"><a
						href="/news-admin/editTags/delete/${tag.tagId}"><s:message
								code="locale.delete_tag" /></a></span> <span id="cancel${tag.tagId}"
						class="links" hidden="true"
						onclick="hideLinks('${tag.tagId}', 'delete')"><s:message
							code="locale.cancel" /></span>
				</sf:form>
			</div>
		</c:forEach>
		<div class="edit">
			<label for="addTag"><s:message code="locale.add_tag" />:</label><input
				id="add" class="textInput" type="text" value="" name="nameNewTag"
				form="addForm" maxlength="30">
			<form action="/news-admin/editTags/addTag" id="addForm" method="POST">
				<span id="add" class="links" onclick="save('addForm')"><s:message
						code="locale.save" /></span>
			</form>
		</div>
	</div>
</body>
</html>
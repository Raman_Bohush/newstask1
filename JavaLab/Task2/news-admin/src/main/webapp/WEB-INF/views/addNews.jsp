<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href=<c:url value="/resources/css/sumoselect.css" />>
<link rel="stylesheet" type="text/css"
	href=<c:url value="/resources/css/jquery.datetimepicker.css" />>
<script src=<c:url value="/resources/js/jquery-1.11.3.min.js" />></script>
<script src=<c:url value="/resources/js/jquery.sumoselect.min.js" />></script>
<script src=<c:url value="/resources/js/dropdown.js" />></script>
<script src=<c:url value="/resources/js/validator.js" />></script>
<script src=<c:url value="/resources/js/date.js" />></script>
<script src=<c:url value="/resources/js/jquery.datetimepicker.js" />></script>
</head>
<body>
	<div class="content">
		<div class="news-form-container">
			<sf:form action="/news-admin/addNews/saveNews"
				modelAttribute="news" method="POST"
				onsubmit="return validate(this, 'newsForm')">
				<div class="title">
					<div>
						<sf:label for="newsTitle" path="title">
							<s:message code="locale.title" />
						</sf:label>
					</div>
					<div>
						<sf:input id="newsTitle" path="title" type="text"
							name="newsTitle" maxlength="30" />
					</div>
				</div>
				<div class="creationDate">
					<div>
						<sf:label for="newsCreationDate" path="creationDate">
							<s:message code="locale.date" />
						</sf:label>
						<c:set var="locale" value="${pageContext.response.locale}" />

						<s:message code="locale.date_pattern" var="datePattern"/>
						<s:message code="locale.date_picker_pattern"
							var="datePickerPattern" />

						<fmt:setLocale value="${locale }" />

						<fmt:formatDate value="${currentDate}" pattern="${datePattern}"
							var="formatDate" />
					</div>
					<div>
						<sf:input id="newsCreationDate" path="creationDate"
							type="text" name="newsCreationDate" value="${formatDate}"
							readonly="true" />
					</div>
					<input id="locale" type="hidden" value="${locale }"> <input
						id="pattern" type="hidden" value="${datePickerPattern }">
				</div>
				<div class="shortText">
					<div>
						<sf:label for="newsShortText" path="shortText">
							<s:message code="locale.short_text" />
						</sf:label>
					</div>
					<div>
						<sf:textarea id="newsShortText" path="shortText"
							name="newsShortText" maxlength="100"></sf:textarea>
					</div>
				</div>
				<div class="fullText">
					<div>
						<sf:label for="newsFullText" path="fullText">
							<s:message code="locale.full_text" />
						</sf:label>
					</div>
					<div>
						<sf:textarea id="newsFullText" path="fullText"
							name="newsFullText" maxlength="2000"></sf:textarea>
					</div>
				</div>
				<div class="filter_block">
					<div class="selectAuthor">
						<sf:select path="authorsList" class="dropdown"
							id="authorsDropdown">
							<c:forEach var="author" items="${authorsList}">
								<sf:option value="${author.authorId}">
									<c:out value="${author.name}" />
								</sf:option>
							</c:forEach>
						</sf:select>
					</div>
					<div class="multiselectTags">
						<sf:select path="tagsList" class="dropdown">
							<sf:option value="0" disabled="true" selected="true">
								<s:message code="locale.select_tags" />
							</sf:option>
							<c:forEach var="tag" items="${tagsList}">
								<sf:option value="${tag.tagId}">
									<c:out value="${tag.tagName}" />
								</sf:option>
							</c:forEach>
						</sf:select>
					</div>
				</div>
				<input class="btn-save" type="submit"
					value=<s:message code="locale.save"/>>
			</sf:form>
		</div>
	</div>
</body>
</html>
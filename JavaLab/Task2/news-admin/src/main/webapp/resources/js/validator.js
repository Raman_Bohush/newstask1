function showError(container) {
	container.className = 'error';
}

function resetError(container) {
	container.className = 'not-error';
	if (container.lastChild.className == "error-message") {
		container.removeChild(container.lastChild);
	}
}

function validate(form, nameForm){
	
	switch (nameForm) {
	case 'newsForm':
		return validateNews(form);

	case 'commentForm':
		return validateComment(form);
		
	case 'deleteForm':
		return validateDeleteNews();
		
	case 'authorForm':
		
		break;
		
	case 'tagForm':
		
		break;
	}
}

function validateNews(form) {
	var elems = form.elements;
	var isValid = true;

	resetError(elems.newsTitle.parentNode);
	if (!elems.newsTitle.value) {
		showError(elems.newsTitle.parentNode);
		isValid = false;
	}

	resetError(elems.newsCreationDate.parentNode);
	if (!elems.newsCreationDate.value) {
		showError(elems.newsCreationDate.parentNode);
		isValid = false;
	}

	resetError(elems.newsShortText.parentNode);
	if (!elems.newsShortText.value) {
		showError(elems.newsShortText.parentNode);
		isValid = false;
	}

	resetError(elems.newsFullText.parentNode);
	if (!elems.newsFullText.value) {
		showError(elems.newsFullText.parentNode);
		isValid = false;
	}

	return isValid;

}

function validateComment(form) {
	var elems = form.elements;
	var isValid = true;

	resetError(elems.commentText.parentNode);
	if (!elems.commentText.value) {
		showError(elems.commentText.parentNode);
		isValid = false;
	}else if (!elems.commentText.value) {
		showError(elems.commentText.parentNode);
		isValid = false;
	}


	return isValid;

}

function validateDeleteNews() {
	return $('input[name=markedNews]:checked').length > 0;
}

package com.epam.javalab.task1.dao;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import org.dbunit.Assertion;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.javalab.task1.dao.impl.NewsDao;
import com.epam.javalab.task1.domain.Author;
import com.epam.javalab.task1.domain.News;
import com.epam.javalab.task1.domain.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;


/**
 * Tests News Data Access Object using DBUnit framework.
 * @author Roma
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:dataSource-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("/test/databaseDataset.xml")
public class NewsDaoTest{
	
	private final static String NEWS_TABLE = "news";
	private final static String NEWS_AUTHOR_TABLE = "news_author";
	private final static String NEWS_TAG_TABLE = "news_tag";
	
	/**
	 * News Data Access Object.
	 */
	@Autowired
	private NewsDao newsDao;
	
	/**
	 * Tests create operation.
	 * @throws Exception
	 */
	@Test
	public void createNews() throws Exception{
		
		News news = new News();
		news.setTitle("Windows");
		news.setShortText("short4");
		news.setFullText("full4");
		news.setCreationDate(Timestamp.valueOf("2015-08-17 11:11:11"));
		news.setModificationDate(new SimpleDateFormat("yyyy-MM-dd").parse("2015-09-17"));
		
		long id = newsDao.create(news);
		Assert.assertEquals(news.getTitle(), newsDao.read(id).getTitle());
		Assert.assertEquals(news.getShortText(), newsDao.read(id).getShortText());
		Assert.assertEquals(news.getFullText(), newsDao.read(id).getFullText());
		Assert.assertEquals(news.getCreationDate(), newsDao.read(id).getCreationDate());
		Assert.assertEquals(news.getModificationDate(), newsDao.read(id).getModificationDate());		
	}
	
	/**
	 * Tests read by id operation.
	 * @throws Exception
	 */
	@Test
	public void readNewsById() throws Exception{
		
		News news = newsDao.read(2L);
		
		Assert.assertEquals("Programming", news.getTitle());		
	}
	
	/**
	 * Tests update operation.
	 * @throws Exception
	 */
	@ExpectedDatabase(value = "/test/expectedUpdateDataset.xml", table = NEWS_TABLE, 
			assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void updateNews() throws Exception{
		
		News news = new News();
		news.setNewsId(3L);
		news.setTitle("Belarus Minsk");
		news.setShortText("short3");
		news.setFullText("full3");
		news.setCreationDate(Timestamp.valueOf("2015-04-17 11:11:11"));
		news.setModificationDate(new SimpleDateFormat("yyyy-MM-dd").parse("2015-09-17"));
		
		newsDao.update(news);		
	}
	
	/**
	 * Tests delete operation.
	 * @throws Exception
	 */
	@ExpectedDatabase(value = "/test/expectedDeleteDataset.xml", table = NEWS_TABLE, 
			assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void deleteNews() throws Exception{
		
		newsDao.delete(2L);
		
	}
	
	/**
	 * Tests read all operation.
	 * @throws Exception
	 */
	@Test
	public void getAllNews() throws Exception{
		
		List<News> newsList = newsDao.readAll();

		Assert.assertEquals(3, newsList.size());	
	}
	
	/**
	 * Tests search news by author operation.
	 * @throws Exception
	 */
	@Test
	public void searchNewsByAuthor() throws Exception{
		
		Author author = new Author();
		author.setAuthorId(1L);
		
		List<News> newsList = newsDao.searchNewsByAuthor(author);
		
		Assert.assertEquals(1, newsList.size());
		
	}
	
	/**
	 * Tests search news by tag operation.
	 * @throws Exception
	 */
	@Test
	public void searchNewsByTag() throws Exception{
		
		Tag tag = new Tag();
		tag.setTagId(2L);
		
		List<News> newsList = newsDao.searchNewsByTag(tag);
		
		Assert.assertEquals(1, newsList.size());
		
	}
	
	/**
	 * Tests sorting news by comments operation.
	 * @throws Exception
	 */
	@Test
	public void getSortedNews() throws Exception{
		
		List<News> newsList = newsDao.getSortedNews();
		
		Assert.assertEquals(3, newsList.size());
		
		Assert.assertEquals("Programming", newsList.get(0).getTitle());
		Assert.assertEquals("Football", newsList.get(1).getTitle());
		Assert.assertEquals("Belarus", newsList.get(2).getTitle());
			
	}
	
	/**
	 * Tests add author for news operation.
	 * @throws Exception
	 */
	@ExpectedDatabase(value = "/test/expectedAddForNewsDataset.xml", table = NEWS_AUTHOR_TABLE, 
			assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void addAuthorForNews() throws Exception{
		
		newsDao.addAuthorForNews(4L, 4L);
		
	}
	
	/**
	 * Tests add tag for news operation.
	 * @throws Exception
	 */
	@ExpectedDatabase(value = "/test/expectedAddForNewsDataset.xml", table = NEWS_TAG_TABLE, 
			assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void addTagForNews() throws Exception{
		
		newsDao.addTagForNews(4L, 4L);
		
	}

}

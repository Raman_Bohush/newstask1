package com.epam.javalab.task1.service;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.javalab.task1.domain.Author;
import com.epam.javalab.task1.domain.News;
import com.epam.javalab.task1.domain.Tag;
import com.epam.javalab.task1.service.exception.ServiceException;
import com.epam.javalab.task1.service.impl.AuthorService;
import com.epam.javalab.task1.service.impl.CommentService;
import com.epam.javalab.task1.service.impl.NewsManagementService;
import com.epam.javalab.task1.service.impl.NewsService;
import com.epam.javalab.task1.service.impl.TagService;

@RunWith(MockitoJUnitRunner.class)
public class NewsManagementServiceTest {
	
	@Mock
	private AuthorService authorService;
	
	@Mock
	private CommentService commentService;
	
	@Mock
	private NewsService newsService;
	
	@Mock
	private TagService tagService;
	
	@InjectMocks
	private NewsManagementService newsManagementService;
	
	@Test
	public void saveNews() throws ServiceException{
		
		News news = new News();
		Author author = new Author();
		List<Tag> tagsList = new ArrayList<>();
		Tag tag1 = new Tag();
		Tag tag2 = new Tag();
		tagsList.add(tag1);
		tagsList.add(tag2);
		
		newsManagementService.saveNews(news, author, tagsList);
		verify(newsService, times(1)).create(news);
		verify(newsService, times(1)).addAuthorForNews(news.getNewsId(), author.getAuthorId());
		verify(newsService, times(tagsList.size())).addTagForNews(anyLong(), anyLong());
	}
}

package com.epam.javalab.task1.dao;

import java.io.File;
import java.util.List;

import org.dbunit.Assertion;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.javalab.task1.dao.impl.TagDao;
import com.epam.javalab.task1.domain.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

/**
 * Tests Tag Data Access Object using DBUnit framework.
 * @author Roma
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:dataSource-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("/test/databaseDataset.xml")
public class TagDaoTest{
	
	private final static String TAG_TABLE = "tag";
	
	/**
	 * Tag Data Access Object.
	 */
	@Autowired
	private TagDao tagDao;
	
	/**
	 * Tests create operation.
	 * @throws Exception
	 */
	@Test
	public void createTag() throws Exception{
		
		Tag tag = new Tag();
		tag.setTagName("IT");
		
		long id = tagDao.create(tag);
		Assert.assertEquals(tag.getTagName(), tagDao.read(id).getTagName());
		
	}
	
	/**
	 * Tests read by id operation.
	 * @throws Exception
	 */
	@Test
	public void readTagById() throws Exception{
		
		Tag tag = tagDao.read(2L);
		
		Assert.assertEquals("Java", tag.getTagName());		
	}
	
	/**
	 * Tests update operation.
	 * @throws Exception
	 */
	@ExpectedDatabase(value = "/test/expectedUpdateDataset.xml", table = TAG_TABLE, 
			assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void updateTag() throws Exception{
		
		Tag tag = new Tag();
		tag.setTagId(2L);
		tag.setTagName("Java EE");
		
		tagDao.update(tag);
		
	}

	/**
	 * Tests delete operation.
	 * @throws Exception
	 */
	@ExpectedDatabase(value = "/test/expectedDeleteDataset.xml", table = TAG_TABLE, 
			assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void deleteTag() throws Exception{
		
		tagDao.delete(1L);
		
	}
	
	/**
	 * Tests read all operation.
	 * @throws Exception
	 */
	@Test
	public void getAllTags() throws Exception{
		
		List<Tag> tagsList = tagDao.readAll();

		Assert.assertEquals(3, tagsList.size());		
	}
	
}

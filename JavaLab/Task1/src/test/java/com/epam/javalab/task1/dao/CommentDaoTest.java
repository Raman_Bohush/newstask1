package com.epam.javalab.task1.dao;

import java.io.File;
import java.sql.Timestamp;
import java.util.List;

import org.dbunit.Assertion;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.javalab.task1.dao.impl.CommentDao;
import com.epam.javalab.task1.domain.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

/**
 * Tests Comment Data Access Object using DBUnit framework.
 * @author Roma
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:dataSource-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("/test/databaseDataset.xml")
public class CommentDaoTest{

	private final static String COMMENTS_TABLE = "comments";
	
	/**
	 * Comment Data Access Object.
	 */
	@Autowired
	private CommentDao commentDao;
	
	/**
	 * Tests create operation.
	 * @throws Exception
	 */
	@Test
	public void createComment() throws Exception{
		
		Comment comment = new Comment();
		comment.setNewsId(1L);
		comment.setCommentText("big com_text1");
		comment.setCreationDate(Timestamp.valueOf("2015-08-17 11:11:11"));
		
		long id = commentDao.create(comment);
		
		Assert.assertEquals(comment.getNewsId(), commentDao.read(id).getNewsId());
		Assert.assertEquals(comment.getCommentText(), commentDao.read(id).getCommentText());
		Assert.assertEquals(comment.getCreationDate(), commentDao.read(id).getCreationDate());
	}
	
	/**
	 * Tests read by id operation.
	 * @throws Exception
	 */
	@Test
	public void readCommentById() throws Exception{
		
		Comment comment = commentDao.read(2L);
		
		Assert.assertEquals("com_text2", comment.getCommentText());		
	}
	
	/**
	 * Tests update operation.
	 * @throws Exception
	 */
	@ExpectedDatabase(value = "/test/expectedUpdateDataset.xml", table = COMMENTS_TABLE, 
			assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void updateComment() throws Exception{
		
		Comment comment = new Comment();
		comment.setCommentId(1L);
		comment.setNewsId(1L);
		comment.setCommentText("big com_text1");
		comment.setCreationDate(Timestamp.valueOf("2015-08-17 11:11:11"));
		commentDao.update(comment);		
	}
	
	/**
	 * Tests delete operation.
	 * @throws Exception
	 */
	@ExpectedDatabase(value = "/test/expectedDeleteDataset.xml", table = COMMENTS_TABLE, 
			assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void deleteComment() throws Exception{

		commentDao.delete(1L);
		
	}
	
	/**
	 * Tests read all operation.
	 * @throws Exception
	 */
	@Test
	public void getAllComments() throws Exception{
		
		List<Comment> commentsList = commentDao.readAll();
		
		Assert.assertEquals(3, commentsList.size());	
	}
}

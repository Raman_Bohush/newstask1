package com.epam.javalab.task1.dao;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import com.epam.javalab.task1.dao.impl.AuthorDao;
import com.epam.javalab.task1.domain.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

/**
 * Tests Author Data Access Object using DBUnit framework.
 * 
 * @author Roma
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:dataSource-context.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("/test/databaseDataset.xml")
public class AuthorDaoTest {

	private final static String AUTHOR_TABLE = "author";

	/**
	 * Author Data Access Object.
	 */
	@Autowired
	private AuthorDao authorDao;

	/**
	 * Tests create operation.
	 * 
	 * @throws Exception
	 */

	@Test
	public void createAuthor() throws Exception {

		Author author = new Author();
		author.setName("Sveta");

		long id = authorDao.create(author);
		Assert.assertEquals(author.getName(), authorDao.read(id).getName());

	}

	/**
	 * Tests read by id operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void readAuthorById() throws Exception {

		Author author = authorDao.read(2L);

		Assert.assertEquals("Masha", author.getName());
	}

	/**
	 * Tests update operation.
	 * 
	 * @throws Exception
	 */
	@ExpectedDatabase(value = "/test/expectedUpdateDataset.xml", table = AUTHOR_TABLE, 
		assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void updateAuthor() throws Exception {

		Author author = new Author();
		author.setAuthorId(1L);
		author.setName("Roman Bogush");

		authorDao.update(author);

	}

	/**
	 * Tests delete operation.
	 * 
	 * @throws Exception
	 */
	@ExpectedDatabase(value="/test/expectedDeleteDataset.xml", table=AUTHOR_TABLE,
			assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
	@Test
	public void deleteAuthor() throws Exception {

		authorDao.delete(1L);
	}

	/**
	 * Tests read all operation.
	 * 
	 * @throws Exception
	 */
	@Test
	public void getAllAuthors() throws Exception {
		
		List<Author> authorsList = authorDao.readAll();
		Assert.assertEquals(3, authorsList.size());
	}

}

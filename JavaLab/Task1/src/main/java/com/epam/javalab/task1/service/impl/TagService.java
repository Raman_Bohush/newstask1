package com.epam.javalab.task1.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epam.javalab.task1.dao.exception.DaoException;
import com.epam.javalab.task1.dao.impl.TagDao;
import com.epam.javalab.task1.domain.Tag;
import com.epam.javalab.task1.service.ITagService;
import com.epam.javalab.task1.service.exception.ServiceException;

@Service
public class TagService implements ITagService{
	
	@Autowired
	private TagDao tagDao;

	@Override
	public long create(Tag tag) throws ServiceException {
		
		long id = 0;
		
		if(tag == null){
			throw new ServiceException("Tag is empty.");
		}
		
		try {
			id = tagDao.create(tag);
		} catch (DaoException e) {
			throw new ServiceException("Can not create tag", e);
		}
		return id;
		
	}

	@Override
	public Tag read(Long id) throws ServiceException {
		
		Tag tag = null;
		try {
			tag = tagDao.read(id);
		} catch (DaoException e) {
			throw new ServiceException("Can not read tag", e);
		}
		return tag;
	}

	@Override
	public void update(Tag tag) throws ServiceException {
		
		if(tag == null){
			throw new ServiceException("Tag is empty.");
		}
		
		try {
			tagDao.update(tag);
		} catch (DaoException e) {
			throw new ServiceException("Can not update tag", e);
		}
	}

	@Override
	public void delete(Long id) throws ServiceException {

		try {
			tagDao.delete(id);
		} catch (DaoException e) {
			throw new ServiceException("Can not delete tag", e);
		}
		
	}

	@Override
	public List<Tag> readAll() throws ServiceException {
		
		List<Tag> tagsList;
		try {
			tagsList = tagDao.readAll();
		} catch (DaoException e) {
			throw new ServiceException("Can not read all tags.", e);
		}
		return tagsList;
	}
	
}

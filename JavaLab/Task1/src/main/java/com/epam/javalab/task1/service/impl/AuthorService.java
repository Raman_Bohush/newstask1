package com.epam.javalab.task1.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epam.javalab.task1.dao.exception.DaoException;
import com.epam.javalab.task1.dao.impl.AuthorDao;
import com.epam.javalab.task1.domain.Author;
import com.epam.javalab.task1.service.IAuthorService;
import com.epam.javalab.task1.service.exception.ServiceException;

@Service
public class AuthorService implements IAuthorService {

	@Autowired
	private AuthorDao authorDao;

	@Override
	public long create(Author author) throws ServiceException {

		long id = 0;

		if (author == null) {
			throw new ServiceException("Author is empty.");
		}

		try {
			id = authorDao.create(author);
		} catch (DaoException e) {
			throw new ServiceException("Can not create author.", e);
		}
		return id;

	}

	@Override
	public Author read(Long id) throws ServiceException {

		Author author = null;
		try {
			author = authorDao.read(id);
		} catch (DaoException e) {			
			throw new ServiceException("Can not read author.", e);
		}
		return author;
	}

	@Override
	public void update(Author author) throws ServiceException {

		if (author == null) {
			throw new ServiceException("Author is empty.");
		}

		try {
			authorDao.update(author);
		} catch (DaoException e) {
			throw new ServiceException("Can not update author.", e);
		}

	}

	@Override
	public void delete(Long id) throws ServiceException {

		try {
			authorDao.delete(id);
		} catch (DaoException e) {
			throw new ServiceException("Can not delete author.", e);
		}

	}

	@Override
	public List<Author> readAll() throws ServiceException {

		List<Author> authorsList;
		try {
			authorsList = authorDao.readAll();
		} catch (DaoException e) {
			throw new ServiceException("Can not read all authors.", e);
		}
		return authorsList;
	}

}

package com.epam.javalab.task1.service;

import java.util.List;

import com.epam.javalab.task1.domain.Author;
import com.epam.javalab.task1.domain.News;
import com.epam.javalab.task1.domain.Tag;
import com.epam.javalab.task1.service.exception.ServiceException;

/**
 * News Service interface.
 * Set of additional operations for news object 
 * and coordinates the application's response in each operation.
 * @author Roma
 *
 */
public interface INewsService extends IGenericService<News>{
	
	/**
	 * Invokes search news by author operation from Data Access Object layer.
	 * Finds news persistent object by author.
	 * Checks parameter to search.
	 * @param the author
	 * @return list of found news
	 * @throws ServiceException
	 */
	public List<News> searchNewsByAuthor(Author author) throws ServiceException;
	
	/**
	 * Invokes search news by tag operation from Data Access Object layer.
	 * Finds news persistent object by tag.
	 * Checks parameter to search.
	 * @param tag the tag
	 * @return list of found news
	 * @throws ServiceException
	 */
	public List<News> searchNewsByTag(Tag tag) throws ServiceException;
	
	/**
	 * Invokes sorting news by comment operation from Data Access Object layer.
	 * @return sorted list of news
	 * @throws ServiceException
	 */
	public List<News> getSortedNews() throws ServiceException;
	
	/**
	 * Invokes add author for news operation from Data Access Object layer.
	 * Binds author with the news. Add author for news.
	 * @param newsId the news
	 * @param authorId the author id
	 * @throws ServiceException
	 */
	public void addAuthorForNews(Long newsId, Long authorId) throws ServiceException;
	
	/**
	 * Invokes add tag for news operation from Data Access Object layer.
	 * Binds tag with the news. Add tag for news.
	 * @param newsId the news id
	 * @param tagId the tag id
	 * @throws ServiceException
	 */
	public void addTagForNews(Long newsId, Long tagId) throws ServiceException;
}

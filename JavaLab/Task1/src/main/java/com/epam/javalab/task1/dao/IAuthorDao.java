package com.epam.javalab.task1.dao;

import com.epam.javalab.task1.domain.Author;

/**
 * Author Data Access Object interface.
 * Provides additional operations with author persistent object.
 * @author Raman_Bohush
 *
 */
public interface IAuthorDao extends IGenericDao<Author>{}

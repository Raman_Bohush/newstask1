package com.epam.javalab.task1.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epam.javalab.task1.dao.exception.DaoException;
import com.epam.javalab.task1.dao.impl.CommentDao;
import com.epam.javalab.task1.domain.Comment;
import com.epam.javalab.task1.service.ICommentService;
import com.epam.javalab.task1.service.exception.ServiceException;

@Service
public class CommentService implements ICommentService{
	
	@Autowired
	private CommentDao commentDao;

	@Override
	public long create(Comment comment) throws ServiceException {
		
		long id = 0;
		
		if(comment == null){
			throw new ServiceException("Comment is empty.");
		}
		
		try {
			id = commentDao.create(comment);
		} catch (DaoException e) {
			throw new ServiceException("Can not create comment.", e);
		}
		return id;
		
	}

	@Override
	public Comment read(Long id) throws ServiceException {
		
		Comment comment = null;
		try {
			comment = commentDao.read(id);
		} catch (DaoException e) {
			throw new ServiceException("Can not read comment", e);
		}
		return comment;
	}

	@Override
	public void update(Comment comment) throws ServiceException {
		
		if(comment == null){
			throw new ServiceException("Comment is empty.");
		}
		
		try {
			commentDao.update(comment);
		} catch (DaoException e) {
			throw new ServiceException("Can not update comment", e);
		}
		
	}

	@Override
	public void delete(Long id) throws ServiceException {
		
		try {
			commentDao.delete(id);
		} catch (DaoException e) {
			throw new ServiceException("Can not delete comment", e);
		}
			
		
	}

	@Override
	public List<Comment> readAll() throws ServiceException {
		
		List<Comment> commentsList;
		try {
			commentsList = commentDao.readAll();
		} catch (DaoException e) {
			throw new ServiceException("Can not read all comments.", e);
		}
		return commentsList;
	}

}

package com.epam.javalab.task1.dao;

import com.epam.javalab.task1.domain.Comment;

/**
 * Comment Data Access Object interface.
 * Provides additional operations with comment persistent object.
 * @author Raman_Bohush
 *
 */
public interface ICommentDao extends IGenericDao<Comment>{}

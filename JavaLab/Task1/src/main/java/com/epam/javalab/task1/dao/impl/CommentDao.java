package com.epam.javalab.task1.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.javalab.task1.dao.ICommentDao;
import com.epam.javalab.task1.dao.exception.DaoException;
import com.epam.javalab.task1.domain.Comment;

/**
 * Comment Repository
 * Implements all operations with comment object.
 * @author Roma
 *
 */
@Repository
public class CommentDao implements ICommentDao{
	
	/**
	 * Receives a connection.
	 */
	@Autowired
	private DataSource dataSource;
	
	/**
	 * SQL request for create record.
	 */
	private final static String CREATE_SQL = 
			"INSERT INTO comments (comment_id, news_id, comment_text, creation_date) "
			+ "VALUES(SEQ_COMMENTS_ID.nextVal, ?, ?, ?)";
	
	/**
	 * SQL request for read record by id.
	 */
	private final static String READ_BY_ID_SQL = 
			"SELECT comment_id, news_id, comment_text, creation_date "
			+ "FROM comments "
			+ "WHERE comment_id=?";
	
	/**
	 * SQL request for update record.
	 */
	private final static String UPDATE_SQL = 
			"UPDATE comments "
			+ "SET news_id=?, comment_text=?, creation_date=? "
			+ "WHERE comment_id=?";
	
	/**
	 * SQL request for delete record.
	 */
	private final static String DELETE_SQL = 
			"DELETE FROM comments "
			+ "WHERE comment_id=?";
	
	/**
	 * SQL request for read all records.
	 */
	private final static String READ_ALL_SQL = 
			"SELECT comment_id, news_id, comment_text, creation_date "
			+ "FROM comments";
	
	private final static String COMMENT_ID = "comment_id";
	
	private CommentDao() {}

	@Override
	public long create(Comment comment) throws DaoException {
		
		ResultSet rsKey = null;
		long id = 0;
		String[] generatedKeys = {COMMENT_ID};
		try(Connection con = dataSource.getConnection();
				PreparedStatement ps = con.prepareStatement(CREATE_SQL, generatedKeys);
				Statement statement = con.createStatement()){
			
			ps.setLong(1, comment.getNewsId());
			ps.setString(2, comment.getCommentText());
			ps.setTimestamp(3, new java.sql.Timestamp(comment.getCreationDate().getTime()));
			ps.executeUpdate();
			
			rsKey = ps.getGeneratedKeys();
			if(rsKey.next()){
				id = rsKey.getInt(1);
			}
		}catch(SQLException e){
			throw new DaoException("SQL error in the create operation.", e);
		}finally{
			closeResultSet(rsKey);
		}
		return id;
	}

	@Override
	public Comment read(Long commentId) throws DaoException {
		
		ResultSet rs = null;
		Comment comment = null;
		try(Connection con = dataSource.getConnection();
				PreparedStatement ps = con.prepareStatement(READ_BY_ID_SQL)){
			ps.setLong(1, commentId);
			rs = ps.executeQuery();
			if(rs.next()){
				comment = new Comment();
				comment.setCommentId(rs.getLong(1));
				comment.setNewsId(rs.getLong(2));
				comment.setCommentText(rs.getString(3));
				comment.setCreationDate(rs.getTimestamp(4));
			}			
		}catch (SQLException e) {
			throw new DaoException("SQL error in the read operation.", e);
		}finally{
			closeResultSet(rs);
		}
		return comment;
	}

	@Override
	public void update(Comment comment) throws DaoException {
		
		try(Connection con = dataSource.getConnection();
				PreparedStatement ps = con.prepareStatement(UPDATE_SQL)){
			ps.setLong(1, comment.getNewsId());
			ps.setString(2, comment.getCommentText());
			ps.setTimestamp(3, new java.sql.Timestamp(comment.getCreationDate().getTime()));
			ps.setLong(4, comment.getCommentId());
			ps.executeUpdate();
		}catch (SQLException e) {
			throw new DaoException("SQL error in the update operation.", e);
		}
	}

	@Override
	public void delete(Long id) throws DaoException {

		try(Connection con = dataSource.getConnection();
				PreparedStatement ps = con.prepareStatement(DELETE_SQL)){
			ps.setLong(1, id);
			ps.executeUpdate();
		}catch (SQLException e) {
			throw new DaoException("SQL error in the delete operation.", e);
		}	
	}

	@Override
	public List<Comment> readAll() throws DaoException {
		
		List<Comment> commentsList = new ArrayList<Comment>();
		Comment comment = null;
		
		try(Connection con = dataSource.getConnection();
				Statement statement = con.createStatement();
				ResultSet rs = statement.executeQuery(READ_ALL_SQL)){
			while(rs.next()){
				comment = new Comment();
				comment.setCommentId(rs.getLong(1));
				comment.setNewsId(rs.getLong(2));
				comment.setCommentText(rs.getString(3));
				comment.setCreationDate(rs.getTimestamp(4));
				commentsList.add(comment);
			}
		}catch (SQLException e) {
			throw new DaoException("SQL error in the read  all operation.", e);
		}
		return commentsList;
	}
	
	private void closeResultSet(ResultSet rs) throws DaoException{
		
		if(rs != null){
			try {
				rs.close();
			} catch (SQLException e) {
				throw new DaoException("Error closing.", e);
			}
		}
	}
}

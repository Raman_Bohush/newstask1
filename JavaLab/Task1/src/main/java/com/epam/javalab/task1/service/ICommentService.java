package com.epam.javalab.task1.service;

import com.epam.javalab.task1.domain.Comment;

/**
 * Comment Service interface.
 * Set of additional operations for comment object 
 * and coordinates the application's response in each operation.
 * @author Roma
 *
 */
public interface ICommentService extends IGenericService<Comment>{}

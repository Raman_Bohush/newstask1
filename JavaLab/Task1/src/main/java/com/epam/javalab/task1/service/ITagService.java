package com.epam.javalab.task1.service;

import com.epam.javalab.task1.domain.Tag;

/**
 * Tag Service interface.
 * Set of additional operations for tag object 
 * and coordinates the application's response in each operation.
 * @author Roma
 *
 */
public interface ITagService extends IGenericService<Tag>{}

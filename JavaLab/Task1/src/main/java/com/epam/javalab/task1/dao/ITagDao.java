package com.epam.javalab.task1.dao;

import com.epam.javalab.task1.domain.Tag;

/**
 * Tag Data Access Object interface.
 * Provides additional operations with tag persistent object.
 * @author Raman_Bohush
 *
 */
public interface ITagDao extends IGenericDao<Tag>{}

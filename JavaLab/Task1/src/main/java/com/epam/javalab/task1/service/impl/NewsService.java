package com.epam.javalab.task1.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.javalab.task1.dao.exception.DaoException;
import com.epam.javalab.task1.dao.impl.NewsDao;
import com.epam.javalab.task1.domain.Author;
import com.epam.javalab.task1.domain.News;
import com.epam.javalab.task1.domain.Tag;
import com.epam.javalab.task1.service.INewsService;
import com.epam.javalab.task1.service.exception.ServiceException;

@Service
public class NewsService implements INewsService{

	@Autowired
	private NewsDao newsDao;
	
	@Override
	public long create(News news) throws ServiceException {

		long id = 0;
		
		if(news == null){
			throw new ServiceException("News is empty.");
		}
		
		try{
			id = newsDao.create(news);
		}catch(DaoException e){
			throw new ServiceException("Can not create news.", e);
		}
		return id;
		
	}

	@Override
	public News read(Long id) throws ServiceException  {
		
		News news = null;
		try {
			news = newsDao.read(id);
		} catch (DaoException e) {
			throw new ServiceException("Can not read news.", e);
		}
		return news;
	}

	@Override
	public void update(News news) throws ServiceException {
		
		if(news == null){
			throw new ServiceException("News is empty.");
		}
		
		try {
			newsDao.update(news);
		} catch (DaoException e) {
			throw new ServiceException("Can not update news.", e);
		}
		
	}

	@Override
	public void delete(Long id) throws ServiceException {

		try {
			newsDao.delete(id);
		} catch (DaoException e) {
			throw new ServiceException("Can not delete news.", e);
		}
		
	}

	@Override
	public List<News> readAll() throws ServiceException {
		
		List<News> newsList;
		try {
			newsList = newsDao.readAll();
		} catch (DaoException e) {
			throw new ServiceException("Can not read all news.", e);
		}
		return newsList;
	}

	@Override
	public List<News> searchNewsByAuthor(Author author) throws ServiceException {
		
		List<News> newsList;
		if(author == null){
			throw new ServiceException("Author is empty.");
		}
		
		try {
			newsList = newsDao.searchNewsByAuthor(author);
		} catch (DaoException e) {
			throw new ServiceException("Can not search news by author.", e);
		}
		return newsList;
	}

	@Override
	public List<News> searchNewsByTag(Tag tag) throws ServiceException {
		
		List<News> newsList;
		if(tag == null){
			throw new ServiceException("Tag is empty.");
		}
		
		try {
			newsList = newsDao.searchNewsByTag(tag);
		} catch (DaoException e) {
			throw new ServiceException("Can not search news by tag.", e);
		}
		return newsList;
	}

	@Override
	public List<News> getSortedNews() throws ServiceException {
		
		List<News> newsList;
		try {
			newsList = newsDao.getSortedNews();
		} catch (DaoException e) {
			throw new ServiceException("Can not get sorted news news.", e);
		}
		return newsList;
	}

	@Override
	public void addAuthorForNews(Long newsId, Long authorId) throws ServiceException {
		
		try {
			newsDao.addAuthorForNews(newsId, authorId);
		} catch (DaoException e) {
			throw new ServiceException("Can not add author for news.", e);
		}
		
	}

	@Override
	public void addTagForNews(Long newsId, Long tagId) throws ServiceException {
		
		try {
			newsDao.addTagForNews(newsId, tagId);
		} catch (DaoException e) {
			throw new ServiceException("Can not add tag for news.", e);
		}
		
	}
	
}

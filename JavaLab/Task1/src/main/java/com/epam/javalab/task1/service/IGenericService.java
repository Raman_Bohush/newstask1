package com.epam.javalab.task1.service;

import java.util.List;

import com.epam.javalab.task1.service.exception.ServiceException;

/**
 * Basic Service interface.
 * Set of CRUD operations 
 * and coordinates the application's response in each operation.
 * @author Raman_Bohush
 *
 * @param <E> persistent object type
 */
public interface IGenericService<E> {

	/**
	 * Invokes create operation from Data Access Object layer.
	 * Creates the persistent object.
	 * Checks parameter to create.
	 * @param entity object to create
	 * @return the id of the added object
	 * @throws ServiceException
	 */
	public long create(E entity) throws ServiceException;
	
	/**
	 * Invokes read operation from Data Access Object layer.
	 * Gets the persistent object by id.
	 * @param id the id
	 * @return the persistent object
	 * @throws ServiceException
	 */
	public E read(Long id) throws ServiceException;
	
	/**
	 * Invokes update operation from Data Access Object layer.
	 * Updates the persistent object.
	 * Checks parameter to update.
	 * @param object to update
	 * @throws ServiceException
	 */
	public void update(E entity) throws ServiceException;
	
	/**
	 * Invokes delete operation from Data Access Object layer.
	 * Delete the persistent object from database.
	 * Checks parameter to delete.
	 * @param id the id
	 * @throws ServiceException
	 */
	public void delete(Long id) throws ServiceException;
	
	/**
	 * Invokes readAll operation from Data Access Object layer.
	 * Gets the list of all objects.
	 * @return list of all objects
	 * @throws ServiceException
	 */
	public List<E> readAll() throws ServiceException;
}

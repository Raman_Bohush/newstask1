package com.epam.javalab.task1.service;

import java.util.List;

import com.epam.javalab.task1.domain.Author;
import com.epam.javalab.task1.domain.News;
import com.epam.javalab.task1.domain.Tag;
import com.epam.javalab.task1.service.exception.ServiceException;

/**
 * News management Service interface.
 * Set of transactions, complex operations 
 * and coordinates the application's response in each operation.
 * @author Roma
 *
 */
public interface INewsManagementService {
	
	/***
	 * Saves news with author and tags as one step.
	 * @param news the news
	 * @param author the author
	 * @param tagsList the list of the tags
	 * @throws ServiceException
	 */
	public void saveNews(News news, Author author, List<Tag> tagsList) throws ServiceException;
}

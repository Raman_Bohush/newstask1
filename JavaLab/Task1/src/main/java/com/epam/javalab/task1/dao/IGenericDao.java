package com.epam.javalab.task1.dao;

import java.util.List;
import com.epam.javalab.task1.dao.exception.DaoException;


/**
 * Basic Data Access Object interface.
 * Provides CRUD operations with persistent objects.
 * @author Raman_Bohush
 *
 * @param <E> persistent object type
 */
public interface IGenericDao<E> {
	
	/**
	 * Creates the persistent object.
	 * @param entity object to create
	 * @throws DaoException
	 */
	public long create(E entity) throws DaoException;
	
	/**
	 * Gets the persistent object by id.
	 * @param id the id
	 * @return the persistent object
	 * @throws DaoException
	 */
	public E read(Long id) throws DaoException;
	
	/**
	 * Updates the persistent object.
	 * @param entity object to update
	 * @throws DaoException
	 */
	public void update(E entity) throws DaoException;
	
	/**
	 * Delete the persistent object from database.
	 * @param id the id
	 * @throws DaoException
	 */
	public void delete(Long id) throws DaoException;
	
	/**
	 * Gets the list of all objects.
	 * @return list of all objects
	 * @throws DaoException
	 */
	public List<E> readAll() throws DaoException;
}

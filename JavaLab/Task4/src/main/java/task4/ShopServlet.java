package task4;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

public class ShopServlet extends HttpServlet {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = -967789014117991520L;

	/**
	 * Logger
	 */
	private static final Logger logger = Logger.getLogger(ShopServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		StreamSource xlsStreamSource = new StreamSource(Paths.get(
				getServletContext().getRealPath("/WEB-INF/resources/xsl/shopStylesheet.xsl")).toFile());

		StreamSource xmlStreamSource = new StreamSource(Paths.get(
				getServletContext().getRealPath("/WEB-INF/resources/xml/shop.xml")).toFile());

		TransformerFactory transformerFactory = TransformerFactory.newInstance();

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		StreamResult result = new StreamResult(baos);

		ServletOutputStream servletOutputStream = null;

		try {

			Transformer transformer = transformerFactory.newTransformer(xlsStreamSource);

			String action = req.getParameter("action");

			if (action == null) {
				transformer.setParameter("action", "cat");
			} else {
				transformer.setParameter("action", action);
			}

			transformer.transform(xmlStreamSource, result);

			byte[] buf = baos.toByteArray();

			resp.setContentType("text/html");
			resp.setContentLength(buf.length);

			servletOutputStream = resp.getOutputStream();

			servletOutputStream.write(buf);

		} catch (TransformerConfigurationException e) {
			logger.info("Transformation configuration error", e);
		} catch (TransformerException e) {
			logger.info("Transformation error", e);
		} finally {
			baos.close();
		}
	}
}

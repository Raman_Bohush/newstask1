<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sh="http://www.epam.com">
	<xsl:template name="subcategories">
		<html>
			<body>
				<h2>Subcategories</h2>
				<table border="1">
					<tr bgcolor="#5858FA">
						<th>Subcategory</th>
						<th>Quantity of products</th>
					</tr>
					<xsl:for-each select="sh:shop/sh:category/sh:subcategory">
						<tr>
							<td>
								<a href="/task4/shop?action=prod">
									<xsl:value-of select="@subcat_name" />
								</a>
							</td>
							<td>
								<xsl:value-of select="count(sh:product)" />
							</td>
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sh="http://www.epam.com">
	<xsl:template name="categories">
		<html>
			<body>
				<h2>Categories</h2>
				<table border="1">
					<tr bgcolor="#5858FA">
						<th>Category</th>
						<th>Quantity of products</th>
					</tr>
					<xsl:for-each select="sh:shop/sh:category">
						<tr>
							<td>
								<a href="/task4/shop?action=subcat">
									<xsl:value-of select="@cat_name" />
								</a>
							</td>
							<td>
								<xsl:value-of select="count(sh:subcategory/sh:product)" />
							</td>
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
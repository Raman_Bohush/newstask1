<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="categories.xsl" />
	<xsl:import href="subcategories.xsl" />
	<xsl:import href="products.xsl" />
	<xsl:import href="add.xsl" />
	<xsl:output method="html" version="4.01" encoding="UTF-8"
		indent="yes" />
	<xsl:param name="action" />
	<xsl:template match="/">
		<head>
			<style>
				table{
				margin: 0 auto;
				}

				td{
				width: 150px;
				}

				th{
				color: #fff;
				}

				h2{
				text-align: center;
				}

				.add{
				margin: 20px auto;
				width: 100px;
				}

				input{
				width: 100px;
				}
				
				#addForm{
				width: 200px;
				margin: 200px auto;
				}
				
				#addForm form{
				width: relative;
				}
				
				#addForm form input[type="text"]{
				width: 200px;
				height: 30px;
				border-radius: 3px;
				border: 1px solid #000;
				}
				
				#addForm form label{
				font-size:24px;
				}
				
				input[type="submit"]{
				border-radius: 3px;
				}		
				
				.save-btn{
				margin: 20px auto;
				}	
			</style>
		</head>
		<xsl:choose>
			<xsl:when test="$action = 'cat'">
				<xsl:call-template name="categories" />
			</xsl:when>
			<xsl:when test="$action = 'subcat'">
				<xsl:call-template name="subcategories" />
			</xsl:when>
			<xsl:when test="$action = 'prod'">
				<xsl:call-template name="products" />
			</xsl:when>
			<xsl:when test="$action = 'Add'">
				<xsl:call-template name="add" />
			</xsl:when>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
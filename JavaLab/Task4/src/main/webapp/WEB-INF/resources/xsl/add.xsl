<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template name="add">
		<html>
			<body>
				<div id="addForm">
					<form action="/task4/shop/add">
						<div class="model">
							<div>
								<label for="model">Model:</label>
							</div>
							<div>
								<input id="model" type="text" />
							</div>
						</div>
						<div class="producer">
							<div>
								<label for="producer">Producer:</label>
							</div>
							<div>
								<input id="producer" type="text" />
							</div>
						</div>
						<div class="color">
							<div>
								<label for="color">Color:</label>
							</div>
							<div>
								<input id="color" type="text" />
							</div>
						</div>
						<div class="price">
							<div>
								<label for="price">Price:</label>
							</div>
							<div>
								<input id="price" type="text" />
							</div>
						</div>
						<div class="date">
							<div>
								<label for="date">Date:</label>
							</div>
							<div>
								<input id="date" type="text" />
							</div>
						</div>
						<div>
							<input class="save-btn" type="submit" />
						</div>
					</form>
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
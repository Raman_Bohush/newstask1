<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sh="http://www.epam.com">
	<xsl:template name="products">
		<html>
			<body>
				<h2>Products</h2>
				<table border="1">
					<tr bgcolor="#5858FA">
						<th>Producer</th>
						<th>Color</th>
						<th>Price</th>
						<th>Date</th>
					</tr>
					<xsl:for-each select="sh:shop/sh:category/sh:subcategory/sh:product">
						<tr>
							<td>
								<xsl:value-of select="sh:producer" />
							</td>
							<td>
								<xsl:value-of select="sh:color" />
							</td>
							<td>
								<xsl:value-of select="sh:price" />
							</td>
							<td>
								<xsl:value-of select="sh:date" />
							</td>
						</tr>
					</xsl:for-each>
				</table>
				<div class="add">
					<form action="/task4/shop/add" method="GET">
						<input type="submit" name="action" value="Add" />
					</form>
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
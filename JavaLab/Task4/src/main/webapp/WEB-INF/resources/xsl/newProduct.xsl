<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>
	<xsl:template name="addNewProduct">
		<subcategory>
			<xsl:apply-templates select="@* | *" />
			<subcategory>
				<product model="test">
					<producer>testPr</producer>
					<date>2015-11-10</date>
					<color>TestCol</color>
					<price>99</price>
				</product>
			</subcategory>
		</subcategory>
	</xsl:template>
</xsl:stylesheet>